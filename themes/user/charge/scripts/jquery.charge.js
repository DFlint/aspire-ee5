(function($) {

    Stripe.setPublishableKey = function() {
        console.log("Charge Error: It looks like you're using the older template code. Please update your implementation per the new guidelines.");
    };

    $.fn.charge = function(form, error_container, progress, coupon_validation_form, coupon_status_container) {
        alert("Charge Error: It looks like you're using the older template code. Please update your implementation per the new guidelines.");
    };

    $.fn.chargeForm = function(options) {
        var stripe_key = options.stripe_key;
        var form_selector = options.form;
        var form_element = document.querySelector(options.form);

        var card_element = document.querySelector(options.card);
        var card_error_element = document.getElementById('card-errors'); // Provided by Stripe

        var pay_button_element = document.querySelector(options.pay_button);
        var pay_button_element_text = $(pay_button_element).html();
        var pay_button_processing_text = options.pay_button_processing_text;
        var pay_errors_element = document.querySelector(options.pay_errors);

        var progress_element = document.querySelector(options.progress);

        var coupon_form_element = document.querySelector(options.coupon_form);
        var coupon_field_element = document.querySelector(options.coupon_field);
        var coupon_status_element = document.querySelector(options.coupon_status);

        var stripe = Stripe(stripe_key);
        var elements = stripe.elements(options.elements_options);
        var billing_selectors = options.billing_selectors;
        var card = elements.create('card', options.card_form_options);

        card.mount(card_element);

        card.addEventListener('change', function(event) {
            if (event.error) {
                card_error_element.textContent = event.error.message;
            } else {
                card_error_element.textContent = '';
            }
        });

/*******************************/
/** Stripe PaymentIntent Flow **/
/*******************************/

        // Watch for the payment detail form to be submit and prevent it from actually submitting.
        form_element.addEventListener('submit', function(e) {
            e.preventDefault();
        });

        // Listen for a click on the button. We are purposely NOT listening for the form submission
        // as we don't want someone pressing [enter] in the form to submit it accidentally.
        $(pay_button_element).on('click', function(ev) {
            var billing_data = {};

            // Check if we have billing selectors and if so, grab the values from them on submission.
            if (billing_selectors !== undefined) {
                billing_data.billing_details = {
                    name: $(billing_selectors.name).val(),
                    address: {
                        line1: $(billing_selectors.address.line1).val(),
                        line2: $(billing_selectors.address.line2).val(),
                        city: $(billing_selectors.address.city).val(),
                        state: $(billing_selectors.address.state).val(),
                        country: $(billing_selectors.address.country).val(),
                        postal_code: $(billing_selectors.address.postal_code).val()
                    }
                };
            }

            // Hide the error elemenr.
            $(pay_errors_element).hide();

            // Show the progress element (if there is one).
            $(progress_element).show();

            // Find out if we're using custom processing text and if so, set it.
            if (!pay_button_processing_text && $(pay_button_element).data('processing')) {
                pay_button_processing_text = $(pay_button_element).data('processing');
            }

            disablePayButton();

            // If the "use saved card" option is checked, bypass the Stripe card verification.
            if ($('#card_use_saved').is(':checked')) {
                // Submit the Charge form to the server.
                var data = $(form_selector).serialize();

                submitForm(data);
            } else {
                // Send the credit card details to Stripe. Stripe will return a token that represents the
                // customer's payment information. When we make the actual purchase from the server, we use
                // this token instead of passing credit card details; this is the only PCI compliant way to
                // transmit payment information to Stripe.
                stripe.createPaymentMethod('card', card, billing_data).then(function(result) {
                    if (result.error) {
                        // Inform the user if there was an error. The `card-errors` element is provided by
                        // Stripe so we don't let the user pass an element for this.
                        card_error_element.textContent = result.error.message;

                        // Re-enable the payment button so the user can update their payment details and try again.
                        enablePayButton();

                        // Hide the progress element.
                        $(progress_element).hide();
                    } else {
                        // Send the token to your server
                        stripeTokenHandler(result.paymentMethod.id);
                    }
                });
            }
        });

        // Now that we have the paymentMethod token, we can set it in the form and submit
        // the form to the server via AJAX. This will submit all the other details of the
        // order to Charge and Charge will use the PaymentMethod token to try to make the
        // payment via Stripe.
        function stripeTokenHandler(paymentMethodToken) {
            // Check to see if we've already inserted the token.
            if ($(form_selector).find('input[name="card_token"]').length !== 0) {
                $(form_selector).find('input[name="card_token"]').val(paymentMethodToken);
            } else {
                // Insert the token ID into the form so it gets submitted to the server
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'card_token');
                hiddenInput.setAttribute('value', paymentMethodToken);
                $(form_selector).append(hiddenInput);
            }

            // Submit the Charge form to the server.
            var data = $(form_selector).serialize();

            submitForm(data);
        }

        // Handle whatever response we get back from Stripe. This could be an error, a request for verification, or success.
        function handleServerResponse(response) {
            if (response.errors) {
                showError(response.errors);
            } else if (response.error) {
                showError(response.error.message);
            } else if (response.data && response.data.requires_action === true) {
                if ("payment_intent_client_secret" in response.data) {
                    // Subscription response
                    stripe.handleCardPayment(
                        response.data.payment_intent_client_secret
                    ).then(function(result) {
                        if (result.error) {
                            showError(result.error.message);
                        } else {
                            var data = $(form_selector).serialize() + '&verified=1&sub_id=' + response.data.stripe_subscription_id;

                            submitForm(data);
                        }
                    });
                } else if ("stripe_client_secret" in response.data) {
                    // Single charge response
                    stripe.handleCardAction(
                        response.data.stripe_client_secret //payment_intent_client_secret
                    ).then(function(result) {
                        if (result.error) {
                            showError(result.error.message);
                        } else {
                            // The card action has been handled
                            // The PaymentIntent can be confirmed again on the server
                            var data = $(form_selector).serialize() + '&payment_intent_id=' + result.paymentIntent.id;

                            submitForm(data);
                        }

                    });
                }
            } else {
                // Handle Success
                if (response.return) {
                    window.location.href = response.return;
                }
            }
        }

        function submitForm(data) {
            $.ajax({
                type: 'POST',
                url: $(form_selector).attr('action'),
                data: data,
                dataType: 'json',
                success: function (response) {
                    // If we're here, then the server was able to get a response. This "success" is not whether
                    // the payment went through, just that the server did not fail to process our request.

                    // Pass the response to the `handleServerResponse` function. We extract this into it's own
                    // function because it can be called multiple times depending on if we need additional verification.
                    handleServerResponse(response);
                },
                error: function (xhr, text, error) {
                    // If we're here, then something went wrong with the server (like a 500 error, or a 403 auth error).
                    console.log('SERVER ERROR:', error);

                    showError('The webserver encountered an error processing this request. More information is available in your browser\'s developer console.');
                }
            });
        }

        function showError(message) {
            // Show the error message on the form.
            $(pay_errors_element).html(message).addClass('alert').addClass('alert-error').show();
            enablePayButton();

            // Hide the progress element.
            $(progress_element).hide();
        }

        function showProcessingText() {
            $(pay_button_element).html()
        }

        function revertProcessingText() {

        }

        function disablePayButton() {
            $(pay_button_element).prop('disabled', true).addClass('disabled');

            if (pay_button_processing_text) {
                $(pay_button_element).html(pay_button_processing_text);
            }
        }

        function enablePayButton() {
            $(pay_button_element).prop('disabled', false).removeClass('disabled').html(pay_button_element_text);
        }

        $(coupon_form_element).submit(function(event) {
            event.preventDefault();

            $.ajax({
                type: 'POST',
                url: $(coupon_form_element).attr('action'),
                data: $(coupon_form_element).serialize(),
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    if(data.success) {
                        // Add your success code here (redirect or status message).
                        $(coupon_status_element).show().html('<div style="font-size:20px;color:green;font-weight:bold;">SUCCESS!</div>');
                    } else {
                        // show the errors on the form
                        $(coupon_status_element).show().html('what'+data.errors).addClass('alert').addClass('alert-error');
                        $(form).find('button').prop('disabled', false).removeClass('disabled');
                        return false;
                    }
                },
                error: function (xhr, text, error) {
                    console.log(error);
                }
            });
        });
    };

}(jQuery));