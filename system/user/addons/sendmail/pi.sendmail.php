<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * SendMail Plugin
 *
 * @category    Plugin
 * @author      Zeth Copher
 * @link        https://midwesterninteractive.com
 */

$plugin_info = array(
    'pi_name'       => 'SendMail',
    'pi_version'    => '0.1',
    'pi_author'     => 'Zeth Copher',
    'pi_author_url' => 'https://midwesterninteractive.com',
    'pi_description'=> '',
    'pi_usage'      => Sendmail::usage()
);


class Sendmail {

    public $return_data;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->EE = get_instance();
        $variables[] = array(
            'id' => $_POST['entry_id'],
            'name' => $_POST['recipient'],
            'street' => $_POST['street'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'zip' => $_POST['zip']
        );
        $this->message = ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $variables);
    }

    public function send()
    {
        if ($_POST) {

            $from = "info@aspirescholarship.org";
            $recipient = $_POST['recipient'];
            $recipient_email = $_POST['recipient_email'];
            $subject = ee()->TMPL->fetch_param('subject');
            

            ee()->load->library('email');
            ee()->load->helper('text');

            ee()->email->wordwrap = true;
            ee()->email->mailtype = 'html';
            ee()->email->from($from, "Aspire Scholarship");
            ee()->email->to($recipient_email);
            ee()->email->subject("$subject");
            ee()->email->message(entities_to_ascii($this->message));
            ee()->email->Send();

            return "sent";

        } else {

            exit("Unauthorized");  

        }
        

    }
    /**
     * Plugin Usage
     */
    public static function usage()
    {
        ob_start();
        ?>
        <h3>SendMail</h3>
        <p></p>
        <hr/>
        Include your documentation here...
        <?php
                $buffer = ob_get_contents();
                ob_end_clean();
                return $buffer;
    }
}


/* End of file pi.sendmail.php */
/* Location: /system/expressionengine/third_party/sendmail/pi.sendmail.php */
