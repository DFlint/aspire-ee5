<?php 
return array(
    'name'       => 'SendMail',
    'version'    => '0.1',
    'author'     => 'Zeth Copher',
    'author_url' => 'https://midwesterninteractive.com',
    'description'=> 'Sends Mail',
    'namespace'      => 'sendmail/'
);