<?php

return array(
    'author'      => 'MidwesternInteractive',
    'author_url'  => 'https://midwesterninteractive.com',
    'name'        => 'UpdateStatus',
    'description' => 'Update Entry Status',
    'version'     => '0.1',
    'namespace'   => 'updatestatus/'
);