<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * update_status Plugin
 *
 * @category    Plugin
 * @author      Midwestern Interactive
 * @link        
 */

$plugin_info = array(
    'pi_name'       => 'update_status',
    'pi_version'    => '0.1',
    'pi_author'     => 'Midwestern Interactive',
    'pi_author_url' => '',
    'pi_description'=> 'Update Entry Status from Open or Close',
    'pi_usage'      => Update_status::usage()
);


class Update_status {

    public $return_data;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->EE =& get_instance();
    }

    public function close_entry()
    {   
        $entry_id = ee()->TMPL->fetch_param('entry_id');

        if ($entry_id != null)
        {
            ee()->db->update(
                'channel_titles',
                array(
                    'status' => 'closed'
                ),
                array (
                    'entry_id' => $entry_id
                )
            );
        } else {
             return null;
        }
    }

    /**
     * Plugin Usage
     */
    public static function usage()
    {
        ob_start();
?>
<h3>update_status</h3>
<p>Update Entry Status from Open or Close</p>
<hr/>
Include your documentation here...
<?php
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }
}


/* End of file pi.update_status.php */
/* Location: /system/expressionengine/third_party/update_status/pi.update_status.php */
