<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * ExpressionEngine Expresso Library Class
 *
 * @package		Expresso
 * @category	Library
 * @author		Ben Croker
 * @link		http://www.putyourlightson.net/expresso
 */

class Expresso_lib
{

    /**
      *  Constructor
      */
    public function __construct()
    {
        $this->site_id = ee()->config->item('site_id');
    }

    // --------------------------------------------------------------------

    /**
      *  Get channel uri entries
      */
    public function get_channel_uri_entries($channel_uris)
    {
        $links = array();
        $select_text = 'Select an entry...';

        // loop through channel uris
        foreach ($channel_uris as $channel_id => $channel_uri) {
            if ($channel_uri) {
                // get channel title
                ee()->db->where('channel_id', $channel_id);
                $channel = ee()->db->get('channels')->row();

                if ($channel) {
                    // get channel entries
                    ee()->db->where('channel_id', $channel_id);
                    ee()->db->order_by('title', 'asc');
                    $entries = ee()->db->get('channel_titles')->result();

                    foreach ($entries as $entry) {
                        $url = $this->_create_url($channel_uri);
                        $url = str_replace('{url_title}', $entry->url_title, $url);
                        $url = str_replace('{entry_id}', $entry->entry_id, $url);
                        $links[] = array($channel->channel_title.' > '.$entry->title, $url);
                    }
                }
            }
        }

        if (count($links)) {
            array_unshift($links, array($select_text, ''));
        }

        return $links;
    }

    // --------------------------------------------------------------------

    /**
      *  Get page links of specified module
      */
    public function get_page_links($module='')
    {
        $links = array();
        $select_text = 'Select a page...';
        $site_pages = array();

        if ($module == 'Pages' or $module == 'Structure') {
            // get site pages
            ee()->db->select('site_pages');
            ee()->db->where('site_id', $this->site_id);
            $query = ee()->db->get('sites');

            $site_pages = unserialize(base64_decode($query->row('site_pages')));
            $site_pages = isset($site_pages[$this->site_id]) ? $site_pages[$this->site_id] : $site_pages;
        }

        if ($module == 'Pages') {
            if (isset($site_pages['uris']) and count($site_pages['uris'])) {
                $entry_ids = array_keys($site_pages['uris']);

                ee()->db->select(array('entry_id', 'title'));
                ee()->db->where_in('entry_id', $entry_ids);
                ee()->db->order_by('title', 'asc');
                $query = ee()->db->get('channel_titles');

                foreach ($query->result() as $row) {
                    if (isset($site_pages['uris'][$row->entry_id])) {
                        $title = $row->title;
                        $url = $this->_create_url($site_pages['uris'][$row->entry_id]);
                        $links[] = array($title, $url);
                    }
                }
            }
        } elseif ($module == 'Structure') {
            // include structure SQL model
            include_once PATH_THIRD.'structure/sql.structure.php';

            // get structure data
            $sql = new Sql_structure();
            $data = $sql->get_data();

            foreach ($data as $item) {
                if (isset($site_pages['uris'][$item['entry_id']])) {
                    $title = str_repeat("-", $item['depth']).$item['title'];
                    $url = $this->_create_url($site_pages['uris'][$item['entry_id']]);
                    $links[] = array($title, $url);
                }
            }
        } elseif ($module == 'Navee') {
            // include navee model
            include_once PATH_THIRD.'navee/models/navee_mod.php';

            // create navee class and set required param
            $navee = new Navee_mod();
            $navee->param['skip_closed_entries'] = false;

            // get navee navs
            ee()->db->select('navigation_id, nav_name');
            ee()->db->where('site_id', $this->site_id);
            $query = ee()->db->get('navee_navs');
            $navee_navs = $query->result_array();

            // get navee data
            foreach ($navee_navs as $navee_nav) {
                $data = $navee->getNavigationTree($navee_nav['navigation_id']);
                array_push($links, array('NavEE Menu: '.$navee_nav['nav_name'], ''));
                $links = array_merge($links, $this->_parse_navee_links($data));
            }

            $select_text = 'Select a NavEE item...';
        }

        if (count($links)) {
            array_unshift($links, array($select_text, ''));
        }

        return $links;
    }

    // --------------------------------------------------------------------

    /**
      *  Create URL from URI
      */
    private function _create_url($uri)
    {
        $site_slash = (substr(ee()->config->item('site_url'), -1, 1) == '/') ? '' : '/';
        $site_index = ee()->config->item('site_index') ? '{site_index}/' : '';
        return '{site_url}'.$site_slash.$site_index.trim($uri, '/');
    }

    // --------------------------------------------------------------------

    /**
      *  Parse navee links recursively
      */
    private function _parse_navee_links($data, $depth=1)
    {
        $links = array();

        foreach ($data as $item) {
            if (!$item['include']) {
                continue;
            }

            $title = str_repeat("-", $depth).$item['text'];
            $url = $this->_create_url($item['link']);
            $links[] = array($title, $url);

            if (count($item['kids'])) {
                $links = array_merge($links, $this->_parse_navee_links($item['kids'], $depth + 1));
            }
        }

        return $links;
    }
}

// END CLASS

/* End of file lib.expresso.php */
/* Location: ./system/expressionengine/third_party/expresso/libraries/expresso_lib.php */;
