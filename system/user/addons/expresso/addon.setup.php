<?php

return array(
    'author'      => 'PutYourLightsOn',
    'author_url'  => 'https://www.putyourlightson.net/',
    'name'        => 'Expresso',
    'description' => 'WYSIWYG editor fieldtype for channel entries',
    'version'     => '4.2.9',
    'namespace'   => '\\',
    'settings_exist' => true,
    'docs_url'    => 'https://www.putyourlightson.net/expresso/docs',
    'fieldtypes' => array(
        'expresso' => array(
            'name' => 'Expresso',
            'compatibility' => 'text'
        )
    )
);
