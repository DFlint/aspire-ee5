<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * ExpressionEngine Expresso Fieldtype Class
 *
 * @package		Expresso
 * @category	Fieldtypes
 * @author		Ben Croker
 * @link		https://www.putyourlightson.net/expresso
 */

class Expresso_ft extends EE_Fieldtype
{
    public $info = array(
        'name'       => 'Expresso',
        'version'    => '4.2.9'
    );

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        // get settings
        require PATH_THIRD.'expresso/settings.php';

        // setup cache
        if (!isset(ee()->session->cache['expresso'])) {
            ee()->session->cache['expresso'] = array();
        }

        // backwards compabitility for EE < 2.4
        if (!defined('URL_THIRD_THEMES')) {
            define('URL_THIRD_THEMES', ee()->config->item('theme_folder_url').'third_party/');
            define('PATH_THIRD_THEMES', ee()->config->item('theme_folder_path').'third_party/');
        }
    }

    // --------------------------------------------------------------------

    /**
      *  Install
      */
    public function install()
    {
        $settings = array();

        // default global settings
        $settings['global'] = $this->default_settings;

        return $settings;
    }

    // --------------------------------------------------------------------

    /**
      *  Update
      */
    public function update()
    {
        return true;
    }

    // --------------------------------------------------------------------

    /**
      *  Accepts Content Type
      */
    public function accepts_content_type($name)
    {
        return (
        	$name == 'channel' ||
        	$name == 'grid' ||
        	$name == 'low_variables' ||
        	$name == 'blocks/1'
        );
    }

    // --------------------------------------------------------------------

    /**
      *  Display Field
      */
    public function display_field($data)
    {
        $file_uploads = $this->_file_uploads();

        $this->_include_scripts($file_uploads);

        $js = $this->_get_config($this->field_name);
        $js .= NL.'$(function() {'.NL.'expresso("'.$this->field_name.'", '.$file_uploads.', expresso_config_'.$this->field_name.');'.NL.'});';
        ee()->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.URL_THIRD_THEMES.'expresso/css/editor.css?v='.$this->info['version'].'" />');
        ee()->cp->add_to_foot('<script type="text/javascript">'.NL.$js.NL.'</script>');

        $textarea = '<textarea id="'.$this->field_name.'" name="'.$this->field_name.'" class="expresso">'.$data.'</textarea>';

        return $textarea;
    }

    // --------------------------------------------------------------------

    /**
      *  Display Grid Field
      */
    public function grid_display_field($data)
    {
        $file_uploads = $this->_file_uploads();

        $this->_include_scripts($file_uploads);

        // limit height
        $this->settings['autoGrow_maxHeight'] = '200';

        // add grid script if not already added
        if (!isset(ee()->session->cache['expresso']['added_grid'])) {
            ee()->cp->add_to_foot('<script type="text/javascript" src="'.URL_THIRD_THEMES.'expresso/javascript/grid.js?v='.$this->info['version'].'"></script>');

            ee()->session->cache['expresso']['added_grid'] = true;
        }

        // add config js if not already added
        if (!isset(ee()->session->cache['expresso']['grid_columns'][$this->field_id])) {
            $js = $this->_get_config('grid_field_id_'.$this->field_id);
            $js .= NL.'var expresso_file_upload_grid_field_id_'.$this->field_id.' = 1;';
            ee()->cp->add_to_foot('<script type="text/javascript">'.NL.$js.NL.'</script>');

            ee()->session->cache['expresso']['grid_columns'][$this->field_id] = true;
        }

        return '<textarea name="'.$this->field_name.'" data-field-id="'.$this->field_id.'" class="expresso">'.$data.'</textarea>';
    }

    // --------------------------------------------------------------------

    /**
      *  Display Matrix Cell
      */
    public function display_cell($data)
    {
        $file_uploads = $this->_file_uploads();

        $this->_include_scripts($file_uploads);

        // limit height
        $this->settings['autoGrow_maxHeight'] = '200';

        // add matrix script if not already added
        if (!isset(ee()->session->cache['expresso']['added_matrix'])) {
            ee()->cp->add_to_foot('<script type="text/javascript" src="'.URL_THIRD_THEMES.'expresso/javascript/matrix.js?v='.$this->info['version'].'"></script>');

            ee()->session->cache['expresso']['added_matrix'] = true;
        }

        // add config js if not already added
        if (!isset(ee()->session->cache['expresso']['matrix_columns'][$this->col_id])) {
            $js = $this->_get_config('matrix_col_id_'.$this->col_id);
            $js .= NL.'var expresso_file_upload_matrix_col_id_'.$this->col_id.' = 1;';
            ee()->cp->add_to_foot('<script type="text/javascript">'.NL.$js.NL.'</script>');

            ee()->session->cache['expresso']['matrix_columns'][$this->col_id] = true;
        }

        return '<textarea name="'.$this->cell_name.'" class="expresso">'.$data.'</textarea>';
    }

    // --------------------------------------------------------------------

    /**
      *  Display as Content Element
      */
    public function display_element($data)
    {
        $file_uploads = $this->_file_uploads();

        $this->_include_scripts($file_uploads);

        // limit height
        $this->settings['autoGrow_maxHeight'] = '200';

        $field_id = random_string('alnum', 16);

        // add content element script if not already added
        if (!isset(ee()->session->cache['expresso']['added_content_element'])) {
            ee()->cp->add_to_foot('<script type="text/javascript" src="'.URL_THIRD_THEMES.'expresso/javascript/content_element.js?v='.$this->info['version'].'"></script>');

            ee()->session->cache['expresso']['added_content_element'] = true;
        }

        $js = $this->_get_config('content_element_'.$field_id);
        //$js .= NL.'$(function() {'.NL.'expresso("'.$field_id.'", '.$file_uploads.', expresso_config_content_element_'.$field_id.');'.NL.'});';

        // if not a hidden instance of expresso then initialise
        if ($this->field_name != '__element_name__[__index__][data]') {
            $js .= NL.'$(function() {'.NL.'expresso("'.$field_id.'", '.$file_uploads.', expresso_config_content_element_'.$field_id.');'.NL.'});';
        }

        $js .= NL.'var expresso_file_upload_content_element_'.$field_id.' = 1;';
        ee()->cp->add_to_foot('<script type="text/javascript">'.NL.$js.NL.'</script>');

        return '<textarea id="'.$field_id.'" name="'.$this->field_name.'" data-field-id="'.$field_id.'" class="expresso">'.$data.'</textarea>';
    }

    // --------------------------------------------------------------------

    /**
      *  Display Low Variables Fieldtype
      */
    public function var_display_field($data)
    {
        $file_uploads = $this->_file_uploads();

        $this->_include_scripts($file_uploads);

        // limit height
        $this->settings['autoGrow_maxHeight'] = '200';

        $field_id = str_replace(array('[', ']'), array('_', ''), $this->field_name);

        ee()->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.URL_THIRD_THEMES.'expresso/css/editor.css?v='.$this->info['version'].'" />');

        $js = $this->_get_config($field_id);
        $js .= NL.'$(function() {'.NL.'expresso("'.$field_id.'", '.$file_uploads.', expresso_config_'.$field_id.');'.NL.'});';
        ee()->cp->add_to_foot('<script type="text/javascript">'.NL.$js.NL.'</script>');

        return '<textarea id="'.$field_id.'" name="'.$this->field_name.'" class="expresso">'.$data.'</textarea>';
    }

    // --------------------------------------------------------------------

    /**
      *  Check if file uploads allowed
      */
    private function _file_uploads()
    {
        $return = 0;

        // check that there is at least one upload destination
        $upload_preferences = array();

        if (version_compare(APP_VER, '2.4', '>=')) {
            ee()->load->model('File_upload_preferences_model');
            $upload_preferences = ee()->File_upload_preferences_model->get_file_upload_preferences();
        } else {
            ee()->load->model('tools_model');
            $upload_preferences = ee()->tools_model->get_upload_preferences()->result_array();
        }

        if (count($upload_preferences)) {
            $return = 1;
        }

        return $return;
    }

    // --------------------------------------------------------------------

    /**
      *  Include Scripts
      */
    private function _include_scripts($file_uploads)
    {
        // include scripts if not already included
        if (!isset(ee()->session->cache['expresso']['included_scripts'])) {
            // add ckeditor scripts
            ee()->cp->add_to_foot('<script type="text/javascript" src="'.URL_THIRD_THEMES.'expresso/ckeditor_'.$this->ckeditor_version.'/ckeditor.js"></script>');
            ee()->cp->add_to_foot('<script type="text/javascript" src="'.URL_THIRD_THEMES.'expresso/ckeditor_'.$this->ckeditor_version.'/adapters/jquery.js"></script>');
            ee()->cp->add_to_foot('<script type="text/javascript" src="'.URL_THIRD_THEMES.'expresso/javascript/expresso.js?v='.$this->info['version'].'"></script>');

            // add global variables
            $js = 'var site_url = "'.ee()->config->item('site_url').'";';
            $js .= 'var theme_folder_url = "'.URL_THIRD_THEMES.'expresso/";';
            $js .= NL.'var ee22 = '.(version_compare(APP_VER, '2.2', '>=') ? 1 : 0).';';

            // check if there are extra links for link dialog
            $extra_links = $this->_get_extra_links();
            $js .= NL.'var extra_links = '.(count($extra_links) ? json_encode($extra_links) : 'false').';';

            // filepicker
            $filepicker = ee('CP/FilePicker')->make()->getUrl();
            $js .= NL.'var filepicker = "'.$filepicker.'";';

            // customise_dialogs
            $js .= NL.'customise_dialogs('.$file_uploads.');';

            ee()->cp->add_to_foot('<script type="text/javascript">'.NL.$js.NL.'</script>');

            ee()->session->cache['expresso']['included_scripts'] = true;
        }
    }

    // --------------------------------------------------------------------

    /**
      *  Get Config
      */
    private function _get_config($id)
    {
        // toolbar
        $settings['toolbar'] = $this->settings['toolbar'];

        // check if autoGrow_maxHeight is set
        $settings['autoGrow_maxHeight'] = isset($this->settings['autoGrow_maxHeight']) ? $this->settings['autoGrow_maxHeight'] : '';

        // get global settings
        if (!isset($this->settings['global'])) {
            ee()->db->select('settings');
            ee()->db->where('name', 'expresso');
            $query = ee()->db->get('fieldtypes');
            $row = $query->row('settings');
            $row = is_array($row) ? $row : unserialize(base64_decode($row));
            $this->settings['global'] = $row['global'];
        }

        // merge global and local css and remove line breaks
        $settings['contentsCss'] = str_replace(array("\n", "\t", "\""), array(" ", " ", "\'"), $this->settings['global']['contentsCss'].' '.(isset($this->settings['contentsCss']) ? $this->settings['contentsCss'] : ''));

        // get clean settings
        $settings = $this->_clean_settings(array_merge($this->settings['global'], $settings));


        $js = 'var expresso_config_'.$id.' = {';

        // loop through field settings
        foreach ($this->field_settings as $key) {
            if (isset($settings[$key]) and (is_array($settings[$key]) or trim($settings[$key]))) {
                $js .= $key.': "'.$settings[$key].'", ';
            }
        }

        // custom toolbar
        if ($settings['toolbar'] == 'custom' && isset($settings['custom_toolbar'])) {
            $js .= 'toolbar: ['.$settings['custom_toolbar'].']';
        }

        // non custom toolbars
        else {
            $js .= 'toolbar: [["Bold","Italic","Underline","Strike"';

            if (in_array('Subscript', $settings['toolbar_icons'])) {
                $js .= ',"Subscript"';
            }

            if (in_array('Superscript', $settings['toolbar_icons'])) {
                $js .= ',"Superscript"';
            }

            $js .= ']';

            if ($settings['toolbar'] != 'light') {
                $block = array();

                if (in_array('List Block', $settings['toolbar_icons'])) {
                    $block[] = '"NumberedList"';
                    $block[] = '"BulletedList"';
                }

                if (in_array('Indent', $settings['toolbar_icons'])) {
                    $block[] = '"Outdent"';
                    $block[] = '"Indent"';
                }

                if (in_array('Blockquote', $settings['toolbar_icons'])) {
                    $block[] = '"Blockquote"';
                }

                $js .= empty($block) ? '' : ',['.implode(',', $block).']';

                $js .= in_array('Justify Block', $settings['toolbar_icons']) ? ',["JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"]' : '';

                $js .= in_array('PasteFromWord', $settings['toolbar_icons']) ? ',["PasteFromWord"]' : '';
            }

            if ($settings['toolbar'] == 'full') {
                $headers = '';

                for ($i = 1; $i <= 6; $i++) {
                    if (in_array('h'.$i, $settings['headers'])) {
                        $headers .= '"h'.$i.'",';
                    }
                }

                $js .= $headers ? ',['.$headers.'"RemoveFormat"]' : '';
                $js .= (!$headers && in_array('RemoveFormat', $settings['toolbar_icons'])) ? ',["RemoveFormat"]' : '';
                $js .= (isset($settings['styles']) and $settings['styles']) ? ',["Styles"]' : '';
                $js .= ',["Link","Unlink"';
                $js .= in_array('Anchor', $settings['toolbar_icons']) ? ',"Anchor"' : '';
                $js .= ',"Image"';
                $js .= in_array('MediaEmbed', $settings['toolbar_icons']) ? ',"MediaEmbed"' : '';
                $js .= in_array('Flash', $settings['toolbar_icons']) ? ',"Flash"' : '';
                $js .= in_array('Table', $settings['toolbar_icons']) ? ',"Table"' : '';
                $js .= in_array('Iframe', $settings['toolbar_icons']) ? ',"Iframe"' : '';
                $js .= ']';
            } elseif ($settings['toolbar'] == 'simple') {
                $js .= ',["Link","Unlink"]';
            }

            $js .= in_array('Maximize', $settings['toolbar_icons']) ? ',["Maximize"]' : '';

            if ($settings['toolbar'] != 'light') {
                $js .= in_array('ShowBlocks', $settings['toolbar_icons']) ? ',["ShowBlocks"]' : '';
                $js .= in_array('Source', $settings['toolbar_icons']) ? ',["Source"]' : '';
            }

            $js .= ']';
        }


        // styles
        if (isset($settings['styles']) and $settings['styles']) {
            $js .= ', stylesSet: ['.$settings['styles'].']';
        }

        $js .= '};';

        return $js;
    }

    // --------------------------------------------------------------------

    /**
      *  Get extra links from Channel URIs / Structure / NavEE / Pages
      */
    private function _get_extra_links()
    {
        // include expresso library
        require PATH_THIRD.'expresso/libraries/expresso_lib.php';
        $expresso_lib = new Expresso_lib();

        $links = array();

        // get channel uri entries
        if (!empty($this->settings['global']['channel_uris'])) {
            $entries = $expresso_lib->get_channel_uri_entries($this->settings['global']['channel_uris']);

            if (count($entries)) {
                $links[] = array('name' => 'Entries', 'links' => $entries);
            }
        }

        // can't order by specific values with active record so create sql manually
        $fields = implode(', ', array_map(create_function('$a', 'return "\'$a\'";'), $this->extra_link_modules));

        // check which if any of the modules are installed
        $sql = 'SELECT module_name FROM exp_modules WHERE module_name IN ('.$fields.') ORDER BY FIELD(module_name, '.$fields.')';

        $query = ee()->db->query($sql);

        if ($query and $query->num_rows) {
            foreach ($query->result() as $row) {
                $links[] = array('name' => $row->module_name, 'links' => $expresso_lib->get_page_links($row->module_name));
            }
        }

        return $links;
    }

    // --------------------------------------------------------------------

    /**
     *  Replace Tag
     */
    public function replace_tag($data, $params = array(), $tagdata = false)
    {
        $options = array(
       		'text_format' => 'none',
        	'html_format' => 'all'
        );
        if(!empty($this->row['channel_auto_link_urls']))
        {
        	$options['auto_links'] = $this->row['channel_auto_link_urls'];
        }
        if(!empty($this->row['channel_allow_img_urls']))
        {
        	$options['allow_img_url'] = $this->row['channel_allow_img_urls'];
        }
        $data = ee()->typography->parse_type(
            ee()->functions->encode_ee_tags($data),
            $options
        );

        // ensure params is an array
        $params = is_array($params) ? $params : array();

        // loop through parameters - order is important!
        foreach ($params as $key => $val) {
            if ($key == 'paragraph_limit') {
                if (substr_count($data, '</p>') > $val) {
                    $pos = $this->_strnposr($data, '</p>', $val);
                    $data = substr($data, 0, ($pos + 4));
                }
            } elseif ($key == 'strip_tags' && $val == 'true') {
                $allowable_tags = isset($params['allowable_tags']) ? $params['allowable_tags'] : '';
                $data = strip_tags($data, $allowable_tags);
            } elseif ($key == 'strip_line_breaks' && $val == 'true') {
                $data = str_replace(array("\n", "\r", "\t"), '', $data);
            } elseif ($key == 'word_limit') {
                $data = ee()->functions->word_limiter($data, $val);
            } elseif ($key == 'character_limit') {
                $data = ee()->functions->char_limiter($data, $val);
            }
        }

        // convert relative urls to absolute urls for all src attributes (removed in 4.2.9)
        //$data = str_replace('src="/', 'src="'.rtrim(ee()->config->item('site_url'), '/').'/', $data);

        return $data;
    }

    // --------------------------------------------------------------------

    /**
    *  Replace Content Element Tag
    */
    public function replace_element_tag($data, $params = array(), $tagdata = '')
    {
        $data = $this->replace_tag($data, $params);

        // if tagdata is empty then return data (for backwards compabitility)
        if (trim($tagdata) == '') {
            return $data;
        }

        $tagdata = ee()->TMPL->parse_variables($tagdata, array(
            array(
                'value' => $data,
                'element_name' => $this->element_name,
            )
        ));

        return $tagdata;
    }

    // --------------------------------------------------------------------

    /**
      *  Find the position of the nth occurrence of a substring in a string
      */
    private function _strnposr($haystack, $needle, $occurrence, $pos=0)
    {
        if ($occurrence <= 1) {
            return strpos($haystack, $needle, $pos);
        }

        $pos = strpos($haystack, $needle, $pos) + 1;
        $occurrence--;

        return $this->_strnposr($haystack, $needle, $occurrence, $pos);
    }

    // --------------------------------------------------------------------

    /**
      *  Display Global Settings
      */
    public function display_global_settings()
    {
        ee()->load->library('table');
        ee()->lang->loadfile('expresso');

        ee()->cp->set_right_nav(array(
            'export_settings' => '#export_settings',
            'import_settings' => '#import_settings'
        ));


        ee()->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.URL_THIRD_THEMES.'expresso/css/settings.css?v='.$this->info['version'].'" />');
        ee()->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.URL_THIRD_THEMES.'expresso/ckeditor_'.$this->ckeditor_version.'/skins/'.$this->ckeditor_skin.'/editor.css?v='.$this->info['version'].'" />');
        ee()->cp->load_package_js('global_settings');

        // export settings
        $export_settings = array();

        // get clean settings
        $settings = $this->_clean_settings($this->settings['global']);

        $vars['settings'] = array();

        // loop through default settings (in case new ones were added)
        foreach ($this->default_settings as $key => $val) {
            $val = isset($settings[$key]) ? $settings[$key] : $val;

            $export_settings[$key] = $val;

            switch ($key) {
                case 'license_number':
                    $vars['settings'][$key] = form_input(array(
                        'name' => $key,
                        'value' => $val,
                        'style' => ($this->_valid_license($val) ? '' : 'border-color: #CE0000;')
                    ));
                    $vars['license'] = $this->_valid_license($val) ? 'valid_license' : 'invalid_license';
                    break;
                case 'contentsCss':
                case 'custom_toolbar':
                case 'styles':
                    $vars['settings'][$key] = form_textarea($key, $val, 'class="'.$key.'"');
                    break;
                case 'toolbar_icons':
                    $vars['settings'][$key] = '<div class="toolbar_icons cke_ltr">';
                    foreach ($this->all_toolbar_icons as $icon) {
                        $vars['settings'][$key] .= '<div class="icon_set">'.form_checkbox('toolbar_icons[]', $icon, in_array($icon, $val)).' <span class="cke_button_icon cke_button__'.str_replace(' ', '_', strtolower($icon)).'_icon" style="background-image: url('.URL_THIRD_THEMES.'expresso/ckeditor_'.$this->ckeditor_version.'/skins/'.$this->ckeditor_skin.'/icons.png);" title="'.$icon.'"></span></div>';
                    }
                    $vars['settings'][$key] .= '</div>';
                    break;
                case 'headers':
                    $vars['settings'][$key] = '<div class="headers">';
                    for ($i = 1; $i <= 6; $i++) {
                        $vars['settings'][$key] .= '<div class="icon_set">'.form_checkbox('headers[]', 'h'.$i, in_array('h'.$i, $val)).'  <span class="cke_button_icon" style="background-image: url('.URL_THIRD_THEMES.'expresso/ckeditor_'.$this->ckeditor_version.'/plugins/headers/images/h'.$i.'.png);" title="H'.$i.'"></span></div>';
                    }
                    $vars['settings'][$key] .= '</div>';
                    break;
                case 'fullWidth':
                case 'startupOutlineBlocks':
                    $vars['settings'][$key] = '<div class="headers">'.form_checkbox($key, 'true', $val).' Enabled</div>';
                    break;
                case 'channel_uris':
                    // get all channels
                    ee()->db->where('site_id', ee()->config->item('site_id'));
                    ee()->db->order_by('channel_title', 'asc');
                    $channels = ee()->db->get('channels')->result();

                    $channel_uris = $val ? $val : array();
                    foreach ($channels as $channel) {
                        $channel->url = isset($channel_uris[$channel->channel_id]) ? $channel_uris[$channel->channel_id] : '';
                    }
                    $vars['settings'][$key] = $channels;
                    break;
                default:
                    $vars['settings'][$key] = form_input($key, $val);
                    break;
            }
        }


        // encode and serialize export settings
        $vars['export_settings'] = base64_encode(serialize(array('global' => $export_settings)));


        return ee()->load->view('global_settings', $vars, true);
    }

    // --------------------------------------------------------------------

    /**
      *  Save Global Settings
      */
    public function save_global_settings()
    {
        if (ee()->input->post('import')) {
            if (!$import_settings = unserialize(base64_decode(ee()->input->post('import_settings')))) {
                ee()->lang->loadfile('expresso');
                ee()->output->show_user_error('submission', lang('import_settings_error'));
            }

            return $import_settings;
        }

        $settings = array();

        // loop through default settings (in case new ones were added)
        foreach ($this->default_settings as $key => $val) {
            $settings[$key] = ee()->input->post($key) !== null ? ee()->input->post($key) : $val;

            // ensure empty values are allowed in arrays
            if (($key == 'headers' || $key == 'toolbar_icons') && !ee()->input->post($key)) {
                $settings[$key] = array();
            }
        }

        return array('global' => $settings);
    }

    // --------------------------------------------------------------------

    /**
      *  Display Settings
      */
    public function display_settings($settings)
    {
        $data = $this->_prepare_display_settings($settings);

        return array('field_options_expresso' => array(
            'label' => 'field_options',
            'group' => 'expresso',
            'settings' => $data
        ));
    }

    public function grid_display_settings($settings)
    {
        $data = $this->_prepare_display_settings($settings);

        return array('field_options' => $data);

    }

    private function _prepare_display_settings($settings)
    {
    	ee()->lang->loadfile('expresso');

        // get clean settings
        $settings = $this->_clean_settings($settings);

        // toolbar type
        $toolbar = isset($settings['toolbar']) ? $settings['toolbar'] : 'full';

        // css
        $contentsCss = isset($settings['contentsCss']) ? $settings['contentsCss'] : '';

        return array(
            array(
                'title' => 'toolbar',
                'fields' => array(
                    'toolbar' => array(
                        'type' => 'select',
                        'choices' => $this->toolbar_options,
                        'value' => $toolbar,
                    )
                )
            ),
            array(
                'title' => 'contentsCss',
                'fields' => array(
                    'contentsCss' => array(
                        'type' => 'textarea',
                        'value' => $contentsCss,
                    )
                )
            ),
        );
    }

    // --------------------------------------------------------------------

    /**
      *  Save Settings
      */
    public function save_settings($data)
    {
        $settings = array(
            'toolbar' => ee()->input->post('toolbar'),
            'height' => ee()->input->post('height'),
            'contentsCss' => ee()->input->post('contentsCss')
        );

        return $settings;
    }

    // --------------------------------------------------------------------

    /**
      *  Save Grid Settings
      */
    public function grid_save_settings($data)
    {
        return $data;
    }

    // --------------------------------------------------------------------

    /**
      *  Display Matrix Cell Settings
      */
    public function display_cell_settings($settings)
    {
        ee()->lang->loadfile('expresso');

        $settings['toolbar'] = isset($settings['toolbar']) ? $settings['toolbar'] : 'simple';

        return array(
            array(str_replace(' ', '&nbsp;', lang('toolbar')), form_dropdown('toolbar', $this->toolbar_options, $settings['toolbar']))
        );
    }

    // --------------------------------------------------------------------

    /**
      *  Display Content Element Settings
      */
    public function display_element_settings($settings)
    {
        ee()->lang->loadfile('expresso');

        $settings['toolbar'] = isset($settings['toolbar']) ? $settings['toolbar'] : 'simple';

        return array(
            array(str_replace(' ', '&nbsp;', lang('toolbar')), form_dropdown('toolbar', $this->toolbar_options, $settings['toolbar']))
        );
    }

    // --------------------------------------------------------------------

    /**
      *  Display Low Variable Field Settings
      */
    public function var_display_settings($settings)
    {
        ee()->lang->loadfile('expresso');

        $settings['toolbar'] = isset($settings['toolbar']) ? $settings['toolbar'] : 'simple';

        return array(
            array(str_replace(' ', '&nbsp;', lang('toolbar')), form_dropdown('toolbar', $this->toolbar_options, $settings['toolbar']))
        );
    }

    // --------------------------------------------------------------------

    /**
      *  Save Low Variable Field Settings
      */
    public function var_save_settings($data)
    {
        return $this->save_settings($data);
    }

    // --------------------------------------------------------------------

    /**
      *  Return Clean Settings
      */
    private function _clean_settings($settings)
    {
        // ensure settings is an array
        $settings = is_array($settings) ? $settings : array();

        foreach ($settings as $key => $val) {
            switch ($key) {
                case 'uiColor':
                    $settings[$key] = '#ECF1F4';
                    break;
                case 'height':
                    $settings[$key] = 80;
                    break;
                case 'toolbar':
                    $settings[$key] = ($val and array_key_exists($val, $this->toolbar_options)) ? $val : 'full';
                    break;
            }
        }

        // use default toolbar icon settings if not set
        $settings['toolbar_icons'] = isset($settings['toolbar_icons']) ? $settings['toolbar_icons'] : $this->default_settings['toolbar_icons'];

        return $settings;
    }

    // --------------------------------------------------------------------

    /**
      *  Checks if is valid license
      */
    private function _valid_license($string)
    {
        return preg_match("/^([a-z0-9]{8})-([a-z0-9]{4})-([a-z0-9]{4})-([a-z0-9]{4})-([a-z0-9]{12})$/", $string);
    }
}

/* End of file ft.expresso.php */
