<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * EntryModification Plugin
 *
 * @category    Plugin
 * @author      Zeth Copher
 * @link        https://midwesterninteractive.com
 */

$plugin_info = array(
    'pi_name'       => 'EntryModification',
    'pi_version'    => '0.1',
    'pi_author'     => 'Zeth Copher',
    'pi_author_url' => 'https://midwesterninteractive.com',
    'pi_description'=> 'Manually modifies data in an entry.',
    'pi_usage'      => Entrymodification::usage()
);


class Entrymodification {

    public $return_data;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->EE = get_instance();

        //who's trying to modify the entry

        if (!empty($_POST["entry_id"])) {

        //the entry to be modified
        $this->entry_id = $_POST["entry_id"];
        //the name of the field to be modified
        $this->field_name = $_POST["field_name"];
        //the value that is trying to be set
        $this->field_value = $_POST["field_value"];

        //the id of the field to be modified
        $this->field_id = ee()->db->select('field_id')
            ->from('channel_fields f')
            ->where(array(
                'f.field_name' => $this->field_name
            ))
            ->limit(1)
            ->get()
            ->row("field_id");
        } else {
            exit('No Entry Specified');
        }

    }

    public function modify()
    {   
        if ($this->field_name == 'status')
        {
            ee()->db->update(
                'channel_titles',
                array(
                    'status' => $this->field_value
                ),
                array (
                    'entry_id' => $this->entry_id
                )
            );
        } else {
             ee()->db->update(
                'channel_data',
                array(
                    "field_id_" . $this->field_id => $this->field_value
                ),
                array(
                    'entry_id' => $this->entry_id
                )
            );
        }
       

        return $this->field_value;
    }

    public function close()
    {

    }

    /**
     * Plugin Usage
     */
    public static function usage()
    {
        ob_start();
?>
<h3>EntryModification</h3>
<p>Manually modifies data in an entry.</p>
<hr/>
Include your documentation here...
<?php
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }
}


/* End of file pi.entrymodification.php */
/* Location: /system/expressionengine/third_party/entrymodification/pi.entrymodification.php */