<?php 
return array(
    'name'       => 'EntryModification',
    'version'    => '0.1',
    'author'     => 'Zeth Copher',
    'author_url' => 'https://midwesterninteractive.com',
    'description'=> 'Modifies Entries',
    'namespace'      => 'entrymodification/'
);