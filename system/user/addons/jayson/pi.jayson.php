<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * jayson Plugin
 *
 * @category    Plugin
 * @author      Midwestern Interactive @ryandoss
 * @link        midwesterninteractive.com
 */

$plugin_info = array(
    'pi_name'       => 'jayson',
    'pi_version'    => '0.1',
    'pi_author'     => 'Midwestern Interactive @ryandoss',
    'pi_author_url' => 'midwesterninteractive.com',
    'pi_description'=> '',
    'pi_usage'      => Jayson::usage()
);

class Jayson {

    public $return_data;
    private $json, $jason;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->EE = get_instance();
        $this->entry_id = ee()->TMPL->fetch_param("entry_id");
        $this->field = ee()->TMPL->fetch_param("field");

        $field_id = $this->EE->db->select('field_id')->from('exp_channel_fields')->where('field_name',$this->field)->get()->row('field_id');
        $this->jayson = $this->EE->db->select('field_id_'.$field_id)->from('exp_channel_data')->where('entry_id',$this->entry_id)->get()->row('field_id_'.$field_id);

        $this->json = json_decode($this->jayson);
    }

    public function pre($arr)
    {
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
    }

    public function options()
    {
        $options = 'Avialable Tag Options: ';

        foreach($this->json[0] as $key => $val)
        {
            $options .= $key.', ';
        }

        return substr($options,0,-2);
    }

    public function loop()
    {

        if (empty($this->json))
        {
            $data[] = array('no_results' => 'true');
        } else {
            $count = 1;
            foreach($this->json as $obj)
            {
                $obj->row_count = $count++;
                $data[] = (array) $obj;
            }
        }

        return $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $data);
    }

    /**
     * Plugin Usage
     */
    public static function usage()
    {
        ob_start();
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }
}


/* End of file pi.jayson.php */
/* Location: /system/expressionengine/third_party/jayson/pi.jayson.php */
