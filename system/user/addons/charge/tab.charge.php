<?php

// This is what we have to do for EE2 support
require_once 'addon.setup.php';

use EEHarbor\Charge\FluxCapacitor\FluxCapacitor;
use EEHarbor\Charge\FluxCapacitor\Base\Tab;

/**
 * ExpressionEngine - by EllisLab
 *
 * @package     ExpressionEngine
 * @author      EllisLab Dev Team
 * @copyright   Copyright (c) 2003 - 2016, EllisLab, Inc.
 * @license     https://expressionengine.com/license
 * @link        http://ellislab.com
 * @since       Version 2.0
 * @filesource
 */

// --------------------------------------------------------------------

/**
 * ExpressionEngine Charge Module
 *
 * @package     ExpressionEngine
 * @subpackage  Modules
 * @category    Modules
 * @author      EllisLab Dev Team
 * @link        http://ellislab.com
 */
class Charge_tab extends Tab
{
    public function __construct()
    {
        parent::__construct();
    }
}
