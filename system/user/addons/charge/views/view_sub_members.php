<div id="charge_container" class="mor ee<?=$ee_ver?>">



	<div class="tg">
		<h2>Subscription Members</h2>
		<div class="inlinealert">
			<p>Members are only added to subscriptions after a successful transaction, based on the "on_success" action. <br /><a href="https://eeharbor.com/charge/documentation/subscriptions">View the documentation on how Subscriptions work</a>.</p>
			<p>When their subscription ends, their status will change to "inactive" and their user will be moved to the "subscription failure group".</p>
			<p>Subscriptions set to cancel "at_period_end" will be "active" until their subscription runs out.</p>
		</div>
	</div>

	<div class="tg">
		<h2><?=$subscription['name']?></h2>
		<div class="inlinealert">
			<p><strong><?=$member_count?></strong> members in this subscription</p>
			<p><?=$subscription['auto_description']?></p>
		</div>

		<table class="data">
			<thead>
				<tr style="background:transparent">
					<th>Id</th>
					<th>Member Id</th>
					<th>Username</th>
					<th>Screen Name</th>
					<th>Email</th>
					<th>Status</th>
					<th>Stripe Customer Id</th>
					<th>Subscription Start Date</th>
					<th>Last Stripe Contact</th>
				</tr>
			</thead>
			<tbody>

				<?php if(count($members) == '0') : ?>
					<tr>
						<td colspan="8">
							No members currently in this subscription
						</td>
					</tr>
				<?php endif; ?>

				<?php foreach($members as $member) : ?>
					<tr>
						<td><?=$member['subscription_member_id']?></td>

						<td><a href="<?=$members_cp_uri.$member['member_id']?>"><?=$member['member_id']?></a></td>
						<td><a href="<?=$members_cp_uri.$member['member_id']?>"><?=$member['username']?></a></td>
						<td><?=$member['screen_name']?></td>
						<td><?=$member['email']?></td>

						<td><?=$member['status']?></td>
						<td><a target="_blanks" href="https://dashboard.stripe.com/customers/<?=$member['customer_id']?>"><?=$member['customer_id']?></a></td>

						<td><?=date('Y/m/d', $member['timestamp']).' at '.date('H:i', $member['timestamp'])?></td>
						<td>
							<?php if($member['last_contact'] == '0') : ?>
								<span class="gicon gicon-warning-sign error"></span> Never
							<?php else : ?>
								<?=date('Y/m/d', $member['last_contact']).' at '.date('H:i', $member['last_contact'])?>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach ?>

			</tbody>
		</table>
	</div>
	<?php if($has_pagination) : ?>
		<ul class="pagination">

			<li>Page <?=$current_page?> of <?=$total_pages?></li>
			<li>
				<?php if( $prev_link ) : ?>
					<a href="<?=$prev_link?>" title="Previous page">Previous</a>
				<?php else : ?>
					<b>Previous</b>
				<?php endif; ?>
			</li>
			<li>
				<?php if( $next_link ) : ?>
					<a href="<?=$next_link?>" title="Next page">Next</a>
				<?php else : ?>
					<b>Next</b>
				<?php endif; ?>
			</li>
		</ul>
	<?php endif; ?>
</div>