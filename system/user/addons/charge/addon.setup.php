<?php

    require_once 'autoload.php';
    $addonJson = json_decode(file_get_contents(__DIR__ . '/addon.json'));

    return array(
        'name'               => $addonJson->name,
        'description'        => $addonJson->description,
        'version'            => $addonJson->version,
        'namespace'          => $addonJson->namespace,
        'author'             => 'EEHarbor',
        'author_url'         => 'https://eeharbor.com/charge',
        'docs_url'           => 'https://eeharbor.com/charge/documentation',
        'settings_exist'     => true,
        'charge_api_version' => '2019-08-14',
        'fieldtypes'         => array(
            'charge' => array(
                'name' => 'Charge',
                'compatibility' => 'text'
            )
        )
    );
