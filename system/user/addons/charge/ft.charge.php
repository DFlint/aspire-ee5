<?php

// This is what we have to do for EE2 support
require_once 'addon.setup.php';

use EEHarbor\Charge\FluxCapacitor\FluxCapacitor;
use EEHarbor\Charge\FluxCapacitor\Base\Ft;

class Charge_ft extends Ft
{
    public $has_array_data = true;

    public $charge_fields = array(
        'charge_type' => 'select',
        'plan_amount_type' => 'select',
        'plan_amount' => 'text',
        'plan_amount_group' => 'member_group',
        // 'plan_amount_discount' => 'text',
        'plan_set_name' => 'text',
        'plan_trial_days' => 'text',
        'plan_length' => 'text',
        'plan_length_interval' => 'select',
        'on_success' => 'select',
        'create_member' => 'enum',
        'plan_balance' => 'text',
        'plan_interval_count' => 'text',
        'plan_interval' => 'select',
    );

    public $charge_field_data = array(
        'charge_type' => array('charge'=>'One-Off', 'recurring'=>'Recurring'),
        'plan_amount_type' => array('single'=>'Single Price', 'member_group'=>'Per Member Group Pricing'),
        'plan_interval' => array(''=>'-None-', 'week'=>'Week', 'month'=>'Month', 'year'=>'Year'),
        'plan_length_interval' => array(''=>'-None-', 'week'=>'Week', 'month'=>'Month', 'year'=>'Year')
    );

    public $charge_fields_default_hidden = array('plan_amount_group', 'plan_interval', 'plan_interval_count', 'plan_balance');

    public function __construct()
    {
        parent::__construct();

        // Add Package Path
        // ee()->load->add_package_path(PATH_THIRD.'charge/');

        // -------------------------------------------
        //  Prepare Cache
        // -------------------------------------------

        if (! isset(ee()->session->cache['charge'])) {
            ee()->session->cache['charge'] = array();
        }

        $this->cache =& ee()->session->cache['charge'];

        ee()->lang->loadfile('charge');

        // -------------------------------------------
        //  Get lib
        // -------------------------------------------
        ee()->load->library('charge_model');
    }

    /**
     * Display Global Settings
     *
     * @access  public
     * @return  form contents
     *
     */
    public function display_global_settings()
    {
        $val = array_merge($this->settings, $_POST);

        return;
    }


    /**
     * Save Global Settings
     *
     * @access  public
     * @return  global settings
     *
     */
    public function save_global_settings()
    {
        return array_merge($this->settings, $_POST);
    }


    /**
     * Save Settings
     *
     * @access  public
     * @return  field settings
     *
     */
    public function save_settings($data)
    {
        return array(
            'field_wide' => true
        );
    }


    /**
     * Display Field on Publish
     *
     * @access  public
     * @param   existing data
     * @return  field html
     *
     */
    public function display_field($data)
    {
        $form = '<style>'."\n";
        $form .= '.charge_fields { border: 0; }'."\n";
        $form .= '.charge_fields tr:nth-child(odd) td { background-color:#fcfcfc; }'."\n";
        $form .= '.charge_fields.ee2 tr:nth-child(odd) td { background-color:#ECF1F4; }'."\n";
        $form .= '.charge_fields tr:nth-child(even) td { background-color:#fff; }'."\n";
        $form .= '.charge_fields tr#create_member_row td { border-bottom: 0; }'."\n";
        $form .= '.charge_fields tr#recurring_header_row td { border-top: 1px solid #e6e6e6; }'."\n";
        $form .= '.charge_fields.ee2 tr#recurring_header_row td { border-top: 0; }'."\n";
        $form .= '.charge_fields td { color:#808080; font-style:italic; font-size: 12px; vertical-align:top; padding: 5px 10px; white-space: normal; }'."\n";
        $form .= '.charge_fields td label { display:block; color:#555; margin-bottom: 3px; padding: 0; }'."\n";
        $form .= '.charge_fields.ee2 td label { color:#5f6c74; }'."\n";
        $form .= '.charge_fields td label strong { font-style:initial; }'."\n";
        $form .= '.charge_fields td input[type="text"] { min-width:150px; }'."\n";
        $form .= '.charge_fields td .bg-warning { padding: 5px 10px; background-color: #FFDD7C; }'."\n";
        $form .= '.charge_fields tr#recurring_header_row td { padding:10px 0; font-size: 14px; color: #000; font-weight: bold; }'."\n";
        $form .= '</style>'."\n";
        $form .= '<table class="charge_fields ee'.$this->flux->getEEVersion().'" cellspacing="0" cellpadding="3" width="100%">'."\n";

        $entry_id = $this->content_id;
        $post_data = ee()->input->post($this->field_name);

        // Get any Charge Actions we have in the system to populate the dropdown.
        $this->charge_field_data['on_success'] = array();
        $success_action_q = ee()->db->get('charge_action');

        // If there are no Charge Actions, change the field type to a 'message' and display a warning.
        if ($success_action_q->num_rows() == 0) {
            $this->charge_fields['on_success'] = 'message';
            $this->charge_field_data['on_success'] = '<div class="bg-warning"><label><strong>'.lang('on_success_no_actions_title').'</strong></label>'.lang('on_success_no_actions_text').'</div>';
        } else {
            // Otherwise pre-populate the dropdown with the available actions.
            $this->charge_field_data['on_success'][''] = lang('on_success_no_actions');
            foreach ($success_action_q->result() as $success_action) {
                $this->charge_field_data['on_success'][$success_action->shortname] = $success_action->name;
            }
        }

        foreach ($this->charge_fields as $field_name => $field_type) {
            $field_data = '';

            if (isset($post_data[$field_name])) {
                $field_data = $post_data[$field_name];
            } elseif ($entry_id) {
                $field_row = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name))->row();
                if (!empty($field_row)) {
                    $field_data = $field_row->field_data;
                }
            }

            // Check if the Charge Type is a charge or recurring so we can set the default hidden state to prevent the flash before the JS engages.
            if ($field_name == 'charge_type' && $field_data == 'recurring') {
                unset($this->charge_fields_default_hidden[array_search('plan_interval', $this->charge_fields_default_hidden)]);
                unset($this->charge_fields_default_hidden[array_search('plan_interval_count', $this->charge_fields_default_hidden)]);
                unset($this->charge_fields_default_hidden[array_search('plan_balance', $this->charge_fields_default_hidden)]);
            }

            // Check if the Pricing Type is a single price or per-member group price so we can set the default hidden state.
            if ($field_name == 'plan_amount_type' && $field_data == 'member_group') {
                $this->charge_fields_default_hidden[] = 'plan_amount';
                unset($this->charge_fields_default_hidden[array_search('plan_amount_group', $this->charge_fields_default_hidden)]);
            }

            if ($field_name == 'plan_balance') {
                $form .= '<tr '.(in_array($field_name, $this->charge_fields_default_hidden) ? 'style="display:none" class="hidden"' : 'class="visible"').' id="recurring_header_row"><td colspan="2">'.lang('recurring_heading').'</td></tr>'."\n";
            }

            $form .= '<tr '.(in_array($field_name, $this->charge_fields_default_hidden) ? 'style="display:none" class="hidden"' : 'class="visible"').' id="'.$field_name.'_row"><td>'."\n";
            $form .= '<label for="'.$this->field_name.'['.$field_name.']"><strong>'.lang($field_name).'</strong></label>'."\n";
            if (lang($field_name.'_instructions') != $field_name.'_instructions') {
                $form .= lang($field_name.'_instructions')."\n";
            }
            $form .= '</td><td>'."\n";

            switch ($field_type) {
                case 'message':
                    $form .= $this->charge_field_data[$field_name]."\n";
                    break;

                case 'text':
                    $form .= form_input($this->field_name.'['.$field_name.']', !empty($field_data) ? $field_data : '', 'id="'.$field_name.'"')."\n";
                    break;

                case 'enum':
                    $form .= '<label>'.form_checkbox($this->field_name.'['.$field_name.']', 'yes', (!empty($field_data) and $field_data == 'yes' ? true : false)).' '.lang($field_name).'</label>'."\n";
                    break;

                case 'select':
                    $form .= form_dropdown($this->field_name.'['.$field_name.']', $this->charge_field_data[$field_name], !empty($field_data) ? $field_data : '', 'id="'.$field_name.'"')."\n";
                    break;

                case 'member_group':
                    ee()->db->where('is_locked', 'n');
                    ee()->db->or_where('group_title', 'Guests');
                    $member_group_q = ee()->db->get('member_groups');

                    // Loop through the member groups and retrieve the saved data for them (if any).
                    foreach ($member_group_q->result() as $member_group) {
                        // Override the field name with the member's group ID and retrieve the value from the posted data or the database.
                        $field_name = 'plan_amount_group_'.$member_group->group_id;
                        $field_data = $post_data[$field_name];

                        if (empty($field_data)) {
                            $field_row = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name))->row();
                            if (!empty($field_row)) {
                                $field_data = $field_row->field_data;
                            }
                        }

                        $form .= '<label for="'.$field_name.'"><strong>'.$member_group->group_title.'</strong></label>'."\n";
                        $form .= form_input($this->field_name.'['.$field_name.']', !empty($field_data) ? $field_data : '', 'id="'.$field_name.'"')."\n";
                    }
                    break;
            }

            $form .= '</td></tr>'."\n";
        }

        $form .= '</table>'."\n";

        ee()->javascript->output(array(
            "checkPlanType();"."\n",
            "restripeRows();"."\n",
            "$('#charge_type').on('change', function() { checkPlanType(1); });"."\n",
            "function checkPlanType(restripe) {"."\n",
            "  if($('#charge_type').val() == 'recurring') {"."\n",
            "    $('#recurring_header_row, #plan_interval_row, #plan_interval_count_row, #plan_balance_row').show().removeClass('hidden').addClass('visible');"."\n",
            "  } else {"."\n",
            "    $('#recurring_header_row, #plan_interval_row, #plan_interval_count_row, #plan_balance_row').hide().removeClass('visible').addClass('hidden');"."\n",
            "  }"."\n",
            "  if(restripe === 1) restripeRows();"."\n",
            "}"."\n",
            "\n",
            "checkPlanAmount();"."\n",
            "$('#plan_amount_type').on('change', function() { checkPlanAmount(1); });"."\n",
            "function checkPlanAmount(restripe) {"."\n",
            "  if($('#plan_amount_type').val() == 'member_group') {"."\n",
            "    $('#plan_amount_row').hide().removeClass('visible').addClass('hidden');"."\n",
            "    $('#plan_amount_group_row').show().removeClass('hidden').addClass('visible');"."\n",
            "  } else {"."\n",
            "    $('#plan_amount_group_row').hide().removeClass('visible').addClass('hidden');"."\n",
            "    $('#plan_amount_row').show().removeClass('hidden').addClass('visible');"."\n",
            "  }"."\n",
            "  if(restripe === 1) restripeRows();"."\n",
            "}"."\n",
            "\n",
            "function restripeRows() {"."\n",
            "  $('.charge_fields tr:visible:odd td').css('background', '#fcfcfc');"."\n",
            "  $('.charge_fields.ee2 tr:visible:odd td').css('background', '#ECF1F4');"."\n",
            "  $('.charge_fields tr:visible:even td').css('background', '#fff');"."\n",
            "}"."\n"
        ));

        return $form;
    }


    /**
     * Validate the posted data.
     *
     * @param mixed $data
     *
     * @return mixed $result Error message in case of failed validation.
     */
    public function validate($data)
    {
        return true;
    }


    public function save($data)
    {
        if (is_array($data)) {
            ee()->load->helper('custom_field');
            $data = encode_multi_field($data);
        }

        return $data;
    }


    /**
     * Prep data for saving
     *
     * @access  public
     * @param   submitted field data
     * @return  string to save
     */
    public function post_save($data)
    {
        $channelData = array();
        $entry_id = $this->content_id;
        $post_data = ee()->input->post($this->field_name);

        // Loop through each of our charge fields and pull them from the posted data.
        foreach ($this->charge_fields as $field_name => $field_type) {
            // Determine if we're using a single price or price based on what member group the customer is in.
            if ($field_type == 'member_group') {
                ee()->db->where('is_locked', 'n');
                ee()->db->or_where('group_title', 'Guests');
                $member_group_q = ee()->db->get('member_groups');

                // Loop through the member groups and save out the pricing for each.
                foreach ($member_group_q->result() as $member_group) {
                    // Override the field name with the member's group ID and retrieve the value from the posted data.
                    $field_name = 'plan_amount_group_'.$member_group->group_id;
                    $field_data = $post_data[$field_name];

                    // Find out if this field is already in our table, if so, update it, if not, create it.
                    $exists = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name))->row();

                    // If the user didn't input any data and we don't already have this field saved, ignore it.
                    if (empty($field_data) && empty($exists)) {
                        continue;
                    }

                    // If there is no field_data, remove it from our table to prevent clutter, otherwise, insert or update it.
                    if (empty($field_data)) {
                        ee()->db->delete('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name));
                    } else {
                        if (empty($exists)) {
                            ee()->db->set('entry_id', $entry_id);
                            ee()->db->set('field_name', $field_name);
                            ee()->db->set('field_data', $field_data);
                            ee()->db->insert('charge_field_data');
                        } else {
                            ee()->db->where('entry_id', $entry_id);
                            ee()->db->where('field_name', $field_name);
                            ee()->db->update('charge_field_data', array('field_data'=>$field_data));
                        }
                    }
                }
            } else {
                // Pull the field value from the posted data.
                $field_data = $post_data[$field_name];

                // Find out if this field is already in our table, if so, update it, if not, create it.
                $exists = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name))->row();

                // If the user didn't input any data and we don't already have this field saved, ignore it.
                if (empty($field_data) && empty($exists)) {
                    continue;
                }

                // If there is no field_data, remove it from our table to prevent clutter, otherwise, insert or update it.
                if (empty($field_data)) {
                    ee()->db->delete('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name));
                } else {
                    if (empty($exists)) {
                        ee()->db->set('entry_id', $entry_id);
                        ee()->db->set('field_name', $field_name);
                        ee()->db->set('field_data', $field_data);
                        ee()->db->insert('charge_field_data');
                    } else {
                        ee()->db->where('entry_id', $entry_id);
                        ee()->db->where('field_name', $field_name);
                        ee()->db->update('charge_field_data', array('field_data'=>$field_data));
                    }
                }
            }
        }

        return;
    }

    /**
     * Flag that this fieldtype can be updated.
     *
     * @access  public
     * @return  boolean (true)
     */
    public function update($version = '')
    {
        return true;
    }
}
