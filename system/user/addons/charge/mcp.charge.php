<?php

// This is what we have to do for EE2 support
require_once 'addon.setup.php';

use EEHarbor\Charge\FluxCapacitor\FluxCapacitor;
use EEHarbor\Charge\FluxCapacitor\Base\Mcp;

/**
 * Charge MCP Class
 *
 * @package         charge_ee_addon
 * @author          Tom Jaeger <Tom@EEHarbor.com>
 * @link            https://eeharbor.com/charge
 * @copyright       Copyright (c) 2016, Tom Jaeger/EEHarbor
 */

class Charge_mcp extends Mcp
{
    public $cached_vars;
    public $limit = '35';

    private $nocache;
    private $_theme_url;
    private $settings;
    private $last_segment;

    public function __construct()
    {
        parent::__construct();

        $this->base = $this->flux->getBaseURL();
        $this->settings = $this->flux->getSettings();

        $this->_theme_url = $this->_get_package_theme_url();
        $this->cached_vars = array();
        $this->cached_vars['ee_ver'] = substr(APP_VER, 0, 1);
        $this->last_segment = ee()->uri->segment(ee()->uri->total_segments());

        $this->cached_vars['encrypt_key_set'] = false;
        if (ee()->config->item('encryption_key') != '') {
            $this->cached_vars['encrypt_key_set'] = true;
        }

        $this->_add_morphine();
        $this->_check_csrf_exempt();

        // Load our helper
        ee()->load->helper('Charge');
        ee()->lang->loadfile('charge');

        // Load base model
        if (!class_exists('Charge_model')) {
            ee()->load->library('Charge_model');
        }
        if (!isset(ee()->charge_stripe)) {
            ee()->charge_model->load_models($this->flux);
        }
    }


    public function index()
    {
        ee()->view->cp_page_title = lang('charge_module_name');

        $item_count = ee()->charge_stripe->count_all();

        $offset = $this->_pagination($item_count, 'index');

        $this->cached_vars['charges'] = ee()->charge_stripe->get_all($this->limit, $offset, false);

        $this->_get_overview();

        $this->cached_vars['export_uri'] = $this->flux->moduleURL('export');

        if (ee()->addons_model->module_installed('charge_digital')) {
            ee()->load->add_package_path(PATH_THIRD . 'charge_digital');
            ee()->load->library('charge_digital_lib');
            $this->cached_vars['licenses'] = ee()->charge_digital_lib->get_licenses();
        }

        return $this->flux->view('index', $this->cached_vars, true);
    }


    public function export()
    {
        $output = $this->_get_export_data();

        $this->_download_send_headers("charge_export_" . date("Y-m-d") . ".csv");
        echo $this->_array_to_csv($output);
        exit();
    }


    public function coupons()
    {
        ee()->view->cp_page_title = lang('charge_coupons');

        $this->cached_vars['add_coupon_uri'] = $this->flux->moduleURL('view_coupon', array('new'=>'yes'));
        $this->cached_vars['coupons'] = ee()->charge_coupon->get_all();

        $this->cached_vars['view_single_coupon_uri'] = $this->flux->moduleURL('view_coupon', array('id'=>''));
        $this->cached_vars['delete_coupon_uri'] = $this->flux->moduleURL('delete_coupon', array('id'=>''));

        return $this->flux->view('coupons', $this->cached_vars, true);
    }

    public function view_coupon()
    {
        $code = 'ANOTHER_TEST ASDF';
        echo preg_replace('/[^a-zA-Z0-9_\-]+/', '', $code);

        // --------------------------------------
        // Load some libraries
        // --------------------------------------
        ee()->load->library('javascript');
        ee()->cp->add_js_script(array('ui' => array('core', 'datepicker')));
        ee()->javascript->output(array('$( ".datepicker" ).datepicker({ dateFormat: \'yy-mm-dd\' });'));

        ee()->cp->set_breadcrumb($this->base, lang('charge_module_name'));

        ee()->view->cp_page_title = lang('charge_coupons');
        $this->cached_vars['form_post_url'] = $this->flux->moduleURL('view_coupon');
        $this->cached_vars['errors'] = array();

        $this->cached_vars['supported_currencies'] = ee()->charge_stripe->get_currencies();

        // Get the list of members in the system to potentially limit this coupon use to.
        $this->cached_vars['members'] = array();
        $this->cached_vars['members'][0] = '- No Member Restriction -';
        $members = ee()->db->select('member_id, screen_name, email')->get('members')->result_array();

        foreach ($members as $member) {
            $this->cached_vars['members'][$member['member_id']] = $member['screen_name'].' ('.$member['email'].')';
        }

        // Get a list of all the open entries that have Charge fieldtypes installed.
        $this->cached_vars['products'] = array();
        $this->cached_vars['products'][0] = '- No Product Restriction -';

        // For EE4, this is a bit more complictated
        if ($this->flux->ver_gte(4)) {
            // ee('Model')->get('ChannelEntry')
   //              ->with('Channel', 'ChannelField', 'ChannelFieldGroup')
   //              ->filter('ChannelFieldGroup.id', '==', 'something')
   //              ->fields('Channel.*', 'entry_id', 'title', 'channel_id')
   //              ->all()
   //              ->indexBy('entry_id');
   //
   //              $channels = ee('Model')->get('Channel', $channel_ids)
                // ->with('CustomFields')
                // ->with('FieldGroups')
                // ->all();

            // echo 'VARDUMP:<pre>';
            // var_dump($entries);
            // echo '</pre>';
            // exit;
        } else {
            ee()->db->select('channel_titles.entry_id, channel_titles.title');
            ee()->db->from('channel_titles');
            ee()->db->join('channels', 'channels.channel_id=channel_titles.channel_id', 'left');
            ee()->db->join('channel_fields', 'channel_fields.group_id=channels.field_group', 'left');
            ee()->db->where('channel_fields.field_type', 'charge');
            $entries = ee()->db->get();

            foreach ($entries->result() as $entry) {
                $this->cached_vars['products'][$entry->entry_id] = $entry->title;
            }
        }

        $new_coupon = false;

        $id = ee()->input->get_post('id');
        if ($id != '') {
            $coupon = ee()->charge_coupon->get_one(ee()->input->get_post('id'));
        } else {
            $new_coupon = true;
            $coupon = array('name' => '', 'code' => '', 'settings' => array(), 'stripe_id' => '', 'coupon_id' => '', 'type' => 'fixed');
        }

        if (ee()->input->post('edit_coupon') == 'yes') {
            // This is a post
            $status = ee()->charge_coupon->save($id);
            if ($status == false) {
                // Validation failed
                $this->cached_vars['errors'] = ee()->charge_coupon->errors;
                $this->cached_vars['has_errors'] = true;

                foreach (ee()->charge_coupon->this_settings as $key => $val) {
                    if (in_array($key, array('name', 'code'))) {
                        $coupon[$key] = $val;
                    } else {
                        $coupon[$key] = $val;
                    }
                }
            } else {
                // Good
                $this->flux->flashData('message_success', 'Coupon Saved', 'Coupon Saved Successfully!');
                ee()->functions->redirect($this->flux->moduleURL('coupons'));
                exit();
            }
        }

        if (empty($coupon)) {
            ee()->functions->redirect($this->flux->moduleURL('coupons'));
            exit();
        }

        // Check if the dates are zero so we don't pass 0000-00-00 to the form.
        if (!isset($coupon['start_date']) || $coupon['start_date'] == 0) {
            $coupon['start_date'] = '';
        }
        if (!isset($coupon['end_date']) || $coupon['end_date'] == 0) {
            $coupon['end_date'] = '';
        }

        $this->cached_vars['coupon'] = $coupon;

        return $this->flux->view('add_coupon', $this->cached_vars, true);
    }


    public function clear_logs()
    {
        ee()->charge_log->clear_all();

        ee()->functions->redirect($this->flux->moduleURL('settings'));
    }

    public function logs()
    {
        ee()->view->cp_page_title = lang('charge_logs');

        $this->cached_vars['members_cp_uri'] = BASE . '&C=myaccount&id=';

        $item_count = ee()->charge_log->count_all();

        $this->limit = '25';

        $offset = $this->_pagination($item_count, 'logs');


        $this->cached_vars['logs'] = ee()->charge_log->get_all_threaded($this->limit, $offset);

        $member_ids = array('0');
        foreach ($this->cached_vars['logs'] as $set) {
            foreach ($set as $log) {
                $member_ids[$log['member_id']] = $log['member_id'];
            }
        }


        $this->cached_vars['members'] = $this->_get_members($member_ids);

        return $this->flux->view('logs', $this->cached_vars, true);
    }

    public function actions()
    {
        ee()->view->cp_page_title = lang('charge_actions');

        $this->cached_vars['add_action_uri'] = $this->flux->moduleURL('view_action', array('new'=>'yes'));
        $this->cached_vars['actions'] = ee()->charge_action->get_all();

        $this->cached_vars['view_single_action_uri'] = $this->flux->moduleURL('view_action', array('id'=>''));
        $this->cached_vars['delete_action_uri'] = $this->flux->moduleURL('delete_action', array('id'=>''));

        return $this->flux->view('actions', $this->cached_vars, true);
    }


    public function subscriptions()
    {
        ee()->view->cp_page_title = lang('charge_subscriptions');

        $this->cached_vars['view_single_subscription_uri'] = $this->flux->moduleURL('view_subscription', array('id'=>''));
        $this->cached_vars['subscriptions'] = ee()->charge_subscription->get_all();
        $this->cached_vars['add_subscription_uri'] = $this->flux->moduleURL('view_subscription', array('new'=>'yes'));
        $this->cached_vars['view_sub_members_uri'] = $this->flux->moduleURL('view_sub_members', array('id'=>''));
        $this->cached_vars['delete_sub_uri'] = $this->flux->moduleURL('delete_sub', array('id'=>''));
        $this->cached_vars['actions_uri'] = $this->flux->moduleURL('actions');

        return $this->flux->view('subscriptions', $this->cached_vars, true);
    }


    public function view_sub_members()
    {
        ee()->view->cp_page_title = lang('charge_subscription_members');

        $this->cached_vars['subscription'] = ee()->charge_subscription->get_one(ee()->input->get('id'));
        $this->cached_vars['members_cp_uri'] = BASE . '&C=myaccount&id=';

        $this->cached_vars['members'] = ee()->charge_subscription_member->get_all();

        $sub_id =  ee()->input->get('id');
        ee()->db->where('subscription_id', $sub_id);
        $member_count = ee()->charge_subscription_member->count_all();

        $offset = $this->_pagination($member_count, 'view_sub_members', array('id' => $sub_id));

        ee()->db->where('subscription_id', ee()->input->get('id'));
        $this->cached_vars['members'] = ee()->charge_subscription_member->get_all($this->limit, $offset, false);

        $this->cached_vars['member_count'] = $member_count;

        return $this->flux->view('view_sub_members', $this->cached_vars, true);
    }


    public function webhooks()
    {
        ee()->view->cp_page_title = lang('charge_webhooks');

        $item_count = ee()->charge_webhook->count_all();

        $this->limit = '25';

        $offset = $this->_pagination($item_count, 'logs');

        $this->cached_vars['logs'] = ee()->charge_webhook->get_all($this->limit, $offset);

        return $this->flux->view('webhooks', $this->cached_vars, true);
    }


    public function delete_sub()
    {
        $subscription = ee()->charge_subscription->get_one(ee()->input->get_post('id'));

        if (empty($subscription)) {
            ee()->functions->redirect($this->flux->moduleURL('subscriptions'));
        }

        ee()->charge_subscription->delete($subscription['subscription_id']);

        ee()->charge_subscription_member->delete($subscription['subscription_id'], 'subscription_id');

        ee()->functions->redirect($this->flux->moduleURL('subscriptions'));
    }


    public function delete_action()
    {
        $action = ee()->charge_action->get_one(ee()->input->get_post('id'));

        if (empty($action)) {
            ee()->functions->redirect($this->flux->moduleURL('actions'));
        }

        ee()->charge_action->delete($action['action_id']);

        ee()->functions->redirect($this->flux->moduleURL('actions'));
    }


    public function delete_coupon()
    {
        $coupon = ee()->charge_coupon->get_one(ee()->input->get_post('id'));

        if (empty($coupon)) {
            ee()->functions->redirect($this->flux->moduleURL('coupons'));
        }

        ee()->charge_coupon->delete($coupon['coupon_id']);

        $this->flux->flashData('message_success', 'Coupon Deleted', 'Coupon Deleted Successfully!');
        ee()->functions->redirect($this->flux->moduleURL('coupons'));
    }

    // --------------------------------------------------------------------

    /**
     * Module home page
     *
     * @access  public
     * @return  string
     */
    public function settings()
    {
        // --------------------------------------
        // Load some libraries
        // --------------------------------------
        ee()->load->library('javascript');

        ee()->view->cp_page_title = lang('charge_settings');
        ee()->cp->set_breadcrumb($this->base, lang('charge_module_name'));

        $this->cached_vars['base_url'] = $this->flux->moduleURL('save_settings');
        $this->cached_vars['webhook_log_uri'] = $this->flux->moduleURL('webhooks');
        $this->cached_vars['currencies'] = ee()->charge_stripe->get_currencies();
        $this->cached_vars['clear_logs_url'] = $this->flux->moduleURL('clear_logs');

        $this->_get_callback_urls();

        foreach ($this->settings as $setting_name => $setting_value) {
            $this->cached_vars[$setting_name] = $setting_value;
        }

        if ($this->cached_vars['charge_log_level'] == '') {
            $this->cached_vars['charge_log_level'] = '5';
        }

        $default_site_email_settings = ee()->charge_email->get_site_defaults();
        foreach (array('charge_email_send_from' => 'from', 'charge_email_send_name' => 'name', 'charge_email_send_reply_to' => 'reply_to', 'charge_email_send_reply_to_name' => 'reply_to_name') as $key => $val) {
            if ($this->cached_vars[$key] == '') {
                $this->cached_vars[$key] = $default_site_email_settings[$val];
            }
        }

        $this->cached_vars['sections'] = $this->getSettingsForm();
        $this->cached_vars['save_btn_text'] = lang('btn_save_settings');
        $this->cached_vars['save_btn_text_working'] = 'btn_saving';

        // Grab the email settings if not already set in preferences
        return $this->flux->view('settings', $this->cached_vars, true);
    }


    public function save_settings()
    {
        $data = array();

        $data['site_id'] = ee()->config->item('site_id');
        $data['charge_stripe_account_mode'] = ee()->input->post('charge_stripe_account_mode', true);
        $data['charge_stripe_test_credentials_sk'] = ee()->input->post('charge_stripe_test_credentials_sk', true);
        $data['charge_stripe_test_credentials_pk'] = ee()->input->post('charge_stripe_test_credentials_pk', true);
        $data['charge_stripe_live_credentials_sk'] = ee()->input->post('charge_stripe_live_credentials_sk', true);
        $data['charge_stripe_live_credentials_pk'] = ee()->input->post('charge_stripe_live_credentials_pk', true);
        $data['charge_stripe_currency'] = ee()->input->post('charge_stripe_currency', true);
        // $data['charge_webhook_key'] = ee()->input->post('charge_webhook_key', TRUE);
        $data['charge_force_ssl'] = ee()->input->post('charge_force_ssl', true);
        $data['charge_metadata_pass'] = ee()->input->post('charge_metadata_pass', true);
        $data['charge_email_send_from'] = ee()->input->post('charge_email_send_from', true);
        $data['charge_email_send_name'] = ee()->input->post('charge_email_send_name', true);
        $data['charge_email_send_reply_to'] = ee()->input->post('charge_email_send_reply_to', true);
        $data['charge_email_send_reply_to_name'] = ee()->input->post('charge_email_send_reply_to_name', true);
        $data['charge_log_level'] = ee()->input->post('charge_log_level', true);

        // Find out if the settings exist, if not, insert them.
        ee()->db->where('site_id', ee()->config->item('site_id'));
        $exists = ee()->db->count_all_results('charge_settings');

        if ($exists) {
            ee()->db->where('site_id', ee()->config->item('site_id'));
            ee()->db->update('charge_settings', $data);
        } else {
            ee()->db->insert('charge_settings', $data);
        }

        // ----------------------------------
        //  Redirect to Settings page with Message
        // ----------------------------------
        $this->flux->flashData('message_success', 'Preferences Updated!');
        ee()->functions->redirect($this->flux->moduleURL('settings'));
        exit;
    }


    public function view_action()
    {
        // --------------------------------------
        // Load some libraries
        // --------------------------------------
        //  ee()->load->library('javascript');
        //  ee()->load->library('table');
        //  ee()->cp->add_js_script('ui', 'sortable');
        ee()->cp->set_breadcrumb($this->base, lang('charge_module_name'));
        $this->cached_vars['templates'] = ee()->charge_model->get_templates();
        $this->cached_vars['statuses'] = ee()->charge_model->get_statuses();
        $this->cached_vars['channels'] = ee()->charge_model->get_channels();


        ee()->view->cp_page_title = lang('charge_actions');
        $this->cached_vars['form_post_url'] = $this->flux->moduleURL('view_action');
        $this->cached_vars['add_subscription_uri'] = $this->flux->moduleURL('view_subscription', array('new'=>'yes'));

        $this->cached_vars['errors'] = array();
        $this->cached_vars['subscriptions'] = ee()->charge_subscription->get_all();

        $new_action = false;

        $id = ee()->input->get_post('id');
        if ($id != '') {
            $action = ee()->charge_action->get_one(ee()->input->get_post('id'));
        } else {
            $new_action = true;
            $action = array('name' => '', 'shortname' => '', 'settings' => array(), 'action_id' => '');
        }


        if (ee()->input->post('edit_action') == 'yes') {
            // This is a post
            $status = ee()->charge_action->save($id);
            if ($status == false) {
                // Validation failed
                $this->cached_vars['errors'] = ee()->charge_action->errors;
                $this->cached_vars['has_errors'] = true;

                foreach (ee()->charge_action->this_settings as $key => $val) {
                    if (in_array($key, array('name', 'shortname'))) {
                        $action[$key] = $val;
                    } else {
                        $action['settings'][$key] = $val;
                    }
                }
            } else {
                // Good
                ee()->functions->redirect($this->flux->moduleURL('actions'));
                exit();
            }
        }

        if (empty($action)) {
            ee()->functions->redirect($this->flux->moduleURL('actions'));
            exit();
        }

        $this->cached_vars['action'] = $action;

        return $this->flux->view('add_action', $this->cached_vars, true);
    }


    public function view_subscription()
    {
        $this->_add_morphine(true);
        // --------------------------------------
        // Load some libraries
        // --------------------------------------
        ee()->load->library('javascript');
        ee()->load->library('table');
        ee()->cp->add_js_script('ui', 'sortable');
        ee()->cp->set_breadcrumb($this->base, lang('charge_module_name'));
        ee()->view->cp_page_title = lang('charge_subscriptions');

        $this->cached_vars['errors'] = array();
        $this->cached_vars['form_post_url'] = $this->flux->moduleURL('view_subscription');
        $this->cached_vars['templates'] = ee()->charge_model->get_templates();
        $this->cached_vars['member_groups'] = ee()->charge_model->get_member_groups();
        $this->cached_vars['actions_uri'] = $this->flux->moduleURL('actions');

        if (ee()->input->get_post('id')) {
            $id = ee()->input->get_post('id');
        } else {
            $id = $this->last_segment;
        }

        if ($id != '' && is_numeric($id)) {
            $subscription = ee()->charge_subscription->get_one($id);
        } else {
            $new_subscription = true;
            $subscription = array('name' => '', 'settings' => array(), 'subscription_id' => '');
        }

        if (ee()->input->post('edit_subscription') == 'yes') {
            // This is a post
            $status = ee()->charge_subscription->save($id);

            if ($status == false) {
                // Validation failed
                $this->cached_vars['errors'] = ee()->charge_subscription->errors;
                $this->cached_vars['has_errors'] = true;

                foreach (ee()->charge_subscription->this_settings as $key => $val) {
                    if (in_array($key, array('name', 'member_group_valid', 'member_group_invalid'))) {
                        $subscription[$key] = $val;
                    } else {
                        $subscription['settings'][$key] = $val;
                    }
                }
            } else {
                // Good
                ee()->functions->redirect($this->flux->moduleURL('subscriptions'));
                exit();
            }
        }


        if (empty($subscription)) {
            ee()->functions->redirect($this->flux->moduleURL('subscriptions'));
            exit();
        }

        $this->cached_vars['subscription'] = $subscription;

        return $this->flux->view('add_subscription', $this->cached_vars, true);
    }


    // 2017-08-11 NON-Functional - Stripe is adding the ability to pull webhooks but it's not available at the time this was written.
    public function ajax_stripe_verify_webhook()
    {
        // Let's assume there are no errors and everything was a success!
        $return_data['errors'] = array();
        $return_data['status'] = 'success';
        $return_data['timestamp'] = date('Y-m-d g:ia');

        // Get the total count of Charges, limit it to 1 actual record so we're not pulling unnecessary data.
        $call = ee()->charge_stripe->api_call('\Stripe\Charge::all', '', '', array('limit'=>1, 'include'=>array('total_count')), true);

        if (!$call) {
            $return_data['errors'][] = ee()->charge_stripe->api_error_message;
        } else {
            $return_data['total_charges'] = $call['total_count'];
        }

        // If there was an error (or more), flip our status.
        if (count($return_data['errors']) > 0) {
            $return_data['status'] = 'error';
            $return_data['errors'] = '<br />'.implode('<br />', $return_data['errors']);
        } else {
            $return_data['errors'] = 0;
        }

        die(json_encode($return_data));
    }

    public function ajax_stripe_test_connection()
    {
        // Let's assume there are no errors and everything was a success!
        $return_data['errors'] = array();
        $return_data['status'] = 'success';
        $return_data['timestamp'] = date('Y-m-d g:ia');

        // Get the total count of Charges, limit it to 1 actual record so we're not pulling unnecessary data.
        $call = ee()->charge_stripe->api_call('\Stripe\Charge::all', '', '', array('limit'=>1, 'include'=>array('total_count')), true);

        if (!$call) {
            $return_data['errors'][] = ee()->charge_stripe->api_error_message;
        } else {
            $return_data['total_charges'] = $call['total_count'];
        }

        // Get the total count of Coupons, limit it to 1 actual record so we're not pulling unnecessary data.
        $call = ee()->charge_stripe->api_call('\Stripe\Coupon::all', '', '', array('limit'=>1, 'include'=>array('total_count')), true);

        if (!$call) {
            $return_data['errors'][] = ee()->charge_stripe->api_error_message;
        } else {
            $return_data['total_coupons'] = $call['total_count'];
        }

        // Get the total count of Customers, limit it to 1 actual record so we're not pulling unnecessary data.
        $call = ee()->charge_stripe->api_call('\Stripe\Customer::all', '', '', array('limit'=>1, 'include'=>array('total_count')), true);

        if (!$call) {
            $return_data['errors'][] = ee()->charge_stripe->api_error_message;
        } else {
            $return_data['total_customers'] = $call['total_count'];
        }

        // Get the total count of Plans, limit it to 1 actual record so we're not pulling unnecessary data.
        $call = ee()->charge_stripe->api_call('\Stripe\Plan::all', '', '', array('limit'=>1, 'include'=>array('total_count')), true);

        if (!$call) {
            $return_data['errors'][] = ee()->charge_stripe->api_error_message;
        } else {
            $return_data['total_plans'] = $call['total_count'];
        }

        // Change the API Base to go to the Stripe TLS test script.
        \Stripe\Stripe::$apiBase = "https://api.stripe.com";

        try {
            \Stripe\Charge::all();
            $return_data['tls_status'] = 1;
        } catch (\Stripe\Error\ApiConnection $e) {
            $return_data['tls_status'] = 0;
            $return_data['errors'][] = "TLS 1.2 is not supported. You will need to upgrade your integration.";
        }

        // If there was an error (or more), flip our status.
        if (count($return_data['errors']) > 0) {
            $return_data['status'] = 'error';
            $return_data['errors'] = '<br />'.implode('<br />', $return_data['errors']);
        } else {
            $return_data['errors'] = 0;
        }

        die(json_encode($return_data));
    }

    public function ajax_delete_test_transactions()
    {
        // Let's assume there are no errors and everything was a success!
        $return_data['errors'] = array();
        $return_data['status'] = 'success';
        $return_data['timestamp'] = date('Y-m-d g:ia');

        // Loop through all the test transactions and delete the test member's subscription entries.
        $test_entries = ee()->db->get_where('charge_stripe', array('mode'=>'test'));

        foreach ($test_entries->result() as $test_entry) {
            // Get the Charge ID so we can delete any matching member subscriptions.
            $charge_id = $test_entry->id;
            ee()->db->query("DELETE FROM exp_charge_subscription_member WHERE charge_id='$charge_id'");
        }

        // Delete all the test transactions and log entries.
        ee()->db->query("DELETE FROM exp_charge_log WHERE mode='test'");
        ee()->db->query("DELETE FROM exp_charge_stripe WHERE mode='test'");
        ee()->db->query("DELETE FROM exp_charge_customers WHERE mode='test'");

        die(json_encode(array('status'=>'1')));
    }

    public function reset_webhook_key()
    {
        ee()->charge_webhook->reset_webhook_key();

        return true;
    }

    // --------------------------------------------------------------------

    /**
     * Implodes an Array and Hashes It
     *
     * @access  public
     * @return  string
     */

    public function _imploder($arguments)
    {
        return md5(serialize($arguments));
    }

    // END


    private function _add_morphine($add_rowland = false)
    {
        $theme_folder_url = $this->_get_package_theme_url();

        ee()->cp->add_to_head('<link rel="stylesheet" type="text/css" href="' . $theme_folder_url . 'font-awesome/css/font-awesome.min.css" />');
        ee()->cp->add_to_head('<link rel="stylesheet" type="text/css" href="' . $theme_folder_url . 'styles/screen.css" />');
        ee()->cp->add_to_head('<link rel="stylesheet" tyle="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" />');

        ee()->cp->add_to_foot('<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" type="text/javascript"></script>');
        ee()->cp->add_to_foot('<script type="text/javascript" charset="utf-8" src="' . $theme_folder_url . 'scripts/compressed.js"></script>');
        ee()->cp->add_to_foot('<script type="text/javascript" src="' . $theme_folder_url . 'scripts/cp.js"></script>');

        if ($add_rowland) {
            // add our shizzle
            ee()->cp->add_to_foot('<script type="text/javascript" src="' . $theme_folder_url . 'scripts/jquery.roland.js"></script>');
        }
    }


    private function _get_overview()
    {
        $this->cached_vars['have_overview'] = false;
        $this->cached_vars['have_payment_overview'] = false;
        $this->cached_vars['have_recurring_overview'] = false;

        if (count($this->cached_vars['charges'])) {
            return;
        }

        return;
    }

    private function _pagination($total, $method, $extra = array())
    {
        $offset = 0;
        $page = 1;
        $this->cached_vars['prev_link'] = '';
        $this->cached_vars['next_link'] = '';

        $this->cached_vars['current_page'] = $page;
        $this->cached_vars['total_pages'] = $page;
        $this->cached_vars['has_pagination'] = false;

        foreach ($extra as $key => $val) {
            $this->base .= '&'.$key.'='.$val;
        }


        if ($total > $this->limit) {
            $max_pages = ceil($total / $this->limit);


            if (ee()->input->get('page') != '') {
                $page = ee()->input->get('page');

                $offset = ($page - 1) * $this->limit;
            }
            $next_page = $page + 1;
            $prev_page = $page - 1;

            if ($prev_page > 0) {
                $this->cached_vars['prev_link'] = $this->flux->moduleURL($method, array('page'=>$prev_page));
            }
            if ($next_page <= $max_pages) {
                $this->cached_vars['next_link'] = $this->flux->moduleURL($method, array('page'=>$next_page));
            }

            $this->cached_vars['current_page'] = $page;
            $this->cached_vars['total_pages'] = $max_pages;

            $this->cached_vars['has_pagination'] = true;
        }

        return $offset;
    }

    private function _get_package_theme_url()
    {
        $theme_url = '';

        if (defined('URL_THIRD_THEMES')) {
            $theme_url = URL_THIRD_THEMES;
        } else {
            $theme_url = ee()->config->item('theme_folder_url');

            $theme_url .= substr($theme_url, -1) == '/'
                ? 'third_party/'
                : '/third_party/';
        }

        return $theme_url . 'charge/';
    }

    private function _get_callback_urls()
    {
        $webhook = $this->_get_action_url('act_webhook');

        $webhook_key = $this->settings->charge_webhook_key;

        $this->cached_vars['callback_url'] = $webhook . '&key=' . $webhook_key;
    }


    private function _get_action_url($method_name)
    {
        // Cache the action urls for repeated use
        if (!isset(ee()->session->cache['charge']['action_urls'][$method_name])) {
            $action_id = ee()->db->where(
                array(
                    'class'  => 'Charge',
                    'method' => $method_name
                )
            )->get('actions')->row('action_id');

            ee()->session->cache['charge']['action_urls'][$method_name] = ee()->functions->fetch_site_index(0, 0) . '?ACT=' . $action_id;
        }

        return ee()->session->cache['charge']['action_urls'][$method_name];
    }

    private function _get_members($member_ids = array())
    {
        if (!isset(ee()->session->cache['charge']['members'])) {
            ee()->session->cache['charge']['members'] = $member_ids;

            $member_ids = array_keys(ee()->session->cache['charge']['members']);

            // Now pull the member details from the db
            $members = ee()->db->select('member_id, group_id, username, screen_name, email')
                ->where_in('member_id', $member_ids)
                ->get('members')
                ->result_array();

            foreach ($members as $member) {
                if (isset(ee()->session->cache['charge']['members'][$member['member_id']])) {
                    ee()->session->cache['charge']['members'][$member['member_id']] = $member;
                }
            }
        }

        return ee()->session->cache['charge']['members'];
    }


    private function _get_export_data()
    {
        $output = array();

        // Get everything
        $charges = ee()->charge_stripe->get_all(99999);

        // Clean up

        $allowed_stripe = array('stripe_id', 'stripe_description', 'stripe_plan_name');
        $ignore = array('card_number_dotted', 'plan_wordy', 'plan_currency_symbol', 'plan_currency_formatted', 'amount_currency_formatted', 'amount_formatted', 'plan_amount', 'time_wordy', 'ended_on_wordy', 'current_period_end_wordy');

        foreach ($charges as $key => $charge) {
            foreach ($charge as $ckey => $cval) {
                if (is_array($cval)) {
                    unset($charges[$key][$ckey]);
                }
                if (in_array($ckey, $ignore)) {
                    unset($charges[$key][$ckey]);
                }

                // If the name is 'stripe_', and not in the allowed array, unset
                if (strpos($ckey, 'stripe_') === 0 and !in_array($ckey, $allowed_stripe)) {
                    unset($charges[$key][$ckey]);
                }


                // format timestamps
                if (in_array($ckey, array('timestamp', 'ended_on'))) {
                    if ($cval == '0') {
                        $charges[$key][$ckey] = '';
                    } else {
                        $charges[$key][$ckey] = date('c', $cval);
                    }
                }
            }
        }

        return $charges;
    }


    private function _array_to_csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }

        ob_start();
        $df = fopen("php://output", 'w');

        fputcsv($df, array_keys(reset($array)));

        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);

        return ob_get_clean();
    }

    private function _check_csrf_exempt()
    {
        if (version_compare(APP_VER, '2.7') >= 1) {
            $sql = "UPDATE exp_actions SET csrf_exempt = 1 WHERE class = 'Charge' AND method = 'act_webhook'";
            ee()->db->query($sql);
        }
    }


    private function _download_send_headers($filename)
    {
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    private function getSettingsForm()
    {
        // The followign array is how we handel the settings form... this allows us to support the EE form view for a more standard experience
        // trigger URL

        $form = array(
            'charge_label_stripe_config' => array(
                array(
                    'title' => lang('charge_label_stripe_account_mode'),
                    'desc' => '',
                    'fields' => array(
                        'charge_stripe_account_mode' => array(
                            'type'=>'inline_radio',
                            'value'=>($this->settings->charge_stripe_account_mode ? $this->settings->charge_stripe_account_mode : 'test'),
                            'choices'=>array(
                                'live'=>lang('charge_label_stripe_account_mode_live'),
                                'test'=>lang('charge_label_stripe_account_mode_test')
                            )
                        ),
                    )
                ),
                array(
                    'title' => lang('charge_label_stripe_test_credentials_sk'),
                    'desc' => '',
                    'fields' => array(
                        'charge_stripe_test_credentials_sk' => array(
                            'type'=>'text',
                            'value'=>$this->settings->charge_stripe_test_credentials_sk
                        ),
                    )
                ),
                array(
                    'title' => lang('charge_label_stripe_test_credentials_pk'),
                    'desc' => '',
                    'fields' => array(
                        'charge_stripe_test_credentials_pk' => array(
                            'type'=>'text',
                            'value'=>$this->settings->charge_stripe_test_credentials_pk
                        )
                    )
                ),
                array(
                    'title' => lang('charge_label_stripe_live_credentials_sk'),
                    'desc' => '',
                    'fields' => array(
                        'charge_stripe_live_credentials_sk' => array(
                            'type'=>'text',
                            'value'=>$this->settings->charge_stripe_live_credentials_sk
                        )
                    )
                ),
                array(
                    'title' => lang('charge_label_stripe_live_credentials_pk'),
                    'desc' => '',
                    'fields' => array(
                        'charge_stripe_live_credentials_pk' => array(
                            'type'=>'text',
                            'value'=>$this->settings->charge_stripe_live_credentials_pk
                        )
                    )
                ),
                array(
                    'title' => lang('charge_label_stripe_currency'),
                    'desc' => lang('charge_label_stripe_currency_note'),
                    'fields' => array(
                        'charge_stripe_currency' => array(
                            'type'=>'select',
                            'value'=>$this->settings->charge_stripe_currency,
                            'choices'=>array(
                                'usd'=>lang('charge_label_stripe_currency_usd'),
                                'gbp'=>lang('charge_label_stripe_currency_gbp'),
                                'eur'=>lang('charge_label_stripe_currency_eur'),
                                'cad'=>lang('charge_label_stripe_currency_cad'),
                                'aud'=>lang('charge_label_stripe_currency_aud'),
                                'hkd'=>lang('charge_label_stripe_currency_hkd'),
                                'sek'=>lang('charge_label_stripe_currency_sek'),
                                'dkk'=>lang('charge_label_stripe_currency_dkk'),
                                'pen'=>lang('charge_label_stripe_currency_pen'),
                                'jpy'=>lang('charge_label_stripe_currency_jpy')
                            )
                        )
                    )
                ),
                array(
                    'title' => lang('charge_label_webhook_url'),
                    'desc' => lang('charge_label_webhook_title_note'),
                    'fields' => array(
                        'charge_webhook_key' => array(
                            'type'=>'text',
                            'value'=>$this->cached_vars['callback_url']
                        )
                    )
                ),
                /*
                array(
                    'title' => lang('charge_label_verify_webhook'),
                    'desc' => lang('charge_label_verify_webhook_note'),
                    'fields' => array(
                        'charge_stripe_connectivity' => array(
                            'type' => (!empty($this->settings->charge_stripe_test_credentials_sk) ? 'button' : 'message'),
                            'text' => (!empty($this->settings->charge_stripe_test_credentials_sk) ? '' : '<div class="bg-warning">You must enter and save your Stripe keys.</div>'),
                            'button_type' => 'button',
                            'class' => 'btn action charge_stripe_verify_webhook',
                            'value' => lang('charge_label_verify_webhook'),
                            'datatags' => array(
                                array(
                                    'tag' => 'testurl',
                                    'value' => $this->flux->moduleURL('ajax_stripe_verify_webhook')
                                )
                            )
                        )
                    )
                )
                */
            ),
            'charge_label_extra_options' => array(
                array(
                    'title' => lang('charge_label_force_ssl'),
                    'desc' => lang('charge_label_force_ssl_note'),
                    'fields' => array(
                        'charge_force_ssl' => array(
                            'type'=>'inline_radio',
                            'value'=>($this->settings->charge_force_ssl ? $this->settings->charge_force_ssl : 'no'),
                            'choices'=>array(
                                'yes'=>lang('charge_label_force_ssl_yes'),
                                'no'=>lang('charge_label_force_ssl_no')
                            )
                        )
                    )
                ),
                array(
                    'title' => lang('charge_label_metadata_pass'),
                    'desc' => lang('charge_label_metadata_pass_note'),
                    'fields' => array(
                        'charge_metadata_pass' => array(
                            'type'=>'inline_radio',
                            'value'=>($this->settings->charge_metadata_pass ? $this->settings->charge_metadata_pass : 'no'),
                            'choices'=>array(
                                'yes'=>lang('charge_label_metadata_pass_yes'),
                                'no'=>lang('charge_label_metadata_pass_no')
                            )
                        )
                    )
                )
            ),
            'charge_label_email_options' => array(
                array(
                    'title' => lang('charge_label_email_send_from'),
                    'desc' => lang('charge_label_email_send_from_note'),
                    'fields' => array(
                        'charge_email_send_from' => array(
                            'type'=>'text',
                            'value'=>$this->settings->charge_email_send_from
                        )
                    )
                ),
                array(
                    'title' => lang('charge_label_email_send_name'),
                    'desc' => lang('charge_label_email_send_name_note'),
                    'fields' => array(
                        'charge_email_send_name' => array(
                            'type'=>'text',
                            'value'=>$this->settings->charge_email_send_name
                        )
                    )
                ),
                array(
                    'title' => lang('charge_label_email_send_reply_to'),
                    'desc' => lang('charge_label_email_send_reply_to_note'),
                    'fields' => array(
                        'charge_email_send_reply_to' => array(
                            'type'=>'text',
                            'value'=>$this->settings->charge_email_send_reply_to
                        )
                    )
                ),
                array(
                    'title' => lang('charge_label_email_send_reply_to_name'),
                    'desc' => lang('charge_label_email_send_reply_to_name_note'),
                    'fields' => array(
                        'charge_email_send_reply_to_name' => array(
                            'type'=>'text',
                            'value'=>$this->settings->charge_email_send_reply_to_name
                        )
                    )
                )
            ),
            'charge_label_logging_options' => array(
                array(
                    'title' => lang('charge_label_log_level'),
                    'desc' => lang('charge_label_log_level_note'),
                    'fields' => array(
                        'charge_log_level' => array(
                            'type'=>'radio',
                            'value'=>$this->settings->charge_log_level,
                            'choices'=>array(
                                0=>lang('charge_label_log_level_0'),
                                5=>lang('charge_label_log_level_5'),
                                9=>lang('charge_label_log_level_9')
                            )
                        )
                    )
                )
            ),
            'charge_label_stripe_verification' => array(
                array(
                    'title' => lang('charge_label_stripe_connectivity'),
                    'desc' => lang('charge_label_stripe_connectivity_note'),
                    'fields' => array(
                        'charge_stripe_connectivity' => array(
                            'type' => (!empty($this->settings->charge_stripe_test_credentials_sk) ? 'button' : 'message'),
                            'text' => (!empty($this->settings->charge_stripe_test_credentials_sk) ? '' : '<div class="bg-warning">You must enter and save your Stripe keys.</div>'),
                            'button_type' => 'button',
                            'class' => 'btn action charge_stripe_test_connection',
                            'value' => lang('charge_label_test_connection'),
                            'datatags' => array(
                                array(
                                    'tag' => 'testurl',
                                    'value' => $this->flux->moduleURL('ajax_stripe_test_connection')
                                )
                            )
                        )
                    )
                )
            ),
            'charge_label_data_options' => array(
                array(
                    'title' => lang('charge_label_delete_test_transactions'),
                    'desc' => lang('charge_label_delete_test_transactions_note'),
                    'fields' => array(
                        'charge_delete_test_transactions' => array(
                            'type' => 'button',
                            'text' => '',
                            'button_type' => 'button',
                            'class' => 'btn action charge_delete_test_transactions',
                            'value' => lang('charge_label_delete_test_transactions'),
                            'datatags' => array(
                                array(
                                    'tag' => 'acturl',
                                    'value' => $this->flux->moduleURL('ajax_delete_test_transactions')
                                )
                            )
                        )
                    )
                )
            )
        );

        return $form;
    }
}
