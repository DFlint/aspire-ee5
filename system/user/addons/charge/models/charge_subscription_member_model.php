<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Charge Subscription Model class
 *
 * @package         charge_ee_addon
 * @author          Tom Jaeger <Tom@EEHarbor.com>
 * @link            https://eeharbor.com/charge
 * @copyright       Copyright (c) 2016, Tom Jaeger/EEHarbor
 */
class Charge_subscription_member_model extends Charge_model
{

    // --------------------------------------------------------------------
    // METHODS
    // --------------------------------------------------------------------

    /**
     * Constructor
     *
     * @access      public
     * @return      void
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();

        // Initialize this model
        $this->initialize(
            'charge_subscription_member',
            'subscription_member_id',
            array(
                'site_id'               => 'int(4) unsigned NOT NULL default 0',
                'timestamp'             => 'int(10) unsigned NOT NULL default 0',
                'member_id'             => 'int(10) unsigned NOT NULL default 0',
                'subscription_id'       => 'int(10) unsigned NOT NULL default 0',
                'status'                => 'varchar(100) NOT NULL default ""',
                'at_period_end'         => 'tinyint(1) unsigned NOT NULL default 0',
                'last_contact'          => 'int(10) unsigned NOT NULL default 0',
                'customer_id'           => 'varchar(100) NOT NULL default ""',
                'charge_id'             => 'int(10) unsigned NOT NULL default 0')
        );
    }

    // --------------------------------------------------------------------

    /**
     * Installs given table
     *
     * @access      public
     * @return      void
     */
    public function install()
    {
        // Call parent install
        parent::install();

        // Add indexes to table
        ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`site_id`)");
    }



    public function get_all($limit = 50, $offset = 0)
    {
        ee()->db->limit($limit, $offset);
        ee()->db->order_by('subscription_member_id', 'desc');
        $members = parent::get_all();

        $ids = array();
        foreach ($members as $member) {
            $ids[] = $member['member_id'];
        }
        $extra = $this->_get_members($ids);

        // Associate it
        foreach ($members as $key => $member) {
            $members[$key]['email'] = '';
            $members[$key]['group_id'] = '';
            $members[$key]['screen_name'] = '';
            $members[$key]['username'] = '';

            if (isset($extra[$member['member_id']])) {
                $memberData = $extra[$member['member_id']];
                $members[$key]['email'] = $memberData['email'];
                $members[$key]['group_id'] = $memberData['group_id'];
                $members[$key]['screen_name'] = $memberData['screen_name'];
                $members[$key]['username'] = $memberData['username'];
            }
        }

        return $members;
    }


    public function update_subscription($customer_id, $new_subscription_id)
    {
        ee()->db->where('status', 'active');
        $member = ee()->charge_subscription_member->get_one($customer_id, 'customer_id');
        if (empty($member)) {
            return false;
        }

        $data['subscription_id'] = $new_subscription_id;
        parent::update($member['subscription_member_id'], $data);

        return true;
    }

    // --------------------------------------------------------------------


    public function get_all_by_member_id($member_id = '0')
    {
        if ($member_id == '0') {
            return array();
        }

        ee()->db->where('charge_subscription_member.member_id', $member_id)
                    ->join('charge_stripe', 'charge_stripe.id = charge_subscription_member.charge_id');
        $all = self::get_all();

        // We also want the actual sub details for this
        if (empty($all)) {
            return $all;
        }

        $subs = array();

        foreach ($all as $key => $row) {
            $all[$key] = ee()->charge_stripe->parse($row);

            $subs[] = $row['subscription_id'];
        }

        // Get the subscription details
        ee()->db->where_in('subscription_id', $subs);
        $res = ee()->charge_subscription->get_all();
        foreach ($res as $row) {
            $subscriptions[$row['subscription_id']] = $row;
        }

        foreach ($all as $key => $row) {
            $name = '';
            if (isset($subscriptions[$row['subscription_id']]['name'])) {
                $name = $subscriptions[$row['subscription_id']]['name'];
            }

            $all[$key]['subscription_name'] = $name;
        }


        return $all;
    }

    public function active($member_id = '0')
    {
        if ($member_id == '0') {
            return array();
        }

        ee()->db->where('charge_subscription_member.member_id', $member_id)
                    ->where('charge_subscription_member.status', 'active')
                    ->join('charge_stripe', 'charge_stripe.id = charge_subscription_member.charge_id');
        $all = self::get_all();

        // We also want the actual sub details for this
        if (empty($all)) {
            return $all;
        }

        $subs = array();

        foreach ($all as $key => $row) {
            $all[$key] = ee()->charge_stripe->parse($row);

            $subs[] = $row['subscription_id'];
        }

        // Get the subscription details
        ee()->db->where_in('subscription_id', $subs);
        $res = ee()->charge_subscription->get_all();
        foreach ($res as $row) {
            $subscriptions[$row['subscription_id']] = $row;
        }

        foreach ($all as $key => $row) {
            $name = '';
            if (isset($subscriptions[$row['subscription_id']]['name'])) {
                $name = $subscriptions[$row['subscription_id']]['name'];
            }

            $all[$key]['subscription_name'] = $name;
        }


        return $all;
    }


    public function inactive($member_id = '0')
    {
        if ($member_id == '0') {
            return array();
        }

        ee()->db->where('charge_subscription_member.member_id', $member_id)
                    ->where('charge_subscription_member.status', 'inactive')
                    ->join('charge_stripe', 'charge_stripe.id = charge_subscription_member.charge_id');
        $all = self::get_all();

        // We also want the actual sub details for this
        if (empty($all)) {
            return $all;
        }

        $subs = array();

        foreach ($all as $key => $row) {
            $all[$key] = ee()->charge_stripe->parse($row);

            $subs[] = $row['subscription_id'];
        }

        // Get the subscription details
        ee()->db->where_in('subscription_id', $subs);
        $res = ee()->charge_subscription->get_all();
        foreach ($res as $row) {
            $subscriptions[$row['subscription_id']] = $row;
        }

        foreach ($all as $key => $row) {
            $name = '';
            if (isset($subscriptions[$row['subscription_id']]['name'])) {
                $name = $subscriptions[$row['subscription_id']]['name'];
            }

            $all[$key]['subscription_name'] = $name;
        }


        return $all;
    }


    private function _get_members($member_ids = array())
    {
        if (empty($member_ids)) {
            return array();
        }
        // Now pull the member details from the db
        $members = ee()->db->select('member_id, group_id, username, screen_name, email')
            ->where_in('member_id', $member_ids)
            ->get('members')
            ->result_array();

        $return = array();
        foreach ($members as $member) {
            $return[$member['member_id']] = $member;
        }

        return $return;
    }
} // End class

/* End of file Charge_subscription_member_model.php */
