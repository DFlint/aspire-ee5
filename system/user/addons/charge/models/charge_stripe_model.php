<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Charge Stripe Model class
 *
 * @package         charge_ee_addon
 * @author          Tom Jaeger <Tom@EEHarbor.com>
 * @link            https://eeharbor.com/charge
 * @copyright       Copyright (c) 2016, Tom Jaeger/EEHarbor
 */
class Charge_stripe_model extends Charge_model
{
    public $data;
    private $create_plan_if_not_exists = true;
    public $recurring_plan;
    public $errors;
    public $messages;

    public $api_error_message;


    public $default_currency = 'usd';
    public $supported_currencies = array('usd' => array('name' => 'American Dollar', 'symbol' => '&#36;', 'symbol_long' => 'US&#36;'),
                                         'gbp' => array('name' => 'British Pound Sterling', 'symbol' => '&#163;', 'symbol_long' => '&#163;'),
                                         'eur' => array('name' => 'Euro', 'symbol' => '&#128;', 'symbol_long' => '&#128;'),
                                         'cad' => array('name' => 'Canadian Dollars', 'symbol' => '&#36;', 'symbol_long' => 'CA&#36;'),
                                         'aud' => array('name' => 'Australian Dollar', 'symbol' => '&#36;', 'symbol_long' => 'AU&#36;'),
                                         'hkd' => array('name' => 'Hong Kong Dollar', 'symbol' => '&#36;', 'symbol_long' => 'HK&#36;'),
                                         'sek' => array('name' => 'Swedish Krona', 'symbol' => ':-', 'symbol_long' => 'kr'),
                                         'dkk' => array('name' => 'Danish Krone', 'symbol' => ',-', 'symbol_long' => 'dkr'),
                                         'pen' => array('name' => 'Peruvian Nuevo Sol', 'symbol' => 'S/.', 'symbol_long' => 'S/.'),
                                         'jpy' => array('name' => 'Japanese Yen', 'symbol' => '&#165;', 'symbol_long' => '&#165;'));
    public $supported_actions = array('end'        => '_act_cancel_recurring',
                                      'reactivate' => '_act_reactivate_recurring');
    private $action_charge = array();

    private $_full_metadata = array();
    private $save_card_default = true;
    private $settings;

    // --------------------------------------------------------------------
    // METHODS
    // --------------------------------------------------------------------

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        require_once(PATH_THIRD . 'charge/libraries/stripe/init.php');

        // Call parent constructor
        parent::__construct();

        $this->settings = $this->flux->getSettings();

        // Initialize this model
        $this->initialize(
            'charge_stripe',
            'id',
            array(
                'site_id'               => 'int(4) unsigned NOT NULL default 1',
                'timestamp'             => 'int(10) unsigned NOT NULL default 0',
                'ended_on'              => 'int(10) unsigned NOT NULL default 0',
                'current_period_end'    => 'int(10) unsigned NOT NULL default 0',
                'hash'                  => 'varchar(100) NOT NULL default ""',
                'member_id'             => 'int(10) unsigned NOT NULL default 0',
                'source_url'            => 'varchar(255) NOT NULL default ""',
                'type'                  => 'varchar(100) NOT NULL default ""',
                'customer_id'           => 'varchar(100) NOT NULL default ""',
                'payment_id'            => 'varchar(100) NOT NULL default ""',


                'plan_amount'           => 'int(10) unsigned NOT NULL default 0',
                'plan_interval'         => 'varchar(255) NOT NULL default ""',
                'plan_currency'         => 'varchar(255) NOT NULL default ""',
                'plan_interval_count'   => 'int(10) unsigned NOT NULL default 0',
                'plan_trial_days'       => 'int(10) unsigned NOT NULL default 0',
                'plan_trial_end'        => 'int(10) unsigned NOT NULL default 0',
                'plan_length'           => 'int(10) unsigned NOT NULL default 0',
                'plan_length_interval'  => 'varchar(255) NOT NULL default ""',
                'plan_length_expiry'    => 'int(10) unsigned NOT NULL default 0',

                // Coupons!
                'plan_coupon'           => 'varchar(100) NOT NULL default ""',
                'plan_coupon_stripe_id' => 'varchar(100) NOT NULL default ""',
                'plan_discount'         => 'int(10) unsigned NOT NULL default 0',
                'plan_full_amount'      => 'int(10) unsigned NOT NULL default 0',

                // Multi-Plans!
                'multi_payment_id'      => 'int(10) unsigned NOT NULL default 0',

                'card_name'             => 'varchar(255) NOT NULL default ""',
                'card_address_line1'    => 'varchar(255) NOT NULL default ""',
                'card_address_line2'    => 'varchar(255) NOT NULL default ""',
                'card_address_city'     => 'varchar(255) NOT NULL default ""',
                'card_address_state'    => 'varchar(255) NOT NULL default ""',
                'card_address_zip'      => 'varchar(255) NOT NULL default ""',
                'card_address_country'  => 'varchar(255) NOT NULL default ""',
                'card_last4'            => 'varchar(255) NOT NULL default ""',
                'card_type'             => 'varchar(255) NOT NULL default ""',
                'card_exp_month'        => 'varchar(255) NOT NULL default ""',
                'card_exp_year'         => 'varchar(255) NOT NULL default ""',
                'card_fingerprint'      => 'varchar(255) NOT NULL default ""',
                'card_saved'            => 'int(1) unsigned NOT NULL default 0',

                'customer_name'         => 'varchar(255) NOT NULL default ""',
                'customer_email'        => 'varchar(255) NOT NULL default ""',

                'connected_entry_id'    => 'varchar(100) NOT NULL default ""',
                'meta'                  => 'text',
                'stripe'                => 'mediumtext',
                'messages'              => 'text',
                'mode'                  => 'varchar(255) NOT NULL default ""',
                'state'                 => 'varchar(255) NOT NULL default ""',
                'chargecart_unique_id'  => 'varchar(100) NOT NULL default ""'
            )
        );

        $this->_setup_stripe();
    }

    // --------------------------------------------------------------------

    /**
     * Installs given table
     *
     * @access      public
     * @return      void
     */
    public function install()
    {
        // Call parent install
        parent::install();

        // Add indexes to table
        ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`site_id`)");
        ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`member_id`)");
        ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`hash`)");
    }


    public function get_currencies()
    {
        $ret = array();

        foreach ($this->supported_currencies as $currency_key => $currency_data) {
            $ret[$currency_key]['name'] = $currency_data['name'];
            $ret[$currency_key]['cleaned_name'] = strtoupper($currency_key) . ' - ' . $currency_data['name'] . ' (' . $currency_data['symbol_long'] . ')';
            $ret[$currency_key]['symbol'] = $currency_data['symbol'];
        }

        return $ret;
    }

    public function get_currency_symbol($currency_code = '', $type = 'symbol')
    {
        $symbol = '';

        if (isset($this->supported_currencies[$currency_code])) {
            $symbol = $this->supported_currencies[$currency_code][$type];
        }

        return $symbol;
    }


    public function api_call($call_name = '', $call_val = '', $call_key = '', $params = '', $pure = false)
    {
        if ($call_name == '') {
            return false;
        }

        $method = explode('::', $call_name);
        if (count($method) != 2) {
            return false;
        }

        if (empty($params)) {
            $params = array($call_key => $call_val);
        }

        // Some API calls need their parameters to be arrays.
        $params = array($params);

        try {
            $data = forward_static_call_array($method, $params);

            if (!$pure && isset($data['data'])) {
                $data = $data['data'];
            }

            $data = $this->_to_array($data);
        } catch (Exception $e) {
            $this->api_error_message = $e->getMessage();

            return false;
        }

        return $data;
    }


    public function get_recurring($member_id = 0)
    {
        if ($member_id == 0) {
            return array();
        }

        ee()->db->where('member_id', $member_id)
            ->where('type', 'recurring')
            ->where_in('state', array('active','trialing'));
        $charges = parent::get_all();

        foreach ($charges as $key => $row) {
            $charges[$key] = $this->parse($row, true);
        }

        return $charges;
    }


    public function get_recurring_inactive($member_id = 0)
    {
        if ($member_id == 0) {
            return array();
        }

        ee()->db->where('member_id', $member_id)
            ->where('type', 'recurring')
            ->where_not_in('state', array('active','trialing'));
        $charges = parent::get_all();

        foreach ($charges as $key => $row) {
            $charges[$key] = $this->parse($row, true);
        }

        return $charges;
    }


    public function get_all($limit = 50, $offset = 0, $unset = true)
    {
        ee()->db->limit($limit, $offset);
        ee()->db->order_by('id', 'desc');
        $charges = parent::get_all();

        foreach ($charges as $key => $row) {
            $charges[$key] = $this->parse($row, $unset);
        }

        return $charges;
    }

    public function get_all_by_chargecart_unique_id($chargecart_unique_id, $limit = 50, $offset = 0, $unset = true)
    {
        ee()->db->where('chargecart_unique_id', $chargecart_unique_id);
        ee()->db->limit($limit, $offset);
        ee()->db->order_by('id', 'desc');
        $charges = parent::get_all();

        foreach ($charges as $key => $row) {
            $charges[$key] = $this->parse($row, $unset);
        }

        return $charges;
    }

    public function get_first_by_chargecart_unique_id($chargecart_unique_id)
    {
        ee()->db->order_by('id', 'asc');
        $data = self::get_one($chargecart_unique_id, 'chargecart_unique_id');
        if (empty($data)) {
            return false;
        }

        return $this->parse($data);
    }

    public function charge($data)
    {
        $this->data = $data;

        ee()->charge_log->log_attempt_start_one_off($this->data);

        try {
            $description = $this->_create_description();

            $metadata = $this->_collect_metadata();

            $customer = $this->_create_customer(null, 'customer');

            if ($customer == false) {
                return false;
            }

            // Adjust for onetime coupon
            if (isset($this->data['coupon']['plan'])) {
                $this->data['plan']['amount'] = $this->data['coupon']['plan']['amount'];
                $this->data['plan']['full_amount'] = $this->data['coupon']['plan']['full_amount'];
                $this->data['plan']['discount'] = $this->data['coupon']['plan']['discount'];
            }

            if (ee()->input->post('card_use_saved')) {
                if (!empty($customer->invoice_settings->default_payment_method)) {
                    $payment_method = $customer->invoice_settings->default_payment_method;
                } else {
                    $charge = \Stripe\Charge::create(array(
                        "customer"    => $customer,
                        "amount"      => $this->data['plan']['amount'],
                        "currency"    => $this->data['plan']['currency'],
                        "description" => $description,
                        "metadata"    => $metadata
                    ));

                    return $this->_record($charge);
                }
            }

            $payment_intent_id = ee()->input->post('payment_intent_id');

            if ($payment_intent_id) {
                $intent = \Stripe\PaymentIntent::retrieve($payment_intent_id);
                $intent->confirm();
            } else {
                if (empty($payment_method)) {
                    $payment_method = $this->data['card']['token'];
                }

                $intent = \Stripe\PaymentIntent::create(
                    [
                        'payment_method' => $payment_method,
                        'customer' => $customer,
                        'amount' => $this->data['plan']['amount'],
                        'currency' => $this->data['plan']['currency'],
                        'confirmation_method' => 'manual',
                        'confirm' => true,
                        'metadata'    => $metadata,
                        'description' => $description,
                    ]
                );
            }

            $paymentResponse = $this->generatePaymentResponse($intent);

            $customData = array();

            // Check if something went wrong.
            if ($paymentResponse['success'] !== true) {
                // Find out if we need the user's manual confirmation of this payment.
                if ($paymentResponse['requires_action'] === true) {
                    $customData['state'] = 'pending';
                    $customData['requires_action'] = true;
                }
            } else {
                $customData['state'] = 'active';
            }

            return $this->_record($intent, $customData);

            // // Now wipe the customer card
            // $wipe = $this->save_card_default;

            // if (isset($this->data['card']['save'])) {
            //     // This can flip either way, so check both
            //     if ($this->data['card']['save'] == 'y' || $this->data['card']['save'] == 'yes') {
            //         $wipe = false;
            //     }

            //     if ($this->data['card']['save'] == 'n' || $this->data['card']['save'] == 'no') {
            //         $wipe = true;
            //     }
            // }

            // $card = $this->_get_customer_card($customer, !$wipe);

            // ee()->charge_log->log_charge_created(Charge_obj_to_array($intent));

            // Also keep a record in our transaction log
        } catch (Exception $e) {
            $message = $e->getMessage();
            ee()->charge_log->log_exception($message);
            $this->errors[] = $message;
        }

        ee()->charge_log->log_error_creating_charge($this->data);

        return false;
    }

    public function generatePaymentResponse($intent)
    {
        if ($intent->status === 'requires_action' || ($intent->status === 'requires_source_action' && $intent->next_action->type === 'use_stripe_sdk')) {
            $response = array(
                'success' => false,
                'requires_action' => true,
                'payment_intent_client_secret' => $intent->client_secret,
            );
        } elseif ($intent->status == 'succeeded') {
            # The payment didn’t need any additional actions and completed!
            # Handle post-payment fulfillment
            $response = array(
                'success' => true,
                'requires_action' => false,
                'error' => false,
            );
        } else {
            # Invalid status
            http_response_code(500);
            $response = array(
                'success' => false,
                'requires_action' => false,
                'error' => true,
                'error_message' => 'Invalid PaymentIntent status',
            );
        }

        return $response;
    }

    public function recurring($data, $verified = false)
    {
        $this->data = $data;

        ee()->charge_log->log_attempt_start_recurring($this->data);

        $this->_setup_recurring();
        if ($this->recurring_plan == false) {
            // We don't have a plan_id, so can't do much
            ee()->charge_log->log_error_no_plan($this->data);
        }

        // Now create our customer and attach
        $return = $this->_create_customer($this->recurring_plan, 'record', $verified);

        if ($return === false) {
            ee()->charge_log->log_error_no_customer($this->data);
        }

        if (!empty($return['requires_action'])) {
            ee()->charge_log->log_event('recurring_requires_verification', $return);
        } elseif ($verified === true) {
            ee()->charge_log->log_event('recurring_verified', $return);
        }

        return $return;
    }


    public function fetch($hash)
    {
        $data = self::get_one($hash, 'hash');
        if (empty($data)) {
            return false;
        }

        return $this->parse($data);
    }


    public function parse($data, $unset = true)
    {
        // Good, parse our the various encoded parts for easier usage later
        foreach (array('meta', 'stripe') as $key) {
            if (!isset($data[$key]) || $data[$key] == '') {
                $data[$key] = array();
            } else {
                $data[$key] = unserialize(base64_decode($data[$key]));
            }
        }

        foreach ($data['meta'] as $meta_key => $meta_val) {
            $data['meta:' . $meta_key] = $meta_val;
        }


        // for saftey
        $data['stripe_card_exp_month'] = '';
        $data['stripe_card_exp_year'] = '';

        foreach ($data['stripe'] as $stripe_key => $stripe_val) {
            if (is_array($stripe_val)) {
                continue;
            }
            if (is_object($stripe_val)) {
                if (get_class($stripe_val) == 'Stripe_Object') {
                    // Special subscription handling
                    $sub_obj = $stripe_val->__toArray();

                    foreach ($sub_obj as $sub_key => $sub_val) {
                        if (is_array($sub_val)) {
                            continue;
                        }

                        if ($sub_key == 'plan') {
                            $plan = $sub_val->__toArray();
                            foreach ($plan as $plan_key => $plan_val) {
                                $data['stripe_' . $sub_key . '_' . $plan_key] = $plan_val;
                            }
                        } else {
                            $data['stripe_' . $stripe_key . '_' . $sub_key] = $sub_val;
                        }
                    }
                }

                if (get_class($stripe_val) == 'Stripe_List' && $stripe_key == 'subscriptions') {
                    $subscriptions_array = $stripe_val->__toArray();

                    // Charge only cares about the first subscription
                    $sub = current($subscriptions_array['data']);

                    if (!empty($sub)) {
                        $sub = $sub->__toArray();

                        foreach ($sub as $sub_key => $sub_val) {
                            if (is_object($sub_val)) {
                                continue;
                            }
                            $data['stripe_subscription_' . $sub_key] = $sub_val;


                            // Special test for trial states
                            if ($sub_key == 'status' and $sub_val == 'trialing') {
                                // Test to see if the trial has expired

                                if ($sub['trial_end'] < time()) {
                                    // Trial has expired
                                    // Trigger an update to this status
                                    $ret = $this->trigger_trial_end($data['id']);

                                    $data['plan_trial_end'] = $ret['plan_trial_end'];
                                    $data['state'] = $ret['state'];
                                }
                            }
                        }
                    }
                }


                if (get_class($stripe_val) == 'Stripe_Card') {
                    // Special card handling
                    $card_array = $stripe_val->__toArray();
                    foreach ($card_array as $card_key => $card_val) {
                        $data['stripe_card_' . $card_key] = $card_val;
                    }
                }
            } else {
                $data['stripe_' . $stripe_key] = $stripe_val;
            }
        }

        if ($unset) {
            unset($data['meta']);
            unset($data['stripe']);
        }


        // Handle system messages
        if (!isset($data['message']) || $data['messages'] == '') {
            $data['messages'] = array();
        } else {
            $data['messages'] = json_decode($data['messages'], true);
        }

        // Format the data and make sure the fields exist.
        if (!isset($data['card_last4'])) {
            $data['card_last4'] = '';
        }
        if (!isset($data['card_type'])) {
            $data['card_type'] = '';
        }
        if (!isset($data['plan_interval'])) {
            $data['plan_interval'] = '';
        }
        if (!isset($data['plan_interval_count'])) {
            $data['plan_interval_count'] = '';
        }
        if (!isset($data['plan_currency'])) {
            $data['plan_currency'] = '';
        }
        if (!isset($data['ended_on'])) {
            $data['ended_on'] = '0';
        }
        if (!isset($data['current_period_end'])) {
            $data['current_period_end'] = '0';
        }
        if (!isset($data['plan_full_amount'])) {
            $data['plan_full_amount'] = 0;
        }
        if (!isset($data['plan_discount'])) {
            $data['plan_discount'] = 0;
        }

        $data['card_number_dotted'] = $this->make_dotted_card($data['card_last4']);
        $data['plan_wordy'] = $this->_construct_plan_description($data['plan_interval'], $data['plan_interval_count']);
        $data['plan_currency_symbol'] = $this->get_currency_symbol($data['plan_currency']);

        // Format the data into additional vars (2nd lines are legacy var names).
        $data['plan_amount_currency_formatted'] = $this->get_currency_symbol($data['plan_currency']) . number_format($data['plan_amount'] / 100, 2);
        $data['amount_currency_formatted'] = $data['plan_amount_currency_formatted'];

        $data['plan_amount_formatted'] = number_format($data['plan_amount'] / 100, 2);
        $data['amount_formatted'] = $data['plan_amount_formatted'];

        $data['amount'] = number_format($data['plan_amount'] / 100, 2);

        $data['plan_full_amount_formatted'] = number_format($data['plan_full_amount'] / 100, 2);
        $data['plan_full_amount_currency_formatted'] = $this->get_currency_symbol($data['plan_currency']) . number_format($data['plan_full_amount'] / 100, 2);

        $data['plan_discount_formatted'] = number_format($data['plan_discount'] / 100, 2);
        $data['discount_formatted'] = $data['plan_discount_formatted'];

        $data['plan_discount_currency_formatted'] = $this->get_currency_symbol($data['plan_currency']) . number_format($data['plan_discount'] / 100, 2);
        $data['discount_currency_formatted'] = $data['plan_discount_currency_formatted'];

        $data['time_wordy'] = date('H:i', $data['timestamp']) . ' on ' . date('l jS F', $data['timestamp']);
        $data['ended_on_wordy'] = date('H:i', $data['ended_on']) . ' on ' . date('l jS F', $data['ended_on']);
        $data['current_period_end_wordy'] = date('H:i', $data['current_period_end']) . ' on ' . date('l jS F', $data['current_period_end']);

        return $data;
    }


    public function user_action($data, $extra = array())
    {
        if (!isset($data['action'])) {
            return false;
        }
        if (!isset($data['charge_id'])) {
            return false;
        }

        $member_id = ee()->session->userdata('member_id');
        if ($member_id == '' or $member_id == '0') {
            return false;
        }

        // validate the action too
        if (!$this->validate_action($data['action'], $data['charge_id'], $member_id, true)) {
            return false;
        }

        // Appears valid
        // Do something
        try {
            $action = $this->supported_actions[$data['action']];

            if (ee()->extensions->active_hook('charge_action_start') === true) {
                ee()->extensions->call('charge_action_start', $data, $data['action'], $member_id, $extra);
                if (ee()->extensions->end_script === true) {
                    return;
                }
            }

            return $this->$action($extra);
        } catch (Exception $e) {
            return false;
        }
    }

    private function _test_tls()
    {
    }

    private function _act_reactivate_recurring($extra)
    {
        // Only if we have a charge
        if (empty($this->action_charge)) {
            return false;
        }

        // Must be in a cancelled state
        if ($this->action_charge['type'] != 'recurring') {
            return false;
        }
        if ($this->action_charge['state'] != 'canceled' and $this->action_charge['state'] != 'canceled_trial') {
            return false;
        }

        // ok. Decode the stripe block to get the stripe id
        $stripe = unserialize(base64_decode($this->action_charge['stripe']));
        if (!is_array($stripe) or empty($stripe) or $stripe['object'] != 'customer') {
            return false;
        }

        $state = 'active';
        if ($this->action_charge['state'] != 'canceled_trial') {
            $state = 'trialing';
        }

        try {
            if (isset($stripe['subscription'])) {
                $plan_id = $stripe['subscription']->plan->id; // Legacy support
                $sub_id = $stripe['subscription']->id;
                $sub = $stripe['subscription'];
            } else {
                $sub = $stripe['subscriptions']->data;
                $sub = current($sub);
                $plan_id = $sub->plan->id;
                $sub_id = $sub->id;
            }


            $sub->plan = $plan_id;
            $sub->status = $state;

            // Update our record too
            $this->reactivate_charge($this->action_charge['id'], $state);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }


    private function _act_cancel_recurring($extra)
    {
        // Only if we have a charge
        if (empty($this->action_charge)) {
            return false;
        }

        // Only valid on 'active' 'recurring' payments
        if ($this->action_charge['state'] != 'active' and $this->action_charge['state'] != 'trialing') {
            return false;
        }
        if ($this->action_charge['type'] != 'recurring') {
            return false;
        }

        // ok. Decode the stripe block to get the stripe id
        $stripe = unserialize(base64_decode($this->action_charge['stripe']));
        if (!is_array($stripe) or empty($stripe) or $stripe['object'] != 'customer') {
            return false;
        }

        $at_period_end = false;
        if (isset($extra['at_period_end']) and $extra['at_period_end'] == 'yes') {
            $at_period_end = true;
        }

        $cancelled_state = 'canceled';
        if ($this->action_charge['state'] == 'trialing') {
            $cancelled_state = 'canceled_trial';
        }

        try {
            $cu = \Stripe\Customer::retrieve($stripe['id']);

            $sub_ids = array();
            if (isset($cu['subscription'])) {
                $sub_ids[] = $cu['subscription']->id;
            } // Legacy support

            if (isset($cu['subscriptions'])) {
                $subs = $cu['subscriptions']->__toArray();
                foreach ($subs['data'] as $sub) {
                    $sub_ids[] = $sub->id;
                }
            }

            $extra = array();
            foreach ($sub_ids as $sub_id) {
                $this_sub = $cu->subscriptions->retrieve($sub_id)->cancel(array('at_period_end' => $at_period_end));
                if ($this_sub->current_period_end) {
                    $extra['current_period_end'] = $this_sub->current_period_end;
                }
            }

            $this->end_charge($this->action_charge['id'], $cancelled_state, $extra);
        } catch (Exception $e) {
            $message = $e->getMessage();

            if ($message == str_replace('{customer_id}', $stripe['id'], lang('charge_stripe_cancel_no_active'))) {
                // Already cancelled, clear it  (legacy for single sub. support)
                $this->end_charge($this->action_charge['id'], $cancelled_state);
            } elseif ($message == str_replace(array('{customer_id}', '{sub_id}'), array($stripe['id'], $sub_id), lang('charge_stripe_cancel_no_match'))) {
                // No matching sub. found
                $this->end_charge($this->action_charge['id'], $cancelled_state);
            } else {
                // Throw a message
                ee()->charge_log->log_exception($message);
                $this->errors[] = $message;
            }

            return false;
        }

        return true;
    }


    public function validate_action($type, $charge_id, $member_id, $remember = false)
    {
        // Get the charge
        $charge = ee()->db->get_where('charge_stripe', array('member_id'=>$member_id, 'id'=>$charge_id), 1)->row_array();

        if (empty($charge)) {
            return false;
        }

        // We only allow some actions
        if (!isset($this->supported_actions[$type])) {
            return false;
        }

        // Valid, wrap the details up in to a hash
        $data['action'] = $type;
        $data['charge_id'] = $charge_id;

        if ($remember) {
            $this->action_charge = $charge;
        }

        return $data;
    }


    private function reactivate_charge($charge_id, $state = 'active')
    {
        $data = array();
        $data['state'] = $state;
        $data['ended_on'] = '0';
        self::update($charge_id, $data);

        return;
    }


    private function end_charge($charge_id, $state = 'canceled', $extra='')
    {
        $data = array();
        $data['state'] = $state;
        $data['ended_on'] = ee()->localize->now;
        $data['current_period_end'] = (isset($extra['current_period_end']) ? $extra['current_period_end'] : 0);
        self::update($charge_id, $data);

        return;
    }


    public function get_stripe_mode()
    {
        if (isset($this->settings->charge_stripe_account_mode)) {
            $stripe_mode = $this->settings->charge_stripe_account_mode;
        } else {
            $stripe_mode = 'test';
        }

        return $stripe_mode;
    }

    private function _setup_stripe($stripe_mode = '')
    {
        if ($stripe_mode != 'test' && $stripe_mode != 'live') {
            $stripe_mode = $this->get_stripe_mode();
        }

        if (isset($this->settings->{'charge_stripe_' . $stripe_mode . '_credentials_sk'})) {
            $sk_key = $this->settings->{'charge_stripe_' . $stripe_mode . '_credentials_sk'};
            \Stripe\Stripe::setApiKey($sk_key);

            $api_version = $this->settings->charge_api_version;
            \Stripe\Stripe::setApiVersion($api_version);
        }
    }


    private function _setup_recurring()
    {
        // Get the recurring periods, then check stripe to see
        // if we have an existing plan, create if not
        $plan['interval'] = $this->data['plan']['interval'];
        $plan['interval_count'] = $this->data['plan']['interval_count'];
        $plan['amount'] = $this->data['plan']['amount'];
        $plan['currency'] = $this->data['plan']['currency'];
        $plan['trial_days'] = $this->data['plan']['trial_days'];

        // Handle set length subscriptions
        if (isset($this->data['plan']['length']) and isset($this->data['plan']['length_interval'])) {
            // Set length
            $plan['length'] = $this->data['plan']['length'];
            $plan['length_interval'] = $this->data['plan']['length_interval'];
        }

        $plan_name = $this->_construct_plan_name($plan);
        $this->recurring_plan = $this->_check_plan_exists($plan_name, $plan);

        if ($this->recurring_plan === false and $this->create_plan_if_not_exists) {
            // Create the plan
            ee()->charge_log->log_plan_create_start(array('plan_name' => $plan_name, 'plan' => $plan));
            $this->recurring_plan = $this->_create_plan($plan_name, $plan);
        }

        if ($this->recurring_plan === false) {
            return false;
        }

        return true;
    }

    public function get_card_for_customer(\Stripe\Customer $stripe_Customer)
    {
        $card = $this->_get_customer_card($stripe_Customer);

        if ($card['fingerprint'] == '') {
            return false;
        }

        return $card;
    }

    private function _get_customer_card(\Stripe\Customer $stripeCustomer, $wipeCard = false)
    {
        $ret['exp_month'] = '';
        $ret['exp_year'] = '';
        $ret['fingerprint'] = '';

        try {
            if (!empty($stripeCustomer->invoice_settings->default_payment_method)) {
                $payment_method = \Stripe\PaymentMethod::retrieve($stripeCustomer->invoice_settings->default_payment_method);

                $method = $this->convertPaymentMethodToCard($payment_method);

                return $method;
            }

            // Pull out the card id from the Stripe_List on the Stripe_Customer
            $cards = $stripeCustomer->sources->__toArray();
            $card = current($cards['data']);
            if ($card !== false) {

                //   if ($wipeCard == true) $stripeCustomer->cards->retrieve($card->id)->delete();

                if ($card === false) {
                    return $ret;
                }

                $card = $card->__toArray();

                return $card;
            }
        } catch (Exception $e) {
            $this->errors[] = $e->getMessage();
        }

        return $ret;
    }


    private function _create_plan($plan_name, $plan)
    {
        $response = array();
        $plan_id = $plan_name;

        $amount = $plan['amount'];
        $interval = (isset($plan['interval']) ? $plan['interval'] : 'month');
        $interval_count = (isset($plan['interval_count']) ? $plan['interval_count'] : '1');
        $currency = $plan['currency'];
        $trial_days = (isset($plan['trial_days']) and is_numeric($plan['trial_days'])) ? $plan['trial_days'] : 0;

        try {
            // Check to see if a plan already exists.
            $plan = $this->retrieve_stripe_plan(array('name' => $plan_name));

            if (empty($plan)) {
                // Create the Product first then attach a Plan. A Plan determines a Product's pricing.
                // This could be created inline in the `Plan::create()` but will only create a 'service'
                // product. We're keeping it separate here so we can add functionality for creating
                // products that are sold as physical goods.
                $product = \Stripe\Product::create(array(
                    "name"              => $plan_name,
                    "type"              => 'service' // Can be 'service' or 'good'.
                ));

                $plan = \Stripe\Plan::create(array(
                    "amount"            => $amount,
                    "interval"          => $interval,
                    "interval_count"    => $interval_count,
                    "nickname"          => $plan_name,
                    "currency"          => $currency,
                    "product"           => $product, // Could pass this as an array
                    "trial_period_days" => $trial_days
                ));
            }

            // Log this
            ee()->charge_log->log_plan_created(Charge_obj_to_array($plan));

            return $plan;
        } catch (Exception $e) {
            $message = $e->getMessage();
            ee()->charge_log->log_exception($message);
            $this->errors[] = $message;
        }

        ee()->charge_log->log_error_plan_creation($plan);

        return false;
    }

    private function _check_plan_exists($plan_name, $plan)
    {
        try {
            $p = \Stripe\Plan::retrieve($plan_name);

            return $p;
        } catch (\Stripe\Error\Base $e) {
            if ($e->getHttpStatus() == '404') {
                return false;
            }
        }

        return false;
    }


    private function _construct_plan_name($plan)
    {
        // Allow for set names
        if (isset($this->data['plan']['set_name']) and trim($this->data['plan']['set_name']) != '') {
            return $this->data['plan']['set_name'];
        }

        // Ex: 75 USD Every [x] Month(s)
        $plan_name[] = number_format($plan['amount'] / 100, 2);
        $plan_name[] = strtoupper($plan['currency']);
        // $plan_name[] = $plan['amount'];


        // This was actually a one-time charge,
        // but with a setlength for an associated subscription
        // We've spoofed this into a recurring payment
        if (isset($this->data['plan']['spoofed_recurring'])) {
            $plan_name[] = 'charge';
        } else {
            if ($plan['interval_count'] > 1) {
                // every [x] [period]s
                $plan_name[] = 'Every ' . $plan['interval_count'] . ' ' . ucwords($plan['interval'] . 's');
            } else {
                $plan_name[] = ucwords($plan['interval'] . 'ly');
            }
        }

        if (isset($plan['trial_days']) and $plan['trial_days'] > 0) {
            $plan_name[] = 'with ' . $plan['trial_days'] . ' day trial';
        }

        if (isset($plan['length']) and isset($plan['length_interval'])) {
            // Set length
            if ($plan['length'] > 1) {
                $plan_name[] = 'for ' . $plan['length'] . ' ' . ucwords($plan['length_interval'] . 's');
            } else {
                $plan_name[] = 'for ' . $plan['length'] . ' ' . ucwords($plan['length_interval']);
            }
        }


        return implode(' ', $plan_name);
    }


    private function _construct_plan_description($period, $period_count, $trial_days = 0)
    {
        if (!in_array($period, array('week', 'month', 'year'))) {
            return '';
        }

        $plan_name = array();

        if ($period_count > 1) {
            // every [x] [period]s
            $plan_name[] = 'Every ' . $period_count . ' ' . ucwords($period . 's');
        } else {
            $plan_name[] = ucwords($period . 'ly');
        }

        if ($trial_days != 0) {
            $plan_name[] = 'with ' . $trial_days . ' day trial';
        }

        return implode(' ', $plan_name);
    }


    private function _create_customer($plan = null, $return = 'record', $verified = false)
    {
        try {
            $stripe_arr = array();
            $subscription_arr = array();
            $metadata = $this->_collect_metadata();

            if (!is_null($plan)) {
                $subscription_arr['plan'] = $plan;
                $subscription_arr['metadata'] = $metadata;

                // If the `subscription_trial_period_days` or `subscription_trial_end` params are set,
                // add the trial to our subscription itself. A `plan` can have `trial_days` but those
                // are for all customers using the plan whereas `subscription_trial_period_days` and
                // `subscription_trial_end` are for just this specific customer's subscription.
                if (!empty($this->data['subscription']['trial_period_days'])) {
                    $subscription_arr['trial_period_days'] = $this->data['subscription']['trial_period_days'];
                }

                if (!empty($this->data['subscription']['trial_end'])) {
                    $subscription_arr['trial_end'] = $this->data['subscription']['trial_end'];
                }

                if (!empty($this->data['coupon']['coupon']['stripe_id'])) {
                    $subscription_arr['coupon'] = $this->data['coupon']['coupon']['stripe_id'];
                    $this->data['plan']['coupon_stripe_id'] = $this->data['coupon']['coupon']['stripe_id'];
                }
            }

            // Do we actually need to create a customer?
            // If this user is logged in have a quick check to see if there's already a customer record for them
            $customer = null;
            if (ee()->session->userdata('member_id') != '0') {
                $customer = $this->_attempt_customer_fetch(ee()->session->userdata('member_id'));
            }

            if ($customer == null) {
                $description = $this->_create_description();

                // $stripe_arr['card'] = $this->data['card']['token'];

                // If this is a subscription, we have to attach the payment method to the customer first.
                if (!is_null($plan)) {
                    $stripe_arr['payment_method'] = $this->data['card']['token'];
                }

                $stripe_arr['email'] = $this->data['customer']['email'];
                $stripe_arr['description'] = $description;
                $stripe_arr['metadata'] = $metadata;

                $customer = \Stripe\Customer::create($stripe_arr);

                $member_id = ee()->session->userdata('member_id');

                if (!empty($member_id)) {
                    ee()->db->insert('charge_customers', array('member_id'=>ee()->session->userdata('member_id'), 'customer_id'=>$customer->id, 'mode'=>$this->get_stripe_mode()));
                }

                ee()->charge_log->log_customer_created(Charge_obj_to_array($customer));
            } else {
                $update = false;
                foreach ($stripe_arr as $key => $val) {
                    $customer->$key = $val;
                    $update = true;
                }

                // Apply this card
                // @todo - make this configurable
                // if (isset($this->data['card']['token']) && $this->data['card']['token'] != '') {
                    // $customer->card = $this->data['card']['token'];
                    // $update = true;
                // }

                // If we're doing a recurring subscription and we are not verifiying an already attached payment,
                // attach the current user's payment method to the Stripe Customer account.
                if (empty($verified) && !empty($subscription_arr)) {
                    $payment_method = \Stripe\PaymentMethod::retrieve($this->data['card']['token']);
                    $payment_method->attach(['customer' => $customer->id]);
                }

                if ($update) {
                    $customer->save();
                    ee()->charge_log->log_customer_updated(Charge_obj_to_array($customer));
                }
            }

            // Check if we need to set a plan balance. Also set the currency here otherwise it'll be set to the default for the Stripe account.
            if (isset($this->data['plan']['balance'])) {
                $invoice_item_arr['amount'] = $this->data['plan']['balance'];
                $invoice_item_arr['currency'] = $this->data['plan']['currency'];
                $invoice_item_arr['customer'] = $customer->id;

                $invoiceItem = \Stripe\InvoiceItem::create($invoice_item_arr);
            }

            $customData = array();

            // Add the subscription to the customer (instead of passing the "plan" in the $customer->save() above).
            if (!empty($subscription_arr)) {
                // Check to see if we're adding a new subscription or if we need to change an existing one.
                if (isset($this->data['current_plan']) && !empty($this->data['current_plan'])) {
                    // Use the Charge hash to retrieve the Stripe Subscription ID.
                    $charge_info = ee()->db->get_where('charge_stripe', array('hash'=>$this->data['current_plan']), 1)->row();

                    // Get the Stripe data.
                    $charge_stripe_data = unserialize(base64_decode($charge_info->stripe));

                    // Check to see if we have a saved Subscription ID. This only exists if a
                    // subscription was saved since this code was put in place.
                    if (isset($charge_stripe_data['subscription_id']) && !empty($charge_stripe_data['subscription_id'])) {
                        $subscription_id = $charge_stripe_data['subscription_id'];
                    } else {
                        // We don't have a subscription_id saved in the DB so we have to try to get it from the Customer Stripe object.
                        $subscription = $customer->subscriptions->data;
                        $subscription = current($subscription);

                        if ($subscription) {
                            $subscription_id = $subscription['id'];
                        }
                    }

                    // Get the Subscription object from Stripe, change the plan to the one we're upgrading to, and save it back to Stripe.
                    $subscription = \Stripe\Subscription::retrieve($subscription_id);
                    $subscription->plan = $subscription_arr['plan'];
                    $subscription->default_payment_method = $this->data['card']['token'];

                    if (!empty($subscription_arr['coupon'])) {
                        $subscription->coupon = $subscription_arr['coupon'];
                    }

                    $subscription->save();
                } else {
                    $subscription_arr['default_payment_method'] = $this->data['card']['token'];

                    $subscription_id = ee()->input->post('sub_id');

                    if (!empty($subscription_id)) {
                        $sub_response = \Stripe\Subscription::retrieve($subscription_id);
                        if ($sub_response->status === 'active') {
                            $subscription_id = $sub_response['id'];
                            $customData['state'] = 'active';
                        }
                    } else {
                        // We're creating a new subscription for the customer.
                        $sub_response = $customer->subscriptions->create($subscription_arr);
                    }

                    if ($sub_response->status === 'incomplete' && !empty($sub_response->latest_invoice)) {
                        $invoice = \Stripe\Invoice::retrieve($sub_response->latest_invoice);
                        $intent = \Stripe\PaymentIntent::retrieve($invoice->payment_intent);
                        $intent->confirm();

                        $paymentResponse = $this->generatePaymentResponse($intent);


                        // Check if something went wrong.
                        if ($paymentResponse['success'] !== true) {
                            // Find out if we need the user's manual confirmation of this payment.
                            if ($paymentResponse['requires_action'] === true) {
                                $customData['state'] = 'pending';
                                $customData['requires_action'] = true;
                                $customData['payment_intent_client_secret'] = $paymentResponse['payment_intent_client_secret'];
                            }
                        } else {
                            $customData['state'] = 'active';
                        }
                    }

                    $subscription_id = $sub_response['id'];
                }

                // Save the subscription_id from Stripe into the Customer data so it gets saved in the DB with the transaction.
                $customer['subscription_id'] = $subscription_id;
            }

            // Pull out the card exp month/year from the customer->card object
            // at the same time we wipe their card record from our account
            $card = $this->_get_customer_card($customer);

            // I think with the new stripe SCA we can remove the following... since it's not attached to the customer at this point... Not 100% sure.
            $this->data['card']['exp_month'] = $card['exp_month'];
            $this->data['card']['exp_year'] = $card['exp_year'];
            $this->data['card']['fingerprint'] = $card['fingerprint'];

            // If the customer had a coupon applied, the value paid may be different from the value passed.
            // Lets hit the api and pull this customer's payments and see
            if (isset($subscription_arr['coupon']) && $subscription_arr['coupon'] != '') {
                $paymentsList = \Stripe\Charge::all(array('customer' => $customer->id, 'limit' => 1));

                // Get the first payment
                $payments = $paymentsList->data;
                if (!empty($payments)) {
                    $payment = current($payments);
                    $amount = $payment->amount;
                    $planAmount = $this->data['plan']['amount'];

                    // Is there a difference between the amount paid and the original plan_amount?
                    if ($amount != $planAmount) {
                        $this->data['plan']['amount'] = $amount;
                        $this->data['plan']['discount'] = $planAmount - $amount;
                        $this->data['plan']['full_amount'] = $planAmount;
                    }
                }
            }

            if (ee()->extensions->active_hook('charge_customer_created') === true) {
                ee()->extensions->call('charge_customer_created', $customer, $this);

                if (ee()->extensions->end_script === true) {
                    return;
                }
            }

            $customer = $this->retrieve_stripe_customer($customer->id);

            // If we had a plan_expiry, check it now, compared to the expected next payment date,
            // and kill the sub right now if it's beyond the expiry
            if (isset($this->data['plan']['length_expiry']) && (empty($sub_response) || $sub_response->status !== 'incomplete')) {
                $state = $this->_check_subscription_expiry($customer); // This will kill it if needed

                if ($state !== false) {
                    $customer = $state;
                }
                // The same logic fires on recurring webhook triggered payments
            }

            // Also record this in our transaction model
            if ($return == 'record') {
                $record = $this->_record($customer, $customData);

                return $record;
            }

            return $customer;
        } catch (Exception $e) {
            $message = $e->getMessage();
            ee()->charge_log->log_exception($message);
            $this->errors[] = $message;
        }

        ee()->charge_log->log_error_creating_customer($this->data);

        return false;
    }


    private function _record($stripe_object, $customData = array())
    {
        ee()->load->helper('String');

        $data = array();

        $clean_stripe = $this->_clean_stripe_obj($stripe_object);

        // Build our array for the record
        $data['hash'] = random_string('unique');
        $data['member_id'] = ee()->session->userdata['member_id'];
        $data['timestamp'] = ee()->localize->now;
        $data['site_id'] = ee()->config->item('site_id');
        $data['source_url'] = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
        $data['stripe'] = base64_encode(serialize($clean_stripe));
        $data['mode'] = ee()->charge_stripe->get_stripe_mode();
        $data['state'] = (!empty($customData['state']) ? $customData['state'] : 'active');
        $data['customer_id'] = ''; // Just in case we can't find the customer_id later
        $data['payment_id'] = ''; // Just in case we can't find this later
        $data['plan_trial_days'] = 0; // Plan Trials days get special treatment
        $data['chargecart_unique_id'] = isset($this->data['chargecart_unique_id']) ? $this->data['chargecart_unique_id'] : '';

        // set the card last 4
        if (!empty($clean_stripe['charges']->data[0]->payment_method_details->card['last4'])) {
            $data['card_last4'] = $clean_stripe['charges']->data[0]->payment_method_details->card['last4'];
        }

        // set the card type
        if (!empty($clean_stripe['charges']->data[0]->payment_method_details->card['brand'])) {
            $data['card_type'] = $clean_stripe['charges']->data[0]->payment_method_details->card['brand'];
        }

        // set the experation year
        if (!empty($clean_stripe['charges']->data[0]->payment_method_details->card['exp_year'])) {
            // this gets put into global data so it can be assigned below.
            $this->data['card']['exp_year'] = $clean_stripe['charges']->data[0]->payment_method_details->card['exp_year'];
        }

        // set the experation month
        if (!empty($clean_stripe['charges']->data[0]->payment_method_details->card['exp_month'])) {
             $this->data['card']['exp_month'] = $clean_stripe['charges']->data[0]->payment_method_details->card['exp_month'];
        }
        // set the card fingerprint
        if (!empty($clean_stripe['charges']->data[0]->payment_method_details->card['fingerprint'])) {
            $this->data['card']['fingerprint'] = $clean_stripe['charges']->data[0]->payment_method_details->card['fingerprint'];
        }

        if (!empty($this->messages)) {
            $data['messages'] = json_encode($this->messages);
        }

        if (isset($clean_stripe['customer'])) {
            $data['customer_id'] = $clean_stripe['customer'];
        } else {
            if (isset($clean_stripe['object']) and $clean_stripe['object'] == 'customer') {
                if (isset($clean_stripe['id'])) {
                    $data['customer_id'] = $clean_stripe['id'];
                }
            }
        }

        if (isset($clean_stripe['id']) && $clean_stripe['object'] == 'charge') {
            $data['payment_id'] = $clean_stripe['id'];
        }

        // Set the card details if we have them
        $meta = array();

        //  First things first, set up our raw empty tags
        foreach ($this->data as $set => $set_data) {
            if ($set == 'chargecart_unique_id') {
                continue;
            }

            if (is_array($set_data) && !empty($set_data)) {
                foreach ($set_data as $key => $val) {
                    if ($set == 'meta') {
                        $meta[$key] = $val;
                    } else {
                        $data[$set . '_' . $key] = $val;
                    }
                }
            }
        }

        if (isset($this->data['plan']['trial_days'])) {
            $data['plan_trial_days'] = $this->data['plan']['trial_days'];
        }

        // If trialing, we need to change the state, and add an expiry
        if ($data['plan_trial_days'] > 0) {
            $data['state'] = 'trialing';
            $data['plan_trial_end'] = strtotime('+' . $data['plan_trial_days'] . ' days', time());
        }

        if (isset($data['plan_interval_count']) and $data['plan_interval_count'] != '0') {
            $data['type'] = 'recurring';
        } else {
            $data['type'] = 'charge';
        }

        if (!empty($meta)) {
            $data['meta'] = base64_encode(serialize($meta));
        }

        if (!empty($customData)) {
            foreach ($customData as $key => $val) {
                $data[$key] = $val;
            }
        }

        $out = array();
        foreach ($data as $key => $val) {
            if ($this->is_attribute($key) && $val != 'null' && $val != '') {
                // This will stop any potential bad items getting in
                // this fixes an issue thrown on some strict mysql setups
                $out[$key] = $val;
            }
        }

        if (empty($customData['requires_action'])) {
            $data['charge_id'] = self::insert($out);
        }

        // We need to return the clean object for later parsing
        $ret = $this->parse($data);

        return $ret;
    }


    public function recordDummy($dummyData)
    {
        return $this->_record(null, $dummyData);
    }

    private function _clean_stripe_obj($object)
    {
        $json = array();

        if (is_object($object)) {
            $json = $object->__toArray();
        }

        return $json;
    }

    private function _collect_metadata()
    {
        $meta = array();

        if ($this->settings->charge_metadata_pass == 'no') {
            return array();
        }

        $meta['Name'] = $this->data['customer']['name'];
        $meta['Email'] = $this->data['customer']['email'];

        if (ee()->session->userdata('member_id') != '0') {
            $meta['Site Member Id'] = ee()->session->userdata('member_id');
        }

        if (isset($this->data['meta']) and !empty($this->data['meta'])) {
            foreach ($this->data['meta'] as $meta_key => $meta_vals) {
                $meta[ucwords(str_replace('_', ' ', $meta_key))] = $meta_vals;
            }
        }

        $this->_full_metadata = $meta;

        if (count($meta) > 20) {
            // We have an issue, Stripe only allows 20 keys per item
            // Break this down. return the first 20, but keep the rest for local records
            $meta = array_slice($meta, 0, 20);
        }

        $meta = array_filter($meta);

        return $meta;
    }

    private function _create_description()
    {
        $ret = 'Payment by ' . $this->data['customer']['name'] . ' (' . $this->data['customer']['email'] . ')';

        return $ret;
    }

    private function _to_array($obj)
    {
        if (is_object($obj)) {
            $obj = $obj->__toArray();
        }

        if (is_array($obj)) {
            $new = array();

            foreach ($obj as $key => $val) {
                $new[$key] = $this->_to_array($val);
            }
        } else {
            $new = $obj;
        }

        return $new;
    }


    private function _clean_array($arr)
    {
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $new = array();

                foreach ($val as $sub_key => $sub_val) {
                    if (!is_numeric($sub_key)) {
                        // Nope, needs to be a multi-dimensional array
                        $new[0][$sub_key] = $sub_val;
                    }
                }

                $arr[$key] = $new;
            }
        }

        return $arr;
    }


    public function make_dotted_card($last_4 = '')
    {
        return '&#183;&#183;&#183;&#183; ' . $last_4;
    }


    public function find_customer_id($member_id = '', $fallback_to_user = true)
    {
        // Check if we've specified a member_id.
        if (empty($member_id)) {
            // If we're not allowed to fallback to the currently logged in user, there's nothing for us to do.
            if ($fallback_to_user == false) {
                return false;
            }

            // Fallback to the currently logged in user!
            $member_id = ee()->session->userdata['member_id'];

            // Double check that we have a member_id from the session.
            if (empty($member_id)) {
                return false;
            }
        }

        // Let's just be safe.
        $member_id = intval($member_id);

        // Try to pull the customer_id from the member_id
        ee()->db->where('member_id', $member_id);
        ee()->db->order_by('id', 'desc');
        $charges = ee()->charge_stripe->get_all(1);

        // It's possible (although unlikely) that we could have multiple customer_ids
        // for the same user. In this case we'll only care about the most recent
        $customer_id = '';
        foreach ($charges as $charge) {
            if ($charge['customer_id'] != '') {
                $customer_id = $charge['customer_id'];
            }
        }

        // Check if we found a customer ID.
        // TECHNICALLY, if we DON'T have a customer ID, we should fallback to the current user
        // but as it wasn't originally programmed this way, we'll leave it as is because we don't
        // have a use case where specifying a member_id for a non-existant user should fall back.
        if ($customer_id == '') {
            return false;
        }

        return $customer_id;
    }

    public function retrieve_stripe_customer($customer_id)
    {
        try {
            $customer = \Stripe\Customer::retrieve($customer_id);

            return $customer;
        } catch (Exception $e) {
            $this->api_error_message = $e->getMessage();

            return false;
        }

        return false;
    }

    public function retrieve_stripe_plan($options = array())
    {
        try {
            // If we have a product_id, retrieve just that one product.
            if (!empty($options['plan_id'])) {
                return \Stripe\Plan::retrieve($options['plan_id']);
            } elseif (!empty($options['name'])) {
                $plans = \Stripe\Plan::all(array('limit' => 100));

                if (empty($plans)) {
                    return false;
                }

                foreach ($plans as $plan) {
                    if ($plan->nickname === $options['name']) {
                        return $plan;
                    }
                }
            }

            return false;
        } catch (Exception $e) {
            $this->api_error_message = $e->getMessage();

            return false;
        }
    }

    public function retrieve_stripe_product($options = array())
    {
        try {
            // If we have a product_id, retrieve just that one product.
            if (!empty($options['product_id'])) {
                return \Stripe\Product::retrieve($options['product_id']);
            } else {
                // Get all the products and loop through them.
                $products = \Stripe\Product::all(array('limit' => 100));

                if (empty($products)) {
                    return false;
                }

                foreach ($products as $product) {
                    if ($product->name === $options['name']) {
                        return $product;
                    }
                }

                return false;
            }
        } catch (Exception $e) {
            $this->api_error_message = $e->getMessage();

            return false;
        }
    }

    public function update_card($customer_id, $data)
    {
        // Get the customer_id
        if (!isset($data['token'])) {
            return false;
        }

        try {
            $customer = \Stripe\Customer::retrieve($customer_id);

            $existing_payment_methods = \Stripe\PaymentMethod::all([
                'customer' => $customer_id,
                'type' => 'card'
            ]);

            // Retrieve the payment method that was setup through Stripe JS using the token.
            $payment_method = \Stripe\PaymentMethod::retrieve($data['token']);

            // Attach the customer to this payment method (which makes it show up under "Cards" in the Stripe GUI).
            $payment_method->attach(['customer' => $customer_id]);

            // Set this new payment method as the default for all invoices.
            $customer->invoice_settings->default_payment_method = $data['token'];

            // Save the Stripe Customer so our changes take effect.
            $customer->save();

            // Only remove the _other_ cards, once we've successfully added a new card. If there was an
            // error adding the new card, the `catch` below would be triggered and this part wouldn't fire.
            foreach ($existing_payment_methods as $existing_method) {
                $existing_method->detach();
            }

            // Now update all thise relevant db records with this new card
            $record = array();
            $record['card_last4'] = $payment_method->card->last4;
            $record['card_type'] = strtolower($payment_method->card->brand);
            $record['card_exp_month'] = $payment_method->card->exp_month;
            $record['card_exp_year'] = $payment_method->card->exp_year;
            $record['card_fingerprint'] = $payment_method->card->fingerprint;
            $record['card_customer'] = $payment_method->customer;
            $record['card_country'] = $payment_method->card->country;
            $record['card_name'] = $payment_method->billing_details->name;
            $record['card_address_line1'] = $payment_method->billing_details->address->line1;
            $record['card_address_line2'] = $payment_method->billing_details->address->line2;
            $record['card_address_city'] = $payment_method->billing_details->address->city;
            $record['card_address_state'] = $payment_method->billing_details->address->state;
            $record['card_address_zip'] = $payment_method->billing_details->address->postal_code;
            $record['card_address_country'] = $payment_method->billing_details->address->country;

            ee()->db->where('customer_id', $customer_id)
                ->where('type', 'recurring')
                ->where('state', 'active');
            $customer_charges = self::get_all();

            foreach ($customer_charges as $charge) {
                self::update($charge['id'], $record);
            }

            return true;
        } catch (Exception $e) {
            $this->api_error_message = $e->getMessage();

            return false;
        }
    }

    public function cards($customer_id)
    {
        if ($customer_id == '') {
            return false;
        }

        // @todo add caching on this call
        try {
            $customer = \Stripe\Customer::retrieve($customer_id);

            if ($customer == null) {
                return false;
            }

            $payment_methods = \Stripe\PaymentMethod::all([
                'customer' => $customer_id,
                'type' => 'card'
            ]);

            if (empty($payment_methods['data'])) {
                return array();
            }

            foreach ($payment_methods['data'] as $method) {
                $temp = array();

                $data[] = $this->convertPaymentMethodToCard($method, 'card_');
            }

            return $data;
        } catch (Exception $e) {
            $this->api_error_message = $e->getMessage();

            return false;
        }
    }

    private function convertPaymentMethodToCard($method, $prefix = '')
    {
        $card = array();

        $card[$prefix . 'id'] = $method->id;
        $card[$prefix . 'object'] = $method->object;
        $card[$prefix . 'address_city'] = $method->billing_details->address->city;
        $card[$prefix . 'address_country'] = $method->billing_details->address->country;
        $card[$prefix . 'address_line1'] = $method->billing_details->address->line1;
        $card[$prefix . 'address_line1_check'] = $method->card->checks->address_line1_check;
        $card[$prefix . 'address_line2'] = $method->billing_details->address->line2;
        $card[$prefix . 'address_state'] = $method->billing_details->address->state;
        $card[$prefix . 'address_zip'] = $method->billing_details->address->postal_code;
        $card[$prefix . 'address_zip_check'] = $method->card->checks->address_postal_code_check;
        $card[$prefix . 'brand'] = $this->cardMapBrandToName($method->card->brand);
        $card[$prefix . 'country'] = $method->card->country;
        $card[$prefix . 'customer'] = $method->customer;
        $card[$prefix . 'cvc_check'] = $method->card->checks->cvc_check;
        $card[$prefix . 'dynamic_last4'] = null;
        $card[$prefix . 'exp_month'] = $method->card->exp_month;
        $card[$prefix . 'exp_year'] = $method->card->exp_year;
        $card[$prefix . 'fingerprint'] = $method->card->fingerprint;
        $card[$prefix . 'funding'] = $method->card->funding;
        $card[$prefix . 'last4'] = $method->card->last4;
        $card[$prefix . 'metadata'] = $method->metadata;
        $card[$prefix . 'name'] = $method->billing_details->name;
        $card[$prefix . 'tokenization_method'] = null;
        $card[$prefix . 'type'] = $method->card->brand;
        $card[$prefix . 'number_dotted'] = $this->make_dotted_card($card[$prefix . 'last4']);
        $card[$prefix . 'created'] = $method->created;

        return $card;
    }

    private function cardMapBrandToName($brand)
    {
        switch ($brand) {
            case 'amex':
                return 'American Express';
                break;

            case 'diners':
                return 'Diners Club';
                break;

            case 'discover':
                return 'Discover';
                break;

            case 'jcb':
                return 'JCB';
                break;

            case 'mastercard':
                return 'MasterCard';
                break;

            case 'unionpay':
                return 'UnionPay';
                break;

            case 'visa':
                return 'Visa';
                break;

            default:
                return 'Unknown';
        }
    }

    private function _check_subscription_expiry($customer, $charge = array())
    {
        if (empty($charge)) {
            $charge = array('plan_interval'       => $this->data['plan']['interval'],
                            'plan_interval_count' => $this->data['plan']['interval_count'],
                            'plan_length_expiry'  => $this->data['plan']['length_expiry']);
        }

        if ($charge['plan_length_expiry'] == 0) {
            return false;
        }

        // Figure out the next expected bill date based on the interval values
        $next_payment_due = strtotime('+' . $charge['plan_interval_count'] . $charge['plan_interval']);


        // These are to precise to be realiable, so we'll fudge them slightly
        // to catch the case of immediate cancellation. Give it a 10 minute window
        if ($charge['plan_length_expiry'] > ($next_payment_due + 600)) {
            return false;
        } // Nothing to do right now

        // Pull out the sub_id
        $subscription = $customer->subscriptions->data;
        $subscription = current($subscription);

        // Cancel it
        \Stripe\Subscription::update(
            $subscription->id,
            [
                'cancel_at_period_end' => true,
            ]
        );

        // $cancelled = $subscription->cancel();//array('at_period_end' => true));
        // $cancelled = $customer->cancelSubscription($customer, array('cancel_at_period_end' => true));

        // Pull the updated cusomter details so the db record is consistent
        $customer = \Stripe\Customer::retrieve($customer->id);

        return $customer;
    }


    public function handle_plan_expiry($charge)
    {
        if (isset($charge['plan_length_expiry']) and is_numeric($charge['plan_length_expiry'])) {
            // Get the customer
            $customer = \Stripe\Customer::retrieve($charge['customer_id']);

            $customer = $this->_check_subscription_expiry($customer, $charge);

            // Plan not expired
            if ($customer === false) {
                return false;
            }

            // Otherwise we'll update our state just to be sure
            $data['state'] = 'expired';
            $data['ended_on'] = time();

            self::update($charge['id'], $data);

            // Log that we expired the subscription.
            ee()->charge_log->log_event('plan_expired', $data);

            return true;
        }
    }


    public function trigger_trial_end($charge_id)
    {
        $update['plan_trial_end'] = '';
        $update['state'] = 'active';

        self::update($charge_id, $update);


        if (ee()->extensions->active_hook('charge_subscription_trial_end') === true) {
            // Get the charge object so we can pass it to the hook
            $charge = self::get_one($charge_id);

            ee()->extensions->call('charge_subscription_trial_end', $charge);
            if (ee()->extensions->end_script === true) {
                return;
            }
        }

        return $update;
    }


    public function check_coupon_exists($coupon_code)
    {
        try {
            $c = \Stripe\Coupon::retrieve($coupon_code);

            return $c;
        } catch (Stripe_InvalidRequestError $e) {
            if ($e->getHttpStatus() == '404') {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }

        return false;
    }

    public function delete_coupon($stripe_id)
    {
        $response = array();

        try {
            $c = \Stripe\Coupon::retrieve($stripe_id);
            $c->delete();

            return true;
        } catch (Exception $e) {
            $this->errors[] = 'Failed to delete coupon - ' . $e->getMessage();

            return false;
        }

        return false;
    }

    public function create_coupon($data)
    {
        $response = array();

        try {
            // Collect our neat array of attributes
            $coupon = array();
            $coupon['id'] = $data['stripe_id'];

            if ($data['type'] == 'fixed') {
                $coupon['amount_off'] = $data['amount_off'];
                $coupon['currency'] = $data['currency'];
            } elseif ($data['type'] == 'percentage') {
                $coupon['percent_off'] = $data['percentage_off'];
            }

            $coupon['duration'] = $data['duration'];
            if ($data['duration'] == 'repeating') {
                $coupon['duration_in_months'] = $data['duration_in_months'];
            }

            if ($data['max_redemptions'] != '' and $data['max_redemptions'] != '0') {
                $coupon['max_redemptions'] = $data['max_redemptions'];
            }
            if (isset($data['end_date']) and $data['end_date'] != '' and $data['end_date'] != 0) {
                $coupon['redeem_by'] = strtotime($data['end_date']);
            }

            // We need to create for both test and live
            $this->_setup_stripe('test');
            $c = \Stripe\Coupon::create($coupon);

            try {
                //echo('setting to live');
                $this->_setup_stripe('live');
                $cl = \Stripe\Coupon::create($coupon);
            } catch (Exception $e) {
                // Allow this.
                $this->errors[] = 'Coupon - ' . $e->getMessage();
            }

            // Revert back to the real mode for now
            $this->_setup_stripe();
            return $c;
        } catch (Exception $e) {
            $this->errors[] = 'Coupon - ' . $e->getMessage();
            return false;
        }

        return false;
    }

    public function get_customer($member_id = '')
    {
        if ($member_id == '') {
            $member_id = ee()->session->userdata('member_id');
        }

        if ($member_id == '0' || $member_id == null) {
            return false;
        }

        $customer = $this->_attempt_customer_fetch($member_id);

        if ($customer == null) {
            return false;
        }

        return $customer;
    }

    private function _attempt_customer_fetch($member_id)
    {
        // Try to get the customer_id out of the new `exp_charge_customers` table
        // and if not, fall back to loading it from the transactions table.
        $customer_result = ee()->db->where('member_id', $member_id)
            ->where('mode', $this->get_stripe_mode())
            ->get('charge_customers')
            ->row();

        if (!empty($customer_result->customer_id)) {
            try {
                $customer = \Stripe\Customer::retrieve($customer_result->customer_id);

                if (!empty($customer)) {
                    ee()->charge_log->log_fetched_customer($customer->id);
                    return $customer;
                }
            } catch (Exception $e) {
                ee()->charge_log->log_event('customer_not_found', array('member_id'=>$member_id, 'customer_result'=>$customer_result));
            }
        }

        ee()->db->where('member_id', $member_id)
            ->where('mode', $this->get_stripe_mode())
            ->order_by('id', 'desc');

        $res = self::get_all(1);

        if (empty($res)) {
            return null;
        }

        $row = current($res);

        $customer_id = $row['customer_id'];

        if ($customer_id == '') {
            return null;
        }

        try {
            $customer = \Stripe\Customer::retrieve($customer_id);

            if ($customer != null && (!isset($customer->deleted) || (isset($customer->deleted) && $customer->deleted == false))) {
                ee()->charge_log->log_fetched_customer($customer);
                return $customer;
            }
        } catch (Exception $e) {
        }

        return null;
    }
} // End class
/* End of file Charge_stripe_model.php */
