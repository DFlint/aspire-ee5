<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Charge Log Model class
 *
 * @package         charge_ee_addon
 * @author          Tom Jaeger <Tom@EEHarbor.com>
 * @link            https://eeharbor.com/charge
 * @copyright       Copyright (c) 2016, Tom Jaeger/EEHarbor
 */
class Charge_log_model extends Charge_model
{

    // --------------------------------------------------------------------
    // METHODS
    // --------------------------------------------------------------------

    // create new : plus info
    // customer created : user info
    // error : remove error
    // exception : asterisk error /or/ warning-sign
    // info : info-sign info
    // action : transfer info


    private $icon_map = array('exception' => array('icon' => 'exclamation-triangle', 'type' => 'error'),
                              'error'     => array('icon' => 'times-circle', 'type' => 'error'),
                              'warning'   => array('icon' => 'exclamation-circle', 'type' => 'warning'),
                              'success'   => array('icon' => 'heart', 'type' => 'success'),
                              'created'   => array('icon' => 'heart', 'type' => 'success'),
                              'attempt'   => array('icon' => 'shopping-cart', 'type' => 'info'),
                              'customer'  => array('icon' => 'user', 'type' => 'info'),
                              'action'    => array('icon' => 'cog', 'type' => 'info'),
                              'info'      => array('icon' => 'info-sign', 'type' => 'info'));

    private $request_key = '';
    private $settings;

    /**
     * Constructor
     *
     * @access      public
     * @return      void
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();

        $this->settings = $this->flux->getSettings();

        // Initialize this model
        $this->initialize(
            'charge_log',
            'log_id',
            array(
                'site_id'     => 'int(4) unsigned NOT NULL default 1',
                'timestamp'   => 'int(10) unsigned NOT NULL default 0',
                'type'        => 'varchar(100) NOT NULL default ""',
                'message'     => 'varchar(255) NOT NULL default ""',
                'member_id'   => 'int(10) unsigned NOT NULL default 0',
                'extended'    => 'mediumtext',
                'request_key' => 'varchar(255) NOT NULL default ""',
                'mode'        => 'varchar(100) NOT NULL default ""')
        );

        if ($this->request_key == '') {
            $this->request_key = random_string('unique');
        }
    }

    // --------------------------------------------------------------------

    /**
     * Installs given table
     *
     * @access      public
     * @return      void
     */
    public function install()
    {
        // Call parent install
        parent::install();

        // Add indexes to table
        ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`site_id`)");
        ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`member_id`)");
    }


    public function clear_all()
    {
        ee()->db->query("TRUNCATE {$this->table()} ");
    }

    // --------------------------------------------------------------

    public function get_all($limit = 50, $offset = 0)
    {
        ee()->db->order_by('log_id', 'desc');
        ee()->db->limit($limit, $offset);
        $data = parent::get_all();

        $ret = array();

        foreach ($data as $row) {
            $row['extended'] = unserialize(base64_decode($row['extended']));
            $row['time_wordy'] = date('H:i', $row['timestamp']) . ' on ' . date('l jS F', $row['timestamp']);

            $row['log_icon'] = $this->_make_icon($row['type']);

            $ret[] = $row;
        }


        return $ret;
    }

    public function get_all_threaded($limit = 50, $offset = 0)
    {
        ee()->db->order_by('log_id', 'desc')
            ->group_by('log_id')
            ->group_by('request_key')
            ->limit($limit, $offset);
        $data = parent::get_all();


        $keys = array();
        foreach ($data as $row) {
            $keys[$row['request_key']] = array();
        }

        if (empty($keys)) {
            return array();
        }

        // Get all the log lines for each request key
        ee()->db->order_by('log_id', 'asc')
            ->limit('99999999')
            ->where_in('request_key', array_keys($keys));
        $full = parent::get_all();


        foreach ($full as $row) {
            $row['extended'] = unserialize(base64_decode($row['extended']));
            $row['time_wordy'] = date('Y/m/d', $row['timestamp']).' at '.date('H:i', $row['timestamp']);
            $row['log_icon'] = $this->_make_icon($row['type']);

            $keys[$row['request_key']][] = $row;
        }


        return $keys;
    }

    private function _get_log_level()
    {
        $level = $this->settings->charge_log_level;

        if ($level == '') {
            $level = '5';
        }

        return $level;
    }

    private function _make_icon($row_type = '')
    {
        // Make the icons
        $icon_base = '';
        $marker = '';
        $type = '';

        if ($row_type != '') {
            $parts = explode('_', $row_type);

            foreach ($parts as $part) {
                if ($part == 'charge') {
                    continue;
                }

                if (isset($this->icon_map[$part])) {
                    $marker = $this->icon_map[$part]['icon'];
                    $type = $this->icon_map[$part]['type'];
                    $icon_base = '<i class="fa fa-{marker} fa-fw fa-sm {type}"></i>';

                    return str_replace(array('{marker}', '{type}'), array($marker, $type), $icon_base);
                }
            }
        }

        return str_replace(array('{marker}', '{type}'), array($marker, $type), $icon_base);
    }

    // --------------------------------------------------------------

    public function log($data = array())
    {
        if ($this->_get_log_level() < 1) {
            return true;
        }

        if (empty($data)) {
            return false;
        }

        if (!is_array($data)) {
            $arr = array('message' => $data);
            $data = $arr;
        }

        if (!isset($data['member_id']) or $data['member_id'] == 0) {
            $data['member_id'] = ee()->session->userdata('member_id');
        }
        if (!isset($data['site_id'])) {
            $data['site_id'] = ee()->config->item('site_id');
        }
        if (!isset($data['timestamp'])) {
            $data['timestamp'] = ee()->localize->now;
        }
        if (!isset($data['type']) or $data['type'] == '') {
            $data['type'] = '';
        }
        if (!isset($data['mode'])) {
            $data['mode'] = ee()->charge_stripe->get_stripe_mode();
        }

        if (isset($data['extended'])) {
            if (!is_array($data['extended'])) {
                $data['extended'] = array($data['extended']);
            }
            $data['extended'] = base64_encode(serialize($data['extended']));
        }

        $data['request_key'] = $this->request_key;
        self::insert($data);

        return true;
    }


    // --------------------------------------------------------------
    public function log_event($name, $data = array())
    {
        $log_data = array();
        $log_data['type'] = 'charge_' . $name;
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    public function log_action_response($name, $data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_action_' . $name;
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_request_start()
    {
        $log_data = array();
        $log_data['type'] = 'charge_request_start';

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_action_start($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_action_start';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }


    // --------------------------------------------------------------

    public function log_action_create_member_end($member_id)
    {
        $log_data = array();
        $log_data['type'] = 'charge_member_create_end';
        $log_data['extended'] = array('member_id' => $member_id);

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_action_create_member_start($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_member_create_start';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_attempt_start($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_attempt_start';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_attempt_start_one_off($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_attempt_start_one_off';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_attempt_start_recurring($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_attempt_start_recurring';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_plan_created($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_plan_created';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_set_length_plan_attempt_cancel($data = array())
    {
        $log_data = array();
        $log_data['type'] = 'charge_set_length_attempt_cancel';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_set_length_plan_cancelled($data = array())
    {
        $log_data = array();
        $log_data['type'] = 'charge_set_length_cancelled';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_customer_created($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_customer_created';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }


    // --------------------------------------------------------------

    public function log_customer_updated($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_customer_updated';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_charge_created($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_charge_created';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_error_creating_customer($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_error_creating_customer';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_error_creating_charge($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_error_creating_charge';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_error_failed_general($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_error_failed_general';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_error_no_plan($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_error_no_plan';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_error_no_customer($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_error_no_customer';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_exception($data = array())
    {
        if (!is_array($data)) {
            $arr = array('message' => $data);
            $data = $arr;
        }

        $log_data = $data;
        $log_data['type'] = 'charge_exception';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }


    // --------------------------------------------------------------

    public function log_fetched_customer($customer)
    {
        $log_data = array();
        $log_data['type'] = 'charge_customer_fetched';
        $log_data['extended'] = $customer;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_stripe_exception($data = array())
    {
        if (!is_array($data)) {
            $arr = array('message' => $data);
            $data = $arr;
        }

        $log_data = $data;
        $log_data['type'] = 'charge_stripe_exception';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }


    // --------------------------------------------------------------

    public function log_action_trigger_start($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_trigger_start';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_action_failed_to_decode($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_trigger_failed_to_decode_action';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_action_failed_to_run($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_trigger_failed_to_run';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_error_plan_creation($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_error_on_plan_creation';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }

    // --------------------------------------------------------------

    public function log_action_ran($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_trigger_ran_successfully';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }


    public function log_plan_create_start($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_plan_create_start';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }


    public function log_webhook_bad_trigger($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_webhook_bad_trigger';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }


    public function log_webhook_empty($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_webhook_empty';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }


    public function log_webhook_good($data = array())
    {
        $log_data = $data;
        $log_data['type'] = 'charge_webhook_good';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }


    public function log_webhook_started($data = array())
    {
        // Only write if enabled
        // @todo

        if ($this->_get_log_level() < 9) {
            $log_data = $data;
            $log_data['type'] = 'charge_webhook_started';
            $log_data['extended'] = $data;

            $this->log($log_data);
        }

        return true;
    }

    public function log_webhook_handle_failure($data = array())
    {
        $log_data = array();
        $log_data['type'] = 'charge_webhook_exception';
        $log_data['extended'] = $data;

        $this->log($log_data);

        return true;
    }
} // End class

/* End of file Charge_log_model.php */
