<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Charge Subscription Model class
 *
 * @package         charge_ee_addon
 * @author          Tom Jaeger <Tom@EEHarbor.com>
 * @link            https://eeharbor.com/charge
 * @copyright       Copyright (c) 2016, Tom Jaeger/EEHarbor
 */
class Charge_subscription_model extends Charge_model
{
    public $settings = array('email_member_welcome'   =>
                                 array('enabled'  => 'bool',
                                       'template' => 'required',
                                       'subject'  => 'required',
                                       'bcc'      => 'optional'),
                             'email_member_recurring' =>
                                 array('enabled'  => 'bool',
                                       'template' => 'required',
                                       'subject'  => 'required',
                                       'bcc'      => 'optional'),
                             'email_member_failure'   =>
                                 array('enabled'  => 'bool',
                                       'template' => 'required',
                                       'subject'  => 'required',
                                       'bcc'      => 'optional'),
                             'email_member_change'    =>
                                 array('enabled'  => 'bool',
                                       'template' => 'required',
                                       'subject'  => 'required',
                                       'bcc'      => 'optional'));
    public $this_settings = array();

    public $errors = array();
    private $groups = array();


    // --------------------------------------------------------------------
    // METHODS
    // --------------------------------------------------------------------

    /**
     * Constructor
     *
     * @access      public
     * @return      void
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();

        // Initialize this model
        $this->initialize(
            'charge_subscription',
            'subscription_id',
            array(
                'site_id'              => 'int(4) unsigned NOT NULL default 0',
                'timestamp'            => 'int(10) unsigned NOT NULL default 0',
                'member_group_valid'   => 'int(10) unsigned NOT NULL default 0',
                'member_group_invalid' => 'int(10) unsigned NOT NULL default 0',
                'settings'             => 'text',
                'name'                 => 'varchar(255) NOT NULL default ""')
        );
    }

    // --------------------------------------------------------------------

    /**
     * Installs given table
     *
     * @access      public
     * @return      void
     */
    public function install()
    {
        // Call parent install
        parent::install();

        // Add indexes to table
        ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`site_id`)");
    }


    // --------------------------------------------------------------------

    public function add_member($subscription_id, $customer_id, $charge_id)
    {
        $sub = self::get_one($subscription_id);
        if (empty($sub)) {
            return false;
        }

        $member_id = ee()->session->userdata('member_id');


        $group_id = $this->_get_member_group($sub['member_group_valid']);
        if ($group_id == false) {
            return false;
        }


        // We might be moving the customer between subscriptions
        // Let's check if we are first
        $existing_sub = $this->_get_existing($customer_id);
        if ($existing_sub !== false) {
            if (ee()->extensions->active_hook('charge_subscription_start_move_member') === true) {
                ee()->extensions->call('charge_subscription_start_move_member', $member_id, $group_id, $subscription_id, $sub);
                if (ee()->extensions->end_script === true) {
                    return false;
                }
            }

            $this->_move_member($member_id, $group_id);
            ee()->charge_subscription_member->update_subscription($customer_id, $subscription_id);

            if (ee()->extensions->active_hook('charge_subscription_end_move_member') === true) {
                ee()->extensions->call('charge_subscription_end_move_member', $member_id, $group_id, $subscription_id, $sub);
                if (ee()->extensions->end_script === true) {
                    return false;
                }
            }
        } else {
            $sub_data = array('member_id'       => $member_id,
                              'subscription_id' => $subscription_id,
                              'timestamp'       => ee()->localize->now,
                              'site_id'         => ee()->config->item('site_id'),
                              'status'          => 'active',
                              'customer_id'     => $customer_id,
                              'charge_id'       => $charge_id);


            if (ee()->extensions->active_hook('charge_subscription_start_add_member') === true) {
                ee()->extensions->call('charge_subscription_start_add_member', $member_id, $group_id, $sub_data, $sub);
                if (ee()->extensions->end_script === true) {
                    return false;
                }
            }

            $this->_move_member($member_id, $group_id);
            ee()->charge_subscription_member->insert($sub_data);

            if (ee()->extensions->active_hook('charge_subscription_end_add_member') === true) {
                ee()->extensions->call('charge_subscription_end_add_member', $member_id, $group_id, $sub_data, $sub);
                if (ee()->extensions->end_script === true) {
                    return false;
                }
            }
        }

        return true;
    }


    private function _get_existing($customer_id)
    {
        ee()->db->where('status', 'active');
        $member = ee()->charge_subscription_member->get_one($customer_id, 'customer_id');
        if (empty($member)) {
            return false;
        }

        return $member;
    }



    public function remove_member($subscription_member_id)
    {
        // Get the submember details
        $sub_member = ee()->charge_subscription_member->get_one($subscription_member_id, 'subscription_member_id');
        if (empty($sub_member)) {
            return false;
        }

        // Get the subscription itself
        $sub = self::get_one($sub_member['subscription_id']);
        if (empty($sub)) {
            return false;
        }


        // Move the member
        $group_id = $this->_get_member_group($sub['member_group_invalid']);
        if ($group_id == false) {
            return false;
        }

        if (ee()->extensions->active_hook('charge_subscription_start_remove_member') === true) {
            ee()->extensions->call('charge_subscription_start_remove_member', $sub_member['member_id'], $group_id, $sub_member, $sub);
            if (ee()->extensions->end_script === true) {
                return false;
            }
        }

        $this->_move_member($sub_member['member_id'], $group_id);
        $sub_data = array('timestamp' => ee()->localize->now,
                          'status'    => 'inactive');

        ee()->charge_subscription_member->update($subscription_member_id, $sub_data);

        if (ee()->extensions->active_hook('charge_subscription_end_remove_member') === true) {
            ee()->extensions->call('charge_subscription_end_remove_member', $sub_member['member_id'], $group_id, $sub_member, $sub);
            if (ee()->extensions->end_script === true) {
                return false;
            }
        }

        return true;
    }



    public function get_all()
    {
        $subs = parent::get_all();

        $this->groups = ee()->charge_model->get_member_groups();

        foreach ($subs as $sub_key => $sub_val) {
            $subs[$sub_key]['settings'] = unserialize($sub_val['settings']);

            $subs[$sub_key]['auto_description'] = $this->_build_description($subs[$sub_key]);
        }

        return $subs;
    }

    public function get_one($id, $attr = false)
    {
        $item = parent::get_one($id);

        $this->groups = ee()->charge_model->get_member_groups();

        // Decode the settings
        $item['settings'] = unserialize($item['settings']);
        $item['auto_description'] = $this->_build_description($item);

        return $item;
    }


    public function save($subscription_id = 0)
    {
        $this->this_settings = $this->_get_settings();

        $this->this_settings['name'] = ee()->input->post('name');
        $this->this_settings['member_group_valid'] = ee()->input->post('member_group_valid');
        $this->this_settings['member_group_invalid'] = ee()->input->post('member_group_invalid');


        if (!isset($this->this_settings['name']) or $this->this_settings['name'] == '') {
            $this->errors['name'] = lang('charge_subscription_no_name');
        }
        if (!isset($this->this_settings['member_group_valid']) or $this->this_settings['member_group_valid'] == '') {
            $this->errors['member_group_valid'] = lang('charge_subscription_no_valid_group');
        }
        if (!isset($this->this_settings['member_group_invalid']) or $this->this_settings['member_group_invalid'] == '') {
            $this->errors['member_group_invalid'] = lang('charge_subscription_no_invalid_group');
        }

        if (!empty($this->errors)) {
            return false;
        }


        // Ok, looks valid, set it up to dave
        $data['name'] = $this->this_settings['name'];
        $data['member_group_valid'] = $this->this_settings['member_group_valid'];
        $data['member_group_invalid'] = $this->this_settings['member_group_invalid'];

        unset($this->this_settings['name']);
        unset($this->this_settings['member_group_valid']);
        unset($this->this_settings['member_group_invalid']);

        if (!empty($this->this_settings)) {
            $data['settings'] = serialize($this->this_settings);
        }

        $data['timestamp'] = time();
        $data['site_id'] = ee()->config->item('site_id');

        if ($subscription_id == 0 or $subscription_id == '') {
            // New
            self::insert($data);
        } else {
            self::update($subscription_id, $data);
        }

        return true;
    }


    private function _get_member_group($group_id)
    {
        $group = array();

        if (APP_VER > 2.5) {
            ee()->load->model('Member_group_model');

            $group = ee()->Member_group_model->get(array('group_id' => $group_id))->row_array();
        } else {
            $group = ee()->db->where('group_id', $group_id)
                ->from('member_groups')
                ->get()
                ->row_array();
        }
        if (empty($group)) {
            return false;
        }

        return $group['group_id'];
    }

    private function _move_member($member_id, $group_id)
    {
        ee()->db->where('member_id', $member_id)
            ->update('members', array('group_id' => $group_id));
    }


    private function _get_settings()
    {
        $settings = array();


        foreach ($this->settings as $setting_group_key => $setting_group) {
            $this_bool = false;

            foreach ($setting_group as $key => $validation) {
                if (ee()->input->post($setting_group_key . '_' . $key) != '') {
                    $settings[$setting_group_key . '_' . $key] = ee()->input->post($setting_group_key . '_' . $key);
                }

                if ($validation == 'bool') {
                    $this_bool = (ee()->input->post($setting_group_key . '_' . $key) == 'yes' ? true : false);
                }

                if ($this_bool and $validation == 'required' and ee()->input->post($setting_group_key . '_' . $key) == '') {
                    $this->errors[$setting_group_key . '_' . $key] = lang('charge_required_setting');
                }
            }
        }


        return $settings;
    }

    private function _build_description($sub)
    {
        $ret = 'Moves member to <strong>' . $this->groups[$sub['member_group_valid']] . '</strong>';

        $emails = array();
        foreach (array('email_member_welcome_enabled' => 'Welcome', 'email_member_recurring_enabled' => 'Recurring', 'email_member_failure_enabled' => 'Failure', 'email_member_change_enabled' => 'Change') as $email => $key) {
            if (isset($sub['settings'][$email]) and $sub['settings'][$email] == 'yes') {
                $emails[] = $key;
            }
        }


        if (!empty($emails)) {
            $ret .= ' and sends <strong>';

            $ret .= implode('</strong>, <strong>', $emails);

            $ret .= '</strong> emails';
        }


        return $ret;
    }
} // End class

/* End of file Charge_webhook_model.php */
