<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Charge Webhook Model class
 *
 * @package         charge_ee_addon
 * @author          Tom Jaeger <Tom@EEHarbor.com>
 * @link            https://eeharbor.com/charge
 * @copyright       Copyright (c) 2016, Tom Jaeger/EEHarbor
 */
class Charge_webhook_model extends Charge_model
{
    private $events_map = array(
        'customer.subscription.created' => 'start',
        'invoice.payment_succeeded'     => 'start',
        'customer.subscription.deleted' => 'end',
        'customer.subscription.updated' => 'change');
    private $charge_id = null;
    private $customer_id = null;
    private $updateStripeObjectRecord = true;
    private $callType;
    private $callMode;
    private $validCallType = false;

    public $errors = array();
    public $warnings = array();

    // --------------------------------------------------------------------
    // METHODS
    // --------------------------------------------------------------------

    /*
     * Constructor
     *
     * @access      public
     * @return      void
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();

        // Initialize this model
        $this->initialize(
            'charge_webhook',
            'call_id',
            array(
                'site_id'   => 'int(4) unsigned NOT NULL default 0',
                'timestamp' => 'int(10) unsigned NOT NULL default 0',
                'mode'      => 'varchar(100) NOT NULL default ""',
                'id'        => 'varchar(100) NOT NULL default ""',
                'type'      => 'varchar(100) NOT NULL default ""',
                'body'      => 'text',
                'status'    => 'varchar(100) NOT NULL default ""')
        );
    }

    // --------------------------------------------------------------------

    /**
     * Installs given table
     *
     * @access      public
     * @return      void
     */
    public function install()
    {
        // Call parent install
        parent::install();

        // Add indexes to table
        ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`site_id`)");
        ee()->db->query("ALTER TABLE {$this->table()} ADD INDEX (`type`)");

        $this->_set_key();
    }

    public function act($body)
    {
        $state = $this->_act($body);

        if ($state) {
            // We can trigger some extra actions now
            if ($this->updateStripeObjectRecord == true && $this->validCallType && $this->charge_id != null) {
                // Update the customer record with an up to date customer record
                $this->_update_customer_stripe_record();
            }
        }

        return $state;
    }

    private function _update_customer_stripe_record()
    {
        if ($this->charge_id == null || $this->customer_id == null) {
            return;
        }

        // Hit the api for the latest customer record version
        $customer = ee()->charge_stripe->retrieve_stripe_customer($this->customer_id);
        if ($customer == false) {
            return;
        }

        $customer = $customer->__toArray();

        // Encode and update
        $encoded = base64_encode(serialize($customer));


        $update['stripe'] = $encoded;
        ee()->charge_stripe->update($this->charge_id, $update);

        return true;
    }

    private function _act($body)
    {
        $this->errors[] = 'An error was hit handling this webhook event';

        // Pull out the type to start
        if (!isset($body['type'])) {
            $this->errors[] = 'No body type found';
            return false;
        }

        $type = $body['type'];
        $mode = 'test';
        if ($body['livemode'] === true) {
            $mode = 'live';
        }

        $this->callType = $type;
        $this->callMode = $mode;

        // Extract the customer_id
        $customer_id = $this->extract_customer_id($body);
        $invoice_value = $this->extract_invoice_value($body);

        if (ee()->extensions->active_hook('charge_webhook_received_start') === true) {
            ee()->extensions->call('charge_webhook_received_start', $type, $mode, $body);

            if (ee()->extensions->end_script === true) {
                $this->errors[] = 'charge_webhook_received_start extension call returning early';
                return;
            }
        }

        if (!isset($this->events_map[$type])) {
            // Nothing to do really
            $this->errors[] = 'Event type ('.$type.') isnt associated with an action';
            return true;
        }

        $this->validCallType = true;

        // We seem to have something to do
        // We must find the relevant member for this event
        $action_state = $this->events_map[$type];

        if ($customer_id === false) {
            $this->errors[] = 'Failed to get the customer id from the webhook body';
            return false;
        }

        // Get this customer's subscription
        if ($customer_id == 'cus_00000000000000') {
            // Dummy member, coming from test webhook
            $this->errors[] = 'This is a test webhook event';
            return false;

            // Fire anyway?
            //$customer_id = 'cus_5eVSluxZ6QoY0w';
        }

        // Get the original charge that started this subscription
        $charge = ee()->charge_stripe->get_one($customer_id, 'customer_id');
        if (empty($charge)) {
            $charge = $this->_check_bad_charge_state($customer_id);
            if (empty($charge)) {
                $this->errors[] = 'No matching charge customer for this webhook customer (' . $customer_id . ')';
                return false;
            }
        }

        // Good webhook, pull out the data
        ee()->charge_log->log_webhook_good($body);

        $this->charge_id = $charge['id'];
        $this->customer_id = $customer_id;

        // Update the stripe record here too
        $this->_update_customer_stripe_record();

        // We do this right now just in case
        // Now we need to refetch the charge again.
        // this will have the latest version of the stripe array.
        $charge = ee()->charge_stripe->get_one($customer_id, 'customer_id');

        // Handle set length plans
        if ($type == 'invoice.payment_succeeded' and $charge['plan_length_expiry'] != '') {

            // This will kill the plan if needed.
            // If it's killed, this will trigger a fresh set of hook events
            // but that's invisible to this action, so we handle it all atomically
            ee()->charge_stripe->handle_plan_expiry($charge);
        }

        // Handle the empty invoice success we recieve when a trial plan is created
        // with a zero balance paid
        if ($type == 'invoice.payment_succeeded') {
            if ($invoice_value !== false and $invoice_value <= 0) {
                // We have a zero balance invoice from a trial success
                return true;
            }
        }

        // Check if we have subscriptions setup at all, otherwise there's nothing left to do.
        $num_subscriptions = ee()->db->count_all('charge_subscription');
        if ($num_subscriptions == 0) {
            $this->warnings[] = 'There are no subscriptions in the system. Cannot match Stripe customer to Charge Subscription.';
            return false;
        }

        // Now handle any subscriptions for this customer.
        $sub_member = ee()->charge_subscription_member->get_one($customer_id, 'customer_id');
        if (empty($sub_member)) {
            $this->errors[] = 'No matching customer subscription for customer id ('.$customer_id.')';
            return false;
        }

        $subscription = ee()->charge_subscription->get_one($sub_member['subscription_id']);
        if (empty($subscription)) {
            $this->errors[] = 'No matching parent subscription for this subscription member ('.$customer_id.')';
            return false;
        }

        // Based on the action state we might be removing a member subscription
        // or just sending a thank you note
        if ($action_state == 'start') {
            if ($type == 'invoice.payment_succeeded') {
                $action_type = 'recurring';
            } else {
                $action_type = 'welcome';
            }

            // Thank you note
            return $this->_handle_success($customer_id, $sub_member, $subscription, $action_type, $charge);
        } elseif ($action_state == 'change') {
            // Thank you note
            return $this->_handle_change($customer_id, $sub_member, $subscription, $charge);
        } elseif ($action_state == 'end') {
            // Remove the member
            return $this->_handle_failure($customer_id, $sub_member, $subscription, $charge);
        }

        $this->errors[] = 'Failed to handle webhook';
        return false;
    }

    public function extract_customer_id($body = array())
    {
        if (!isset($body['data']['object']['customer'])) {
            return false;
        }

        return $body['data']['object']['customer'];
    }


    public function extract_invoice_value($body = array())
    {
        if (!isset($body['data']['object']['total'])) {
            return false;
        }

        return $body['data']['object']['total'];
    }

    public function reset_webhook_key()
    {
        $this->_set_key();

        return true;
    }

    private function _handle_failure($customer_id, $sub_member, $subscription, $charge)
    {
        // Move the member to the failure group
        ee()->charge_subscription->remove_member($sub_member['subscription_member_id']);

        // Now deal with emails
        $this->_email('failure', $sub_member, $subscription, $charge['hash']);

        return true;
    }

    private function _handle_success($customer_id, $sub_member, $subscription, $type = 'welcome', $charge)
    {
        // Good
        // Touch the record
        $data['last_contact'] = time();
        ee()->charge_subscription_member->update($sub_member['subscription_member_id'], $data);

        // Now deal with success conditions
        $this->_email($type, $sub_member, $subscription, $charge['hash']);

        return true;
    }


    private function _handle_change($customer_id, $sub_member, $subscription, $charge)
    {
        // Good
        // Touch the record
        $data['last_contact'] = time();
        ee()->charge_subscription_member->update($sub_member['subscription_member_id'], $data);

        // Now deal with emails
        $this->_email('change', $sub_member, $subscription, $charge['hash']);

        return true;
    }

    private function _email($type, $sub_member, $subscription, $hash = '')
    {
        if (isset($subscription['settings']['email_member_' . $type . '_enabled']) and $subscription['settings']['email_member_' . $type . '_enabled'] == 'yes') {
            // Fire email

            $subject = $subscription['settings']['email_member_' . $type . '_subject'];
            $template = $subscription['settings']['email_member_' . $type . '_template'];

            $bcc = '';
            if (!empty($subscription['settings']['email_member_' . $type . '_bcc'])) {
                $bcc = $subscription['settings']['email_member_' . $type . '_bcc'];
            }

            // $bcc = array();
            // foreach (explode(',', $subscription['settings']['email_member_' . $type . '_bcc']) as $address) {
            //     $bcc[] = trim($address);
            // }

            $member = ee()->db->select('email')->where('member_id', $sub_member['member_id'])->from('members')->get()->row_array();
            if (empty($member)) {
                return;
            }

            $to = array($member['email']);

            $email = ee()->charge_email->send($hash, array($to), $subject, $subscription['settings']['email_member_' . $type . '_template'], $bcc);
        }
    }

    private function _set_key()
    {
        // Also generate a unique 'charge_webhook_key' on install
        $rand_key = ee()->functions->random('alnum', 32);

        $this->flux->setConfig('charge_webhook_key', $rand_key);

        return true;
    }


    /*
     * Check Bad Charge State
     *
     * This is a weird edge case where the original charge record hasn't been properly recorded
     * but we have a valid membersubscription detail
     * In this case, we'll dummy out a charge record
     */
    private function _check_bad_charge_state($customer_id)
    {
        $ret = array();
        $sub_member = ee()->charge_subscription_member->get_one($customer_id, 'customer_id');

        if (!empty($sub_member)) {
            // Lets create a stub charge payment
            $charge = array();
            $charge['customer_id'] = $customer_id;
            $charge['member_id'] = $sub_member['member_id'];
            $charge['source_url'] = 'DUMMY';

            $ret = ee()->charge_stripe->recordDummy($charge);
        }

        return $ret;
    }
} // End class

/* End of file Charge_webhook_model.php */
