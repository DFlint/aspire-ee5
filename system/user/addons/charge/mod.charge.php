<?php

// This is what we have to do for EE2 support
require_once 'addon.setup.php';

use EEHarbor\Charge\FluxCapacitor\FluxCapacitor;
use EEHarbor\Charge\FluxCapacitor\Base\Mod;

/**
 * Charge Module Class
 *
 * @package         charge_ee_addon
 * @author          Tom Jaeger <Tom@EEHarbor.com>
 * @link            https://eeharbor.com/charge
 * @copyright       Copyright (c) 2016, Tom Jaeger/EEHarbor
 */
class Charge extends Mod
{
    public $return_data;

    private $card_years_show = 10;
    private $recurring_plan = false;
    public $errors;
    public $data;
    private $coupon_data;
    protected $settings;

    public $hidden;
    private $default_currency = 'usd';
    private $protected_params = array('on_success',
        'on_success:*',
        'required',
        'success_action',
        'create_member',
        'current_plan',
        'plan_amount',
        'plan_interval',
        'plan_interval_count',
        'plan_currency',
        'plan_trial_days',
        'plan_balance',
        'plan_length',
        'plan_length_interval',
        'plan_set_name',
        'subscription_trial_period_days',
        'subscription_trial_end',
        'use_chargecart',
        'use_entry_fields',
        'entry_id',
        'plan_coupon');

    private $protected_input_name = 'P';
    private $protected = array();
    private $rules = array(
        'customer' => array(
            'name'  => 'required|alphanumericpunctuation',
            'email' => 'required|email'
        ),
        'plan' => array(
            'amount'            => 'required|numeric',
            'interval'          => 'in:week,month,year',
            'interval_count'    => 'integer',
            'currency'          => 'min:3|max:3',
            'length'            => 'integer',
            'length_interval'   => 'in:week,month,year',
            'coupon'            => ''
        ),
        'card' => array(
            'token'             => 'required',
            'last4'             => '',
            'type'              => '',
            'name'              => '',
            'address_line1'     => '',
            'address_line2'     => '',
            'address_city'      => '',
            'address_state'     => '',
            'address_zip'       => '',
            'address_country'   => '',
            'save'              => '',
            'use_saved'         => '',
            'saved_id'          => ''
        ),
        'member' => array(
            'create'            => '',
            'password'          => 'password',
            'password_confirm'  => 'password|repeat:password',
            'username'          => '',
            'screen_name'       => ''
        ),
        'subscription' => array(
            'trial_period_days' => 'integer',
            'trial_end'         => ''
        )
    );

    /**
     * Constructor: sets EE instance
     *
     * @access      public
     * @return      null
     */
    public function __construct()
    {
        parent::__construct();

        $this->settings = $this->flux->getSettings();

        // Define the package path
        ee()->load->add_package_path(PATH_THIRD . 'charge');

        // Load our helper
        ee()->load->helper('Charge');
        ee()->lang->loadfile('charge');

        // Load base model
        if (!class_exists('Charge_model')) {
            ee()->load->library('Charge_model');
        }
        if (!isset(ee()->charge_stripe)) {
            ee()->charge_model->load_models($this->flux);
        }
    }

    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    //  Public Tag Methods
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------


    public function form($action = 'act_charge', $data = array())
    {
        $hidden = array();
        $data['errors'] = array();

        $data['card_months'] = $this->_add_card_months();
        $data['card_years'] = $this->_add_card_years();

        //  First things first, set up our raw empty tags

        $tagdata = ee()->TMPL->tagdata;

        $marker = 'meta:([^' . preg_quote('}') . '""]+)';
        $matches = ee()->charge_model->match_single($tagdata, $marker);

        $meta_keys = array();
        if ($matches !== false) {
            foreach ($matches[1] as $match) {
                $data['meta:' . $match] = '';
                $meta_keys[] = $match;
            }
        }

        // Get any meta data we may have in our flashdata
        // Get it now, so we'll have it ready for picking out validation errors
        foreach ($meta_keys as $key) {
            if (ee()->session->flashdata('meta:' . $key) != '') {
                $data['meta:' . $key] = ee()->session->flashdata('meta:' . $key);
            }

            // We could have had extra custom validation added to meta: values
            // use our known meta keys to check if we have any persisted errors
            $data['error_meta:' . $key] = false;
            if (ee()->session->flashdata('error_meta:' . $key) != '') {
                $data['errors'][] = array('error_message' => ucwords(str_replace('_', ' ', $key)) . ' - ' . lang(ee()->session->flashdata('error_meta:' . $key)));
                $data['error_meta:' . $key] = true;
                $data['error_meta:' . $key . '_message'] = lang(ee()->session->flashdata('error_meta:' . $key));
            }
        }

        // There is a bug in CI on nginx servers
        // were repeated called to set_flashdata will throw a 502 error
        // We'll have to bundle these up into a single call instead
        $newflash_data = array();

        foreach ($this->rules as $set => $set_data) {
            foreach ($set_data as $key => $val) {
                $data[$set . '_' . $key] = ee()->session->flashdata($set . '_' . $key);
                //ee()->session->set_flashdata($set.'_'.$key, $data[$set.'_'.$key]); // I'd prefer to use keep_flashdata, but EE's session class has wisely removed that option

                if (trim($data[$set . '_' . $key]) != '') {
                    $newflash_data[$set . '_' . $key] = $data[$set . '_' . $key];
                }

                $data['error_' . $set . '_' . $key] = false;

                if (ee()->session->flashdata('error_' . $set . '_' . $key) != '') {
                    $data['errors'][] = array('error_message' => ucwords(str_replace('_', ' ', $key)) . ' - ' . lang(ee()->session->flashdata('error_' . $set . '_' . $key)));
                    $data['error_' . $set . '_' . $key] = true;
                    $data['error_' . $set . '_' . $key . '_message'] = lang(ee()->session->flashdata('error_' . $set . '_' . $key));
                }
            }
        }

        if (!empty($newflash_data)) {
            ee()->session->set_flashdata($newflash_data);
        }

        $data['have_card_token'] = false;
        $data['have_errors'] = false;

        if (ee()->session->flashdata('card_token') != '') {
            $data['have_card_token'] = true;
            $hidden['card_token'] = ee()->session->flashdata('card_token');
            $hidden['card_last4'] = ee()->session->flashdata('card_last4');
            $hidden['card_type'] = ee()->session->flashdata('card_type');
        }

        if (count($data['errors']) > 0) {
            $data['have_errors'] = true;
        }

        $data['error_count'] = count($data['errors']);

        // Do we have a plan amount?
        // This will need to be converted back to pounds
        if (!empty($data['plan_amount']) && is_numeric($data['plan_amount'])) {
            $data['plan_amount'] = $data['plan_amount'] / 100;
        }

        // Set up the dotted version of the card
        $data['card_number_dotted'] = ee()->charge_stripe->make_dotted_card($data['card_last4']);

        // Do we have any special values on the form?
        $protected = array();
        foreach ($this->protected_params as $param) {
            if (strpos($param, ':*') > 0) {
                $matcher = explode(':', $param);

                $matcher = $matcher[0] . ':';

                if (!empty(ee()->TMPL->tagparams)) {
                    foreach (ee()->TMPL->tagparams as $key => $val) {
                        if (strpos($key, $matcher) === 0) {
                            $protected[$key] = $val;
                        }
                    }
                }
            }

            if (ee()->TMPL->fetch_param($param) != '') {
                $protected[$param] = ee()->TMPL->fetch_param($param);
            }
        }

        if (!empty($protected)) {
            // We have to encrypt these
            ee()->load->library('encrypt');
            $hidden[$this->protected_input_name] = ee()->encrypt->encode(base64_encode(serialize($protected)), ee()->config->item('encryption_key'));
        }

        $data['have_customer'] = false;
        $data['have_payment_card'] = false;

        $customer = ee()->charge_stripe->get_customer();
        if ($customer != false) {
            $data['have_customer'] = true;

            foreach ($customer->__toArray() as $key => $val) {
                $data['customer_' . $key] = $val;
            }

            $card = ee()->charge_stripe->get_card_for_customer($customer);

            $data['have_payment_card'] = false;
            if ($card != false) {
                $data['have_payment_card'] = true;

                foreach ($card as $key => $val) {
                    if (!is_object($val)) {
                        $data['saved_card_' . $key] = $val;
                    }
                }

                // For backwards compatibility, save the card brand as 'type'.
                if (isset($data['saved_card_brand'])) {
                    $data['saved_card_type'] = $data['saved_card_brand'];
                }

                // Set up the dotted version of the card
                $data['saved_card_number_dotted'] = ee()->charge_stripe->make_dotted_card($card['last4']);

                // Add the card id as a hidden input on the form for our js to pull
                $hidden['card_saved_id'] = $data['saved_card_id'];
            }
        }

        $tag_data = ee()->TMPL->tagdata;

        if (ee()->TMPL->fetch_param('debug') == 'yes' || ee()->TMPL->fetch_param('debug') == 'true') {
            $debug_messages = array();
            $debug_errors = array();
            $protected_debug = $protected;

            $protected_debug['debug'] = 'yes';

            // Make sure the user has set their test/live keys.
            $debug_messages[] = '<strong>'.lang('charge_label_stripe_account_mode').':</strong> '.$this->settings->charge_stripe_account_mode;

            if (($this->settings->charge_stripe_account_mode == 'live' && empty($this->settings->charge_stripe_live_credentials_pk)) || ($this->settings->charge_stripe_account_mode == 'test' && empty($this->settings->charge_stripe_test_credentials_pk))) {
                $debug_errors[] = lang('charge_error_missing_keys');
            }

            if (isset($protected['use_entry_fields']) && $protected['use_entry_fields'] == 'yes') {
                if (!isset($protected['entry_id'])) {
                    $debug_errors[] = lang('charge_error_entry_id');
                } else {
                    $charge_type = 'charge';
                    $entry_id = $protected['entry_id'];

                    // If we're using the entry field data, the charge form fields contain the name of the db field to use.
                    $entry_result = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id));
                    foreach ($entry_result->result() as $entry_data) {
                        // Skip any plan amount group values, these get used later to override the plan_amount.
                        if (substr($entry_data->field_name, 0, 18) == 'plan_amount_group_') {
                            continue;
                        }

                        // If this field is the charge type, save it to use later but don't store it in the protected var.
                        if ($entry_data->field_name == 'charge_type') {
                            $charge_type = $entry_data->field_data;
                            continue;
                        }

                        // If this field is the plan amount type, save it to use later but don't store it in the protected var.
                        if ($entry_data->field_name == 'plan_amount_type') {
                            $plan_amount_type = $entry_data->field_data;
                            continue;
                        }

                        $protected[$entry_data->field_name] = $entry_data->field_data;
                        $protected_debug[$entry_data->field_name] = $entry_data->field_data;
                    }

                    // Depending on the Pricing Type, make sure we have valid data entered into the channel entry.
                    if (isset($plan_amount_type) && $plan_amount_type == 'member_group') {
                        $field_name = 'plan_amount_group_'.ee()->session->userdata('group_id');
                        $field_row = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name))->row();
                        if (!empty($field_row)) {
                            $debug_messages[] = '<strong>Pricing Type:</strong> Per Member Group';
                            $debug_messages[] = '<strong>Member Group:</strong> '.ee()->session->userdata('group_title').' ('.ee()->session->userdata('group_id').')';
                            $protected['plan_amount'] = $field_row->field_data;
                            $protected_debug['plan_amount'] = $field_row->field_data;
                        } else {
                            $debug_errors[] = lang('charge_error_missing_group_price');
                        }
                    } else {
                        if (!isset($protected_debug['plan_amount']) || empty($protected_debug['plan_amount'])) {
                            $debug_errors[] = lang('charge_error_missing_amount');
                        }
                    }

                    // Depending on what type of charge this is, make sure we have valid data entered into the channel entry.
                    if ($charge_type == 'recurring') {
                        if (isset($protected_debug['plan_interval_count']) && $protected_debug['plan_interval_count'] > 0) {
                            if (!isset($protected_debug['plan_interval']) || empty($protected_debug['plan_interval'])) {
                                $protected['plan_interval'] = 'month';
                                $protected_debug['plan_interval'] = 'month';
                            }
                        }
                    }

                    // If we're using the entry field data, the charge form fields contain the name of the db field to use.
                    // $entry_result = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id));
                    // foreach($entry_result->result() as $entry_data) {
                    //     $protected_debug[$entry_data->field_name] = $entry_data->field_data;
                    // }
                }
            }

            $debug_message = '';
            if (count($debug_messages) > 0) {
                $debug_message = implode("<br />\n", $debug_messages).'<br /><br />';
            }

            $debug_error = '';
            if (count($debug_errors) > 0) {
                $debug_error = '<div class="demo-error"><strong>Tag Errors:</strong><br /><ul><li>'.implode('</li><li>', $debug_errors).'</li></ul></div>';
            }

            $tag_data = '<hr><h3>'.lang('charge_tag_debug_title').'</h3><em class="instruction">'.lang('charge_tag_debug_text').'</em><br />'.$debug_message.$debug_error.'<pre><code>'.var_export($protected_debug, true).'</code></pre><hr><br />'.$tag_data;
        }

        $t = $this->_wrap_form($action, $tag_data, $data, $hidden);

        return $t;
    }


    public function update_plan_form()
    {
        $hidden = array();
        $data['errors'] = array();
        $data['have_errors'] = false;

        $customer_id = ee()->charge_stripe->find_customer_id();
        if ($customer_id == false) {
            return ee()->TMPL->no_results;
        }

        // Get the current payment card, payment plan, subscription..

        // Get their current cards
        // Now pull their card info direct from the api
        $cards = ee()->charge_stripe->cards($customer_id);

        $data['card'] = array();

        if (!empty($cards) && is_array($cards)) {
            $data['card'][] = $cards[0]; // Just in case we have multiple. Let's not deal with that
        } else {
            if ($customer_id == false) {
                return ee()->TMPL->no_results;
            }
        }

        foreach ($this->protected_params as $param) {
            if (strpos($param, ':*') > 0) {
                $matcher = explode(':', $param);

                $matcher = $matcher[0] . ':';

                if (!empty(ee()->TMPL->tagparams)) {
                    foreach (ee()->TMPL->tagparams as $key => $val) {
                        if (strpos($key, $matcher) === 0) {
                            $protected[$key] = $val;
                        }
                    }
                }
            }

            if (ee()->TMPL->fetch_param($param) != '') {
                $protected[$param] = ee()->TMPL->fetch_param($param);
            } else {
                // Verify we have our current_plan paramater if we're updating an existing plan.
                if ($param == 'current_plan') {
                    $data['errors'][] = array('error_message' => lang('charge_error_current_plan'));
                }
            }
        }

        if (!empty($protected)) {
            // We have to encrypt these
            ee()->load->library('encrypt');
            $hidden[$this->protected_input_name] = ee()->encrypt->encode(base64_encode(serialize($protected)), ee()->config->item('encryption_key'));
        }

        $tag_data = ee()->TMPL->tagdata;

        if (ee()->TMPL->fetch_param('debug') == 'yes' || ee()->TMPL->fetch_param('debug') == 'true') {
            $debug_messages = array();
            $debug_errors = array();
            $protected_debug = $protected;

            $protected_debug['debug'] = 'yes';

            // Make sure the user has set their test/live keys.
            $debug_messages[] = '<strong>'.lang('charge_label_stripe_account_mode').':</strong> '.$this->settings->charge_stripe_account_mode;

            if (($this->settings->charge_stripe_account_mode == 'live' && empty($this->settings->charge_stripe_live_credentials_pk)) || ($this->settings->charge_stripe_account_mode == 'test' && empty($this->settings->charge_stripe_test_credentials_pk))) {
                $debug_errors[] = lang('charge_error_missing_keys');
            }

            if (isset($protected['use_entry_fields']) && $protected['use_entry_fields'] == 'yes') {
                if (!isset($protected['entry_id'])) {
                    $debug_errors[] = lang('charge_error_entry_id');
                } else {
                    $charge_type = 'charge';
                    $entry_id = $protected['entry_id'];

                    // If we're using the entry field data, the charge form fields contain the name of the db field to use.
                    $entry_result = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id));
                    foreach ($entry_result->result() as $entry_data) {
                        // Skip any plan amount group values, these get used later to override the plan_amount.
                        if (substr($entry_data->field_name, 0, 18) == 'plan_amount_group_') {
                            continue;
                        }

                        // If this field is the charge type, save it to use later but don't store it in the protected var.
                        if ($entry_data->field_name == 'charge_type') {
                            $charge_type = $entry_data->field_data;
                            continue;
                        }

                        // If this field is the plan amount type, save it to use later but don't store it in the protected var.
                        if ($entry_data->field_name == 'plan_amount_type') {
                            $plan_amount_type = $entry_data->field_data;
                            continue;
                        }

                        $protected[$entry_data->field_name] = $entry_data->field_data;
                        $protected_debug[$entry_data->field_name] = $entry_data->field_data;
                    }

                    // Depending on the Pricing Type, make sure we have valid data entered into the channel entry.
                    if (isset($plan_amount_type) && $plan_amount_type == 'member_group') {
                        $field_name = 'plan_amount_group_'.ee()->session->userdata('group_id');
                        $field_row = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name))->row();
                        if (!empty($field_row)) {
                            $debug_messages[] = '<strong>Pricing Type:</strong> Per Member Group';
                            $debug_messages[] = '<strong>Member Group:</strong> '.ee()->session->userdata('group_title').' ('.ee()->session->userdata('group_id').')';
                            $protected['plan_amount'] = $field_row->field_data;
                            $protected_debug['plan_amount'] = $field_row->field_data;
                        } else {
                            $debug_errors[] = lang('charge_error_missing_group_price');
                        }
                    } else {
                        if (!isset($protected_debug['plan_amount']) || empty($protected_debug['plan_amount'])) {
                            $debug_errors[] = lang('charge_error_missing_amount');
                        }
                    }

                    // Depending on what type of charge this is, make sure we have valid data entered into the channel entry.
                    if ($charge_type == 'recurring') {
                        if (isset($protected_debug['plan_interval_count']) && $protected_debug['plan_interval_count'] > 0) {
                            if (!isset($protected_debug['plan_interval']) || empty($protected_debug['plan_interval'])) {
                                $protected['plan_interval'] = 'month';
                                $protected_debug['plan_interval'] = 'month';
                            }
                        }
                    }



                    // If we're using the entry field data, the charge form fields contain the name of the db field to use.
                    // $entry_result = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id));
                    // foreach($entry_result->result() as $entry_data) {
                    //     $protected_debug[$entry_data->field_name] = $entry_data->field_data;
                    // }
                }
            }

            $debug_message = '';
            if (count($debug_messages) > 0) {
                $debug_message = implode("<br />\n", $debug_messages).'<br /><br />';
            }

            $debug_error = '';
            if (count($debug_errors) > 0) {
                $debug_error = '<div class="demo-error"><strong>Tag Errors:</strong><br /><ul><li>'.implode('</li><li>', $debug_errors).'</li></ul></div>';
            }

            $tag_data = '<hr><h3>'.lang('charge_tag_debug_title').'</h3><em class="instruction">'.lang('charge_tag_debug_text').'</em><br />'.$debug_message.$debug_error.'<pre><code>'.var_export($protected_debug, true).'</code></pre><hr><br />'.$tag_data;
        }

        if (count($data['errors']) > 0) {
            $data['have_errors'] = true;
        }
        $data['error_count'] = count($data['errors']);

        // return $this->form('act_update_plan', $data);
        $t = $this->_wrap_form('act_update_plan', $tag_data, $data, $hidden);
        return $t;
    }


    public function update_card_form()
    {
        return $this->form('act_update_card');
    }

    public function customer()
    {
        $member_id = ee()->session->userdata('member_id');
        if (ee()->TMPL->fetch_param('member_id') != '') {
            $member_id = ee()->TMPL->fetch_param('member_id');
        }

        if ($member_id == '' or $member_id == '0') {
            return ee()->TMPL->no_results;
        }

        // Get the member purchases
        ee()->db->where('member_id', $member_id);
        $charges = ee()->charge_stripe->get_all();
        $data['charges'] = $charges;

        // Set how many charges we have. Also set it as a custom named variable so it doesn't get stomped over.
        $data['count'] = count($charges);
        $data['charge_total_results'] = $data['count'];

        $data['charges_total'] = 0;
        $data['charges_total_currency'] = 0;
        $data['timestamp_first_charge'] = 0;

        if ($data['count'] > 0 && is_array($data['charges'])) {
            $i = 0;
            foreach ($data['charges'] as $key => $charge) {
                if (!$data['timestamp_first_charge']) {
                    $data['timestamp_first_charge'] = $charge['timestamp'];
                }

                // Format some of the variables for easier template usage.
                $data['charges'][$key]['plan_full_amount_formatted'] = number_format($charge['plan_full_amount'] / 100);
                $data['charges'][$key]['plan_full_amount_currency_formatted'] = ee()->charge_stripe->get_currency_symbol($charge['plan_currency']) . number_format($charge['plan_full_amount'] / 100);
                $data['charges'][$key]['plan_discount_formatted'] = number_format($charge['plan_discount'] / 100);
                $data['charges'][$key]['plan_discount_currency_formatted'] = ee()->charge_stripe->get_currency_symbol($charge['plan_currency']) . number_format($charge['plan_discount'] / 100);

                $data['charges_total_currency'] = $charge['plan_currency'];
                $data['charges_total'] += $charge['plan_amount'];

                $i++;
                $data['charges'][$key]['charge_count'] = $i;
            }
        }

        $data['charges_total_currency_formatted_rounded'] = ee()->charge_stripe->get_currency_symbol($data['charges_total_currency']) . number_format($data['charges_total'] / 100);
        $data['charges_total_currency_formatted'] = ee()->charge_stripe->get_currency_symbol($data['charges_total_currency']) . number_format($data['charges_total'] / 100, 2);
        $data['charges_total_formatted'] = number_format($data['charges_total'] / 100, 2);
        $data['charges_total'] = round($data['charges_total'] / 100, 2);

        $data['has_active_subscriptions'] = false;
        $data['has_inactive_subscriptions'] = false;

        // Get any subscriptions they may have
        $subscriptions = ee()->charge_subscription_member->get_all_by_member_id($member_id);

        foreach ($subscriptions as $subscription) {
            // Find out if the user has any ACTIVE or INACTIVE subscriptions.
            // A user can only have 1 active subscription but many inactive subscriptions.
            if ($subscription['status'] == 'active') {
                $data['has_active_subscriptions'] = true;
            }
            if ($subscription['status'] == 'inactive') {
                $data['has_inactive_subscriptions'] = true;
            }

            // If they have both active and inactive, we don't need to keep checking so stop the loop.
            if ($data['has_active_subscriptions'] && $data['has_inactive_subscriptions']) {
                break;
            }
        }

        $data['subscriptions'] = $subscriptions;
        $data['subscriptions_count'] = count($subscriptions);

        $data['subscription_name'] = '';
        if (isset($data['subscriptions'][0]['subscription_name'])) {
            $data['subscription_name'] = $data['subscriptions'][0]['subscription_name'];
        }
        if (isset($data['subscriptions'][0]['subscription_id'])) {
            $data['subscription_id'] = $data['subscriptions'][0]['subscription_id'];
        }

        // Get any recurring payments they may have
        $recurring = ee()->charge_stripe->get_recurring($member_id);
        if (count($recurring) > 0) {
            $data['has_active_recurring'] = true;
        } else {
            $data['has_active_recurring'] = false;
        }

        $data['recurring'] = $recurring;
        $data['count_recurring'] = count($recurring);

        // Get any recurring payments they may have
        $inactive = ee()->charge_stripe->get_recurring_inactive($member_id);
        if (count($inactive) > 0) {
            $data['has_inactive_recurring'] = true;
        } else {
            $data['has_inactive_recurring'] = false;
        }
        $data['inactive_recurring'] = $inactive;
        $data['count_inactive_recurring'] = count($inactive);

        $data['has_active_card'] = true;

        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, array($data), false);
    }

    public function has_active_recurring()
    {
        // Get which field to check from the template params.
        $field = ee()->TMPL->fetch_param('field');

        // Get the value we want to check against.
        $value = ee()->TMPL->fetch_param('value');

        // If no `field` is specified, just check if they have /any/ active recurring plans.
        $anyRecurring = false;
        if (empty($field) || empty($value)) {
            $anyRecurring = true;
            ee()->TMPL->log_item('Charge: Tag param `field` or param `value` missing, setting conditional to "any recurring".');
        }

        // Check to see if we're using a specified member or the logged in member.
        if (ee()->TMPL->fetch_param('member_id') != '') {
            $member_id = ee()->TMPL->fetch_param('member_id');
        } else {
            $member_id = ee()->session->userdata('member_id');
        }

        // If there was no member specified and no logged in member, show no results.
        if ($member_id == '' or $member_id == '0') {
            return ee()->TMPL->no_results;
        }

        // Get the member's purchases.
        ee()->db->where('member_id', $member_id);
        $recurring = ee()->charge_stripe->get_recurring($member_id);

        // Loop through the member's purchases checking the `field` for the `value`.
        foreach ($recurring as $plan) {
            // Check if the state is `active` AND make sure the plan hasn't been ended.
            // "Ended" is determined by having both an `ended_on` timestamp and today being greater than the `current_period_end`.
            if ($plan['state'] == 'active' && ($plan['ended_on'] === 0 || ($plan['ended_on'] > 0 && $plan['current_period_end'] > time()))) {
                if ($anyRecurring === true) {
                    ee()->TMPL->log_item('Charge: Found match for ANY ACTIVE RECURRING.');
                    return true;
                } else {
                    // Check the specified `field` for the `value`
                    if (!empty($plan[$field]) && $plan[$field] == $value) {
                        ee()->TMPL->log_item('Charge: Found match for `'.$field.'` and `'.$value.'`.');
                        return true;
                    }

                    ee()->TMPL->log_item('Charge: No match for `charge_id: '.$plan['id'].'` `'.$plan['stripe_description'].'` for `'.$plan['plan_amount_currency_formatted'].'` `'.$plan['plan_wordy'].'`');
                }
            }
        }

        // If there were no matches above, return false.
        return false;
    }

    public function customer_id()
    {
        $member_id = '';

        if (ee()->TMPL->fetch_param('member_id') != '') {
            $member_id = ee()->TMPL->fetch_param('member_id');
        }

        $customer_id = ee()->charge_stripe->find_customer_id($member_id);
        if ($customer_id == false) {
            return ee()->TMPL->no_results;
        }

        return $customer_id;
    }

    public function dump_debug($data, $level=0)
    {
        $type = !is_string($data) && is_callable($data) ? "Callable" : ucfirst(gettype($data));
        $type_data = null;
        $type_color = null;
        $type_length = null;

        switch ($type) {
            case "String":
                $type_color = "green";
                $type_length = strlen($data);
                $type_data = "\"" . htmlentities($data) . "\""; break;

            case "Double":
            case "Float":
                $type = "Float";
                $type_color = "#0099c5";
                $type_length = strlen($data);
                $type_data = htmlentities($data); break;

            case "Integer":
                $type_color = "red";
                $type_length = strlen($data);
                $type_data = htmlentities($data); break;

            case "Boolean":
                $type_color = "#92008d";
                $type_length = strlen($data);
                $type_data = $data ? "TRUE" : "FALSE"; break;

            case "NULL":
                $type_length = 0; break;

            case "Array":
                $type_length = count($data);
        }

        if (in_array($type, array("Object", "Array"))) {
            $notEmpty = false;

            foreach ($data as $key => $value) {
                if (!$notEmpty) {
                    $notEmpty = true;

                    echo $type . "\n";// . ($type_length !== null ? "(" . $type_length . ")" : "")."\n";

                    for ($i=0; $i <= $level; $i++) {
                        echo "|    ";
                    }

                    echo "\n";
                }

                for ($i=0; $i <= $level; $i++) {
                    echo "|    ";
                }

                echo "[" . $key . "] => ";

                $this->dump_debug($value, $level+1);
            }

            if ($notEmpty) {
                for ($i=0; $i <= $level; $i++) {
                    echo "|    ";
                }
            } else {
                echo $type;// . ($type_length !== null ? "(" . $type_length . ")" : "") . "  ";
            }
        } else {
            echo $type;// . ($type_length !== null ? "(" . $type_length . ")" : "") . "  ";

            if ($type_data != null) {
                //echo $type_data;
            }
        }

        echo "\n";
    }

    public function cards()
    {
        $customer_id = ee()->TMPL->fetch_param('customer_id');
        if ($customer_id == '') {
            $customer_id = ee()->charge_stripe->find_customer_id(ee()->TMPL->fetch_param('member_id'));
        }

        if ($customer_id == '' or $customer_id === false) {
            return ee()->TMPL->no_results;
        }

        // Now pull their card info direct from the api
        $cards = ee()->charge_stripe->cards($customer_id);

        $data['cards_count'] = count($cards);

        $data['cards'] = (!empty($cards) ? $cards : array());


        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, array($data), false);
    }

    public function coupons()
    {
        $type = ee()->TMPL->fetch_param('type');
        if ($type != '') {
            ee()->db->where('type', $type);
        }

        $payment_type = ee()->TMPL->fetch_param('payment_type');
        if ($payment_type != '') {
            ee()->db->where('payment_type', $payment_type);
        }

        $coupons = ee()->db->get('charge_coupon');

        foreach ($coupons->result_array() as $row) {
            $data['coupons'][] = $row;
        }

        $data['coupons_count'] = $coupons->num_rows;

        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, array($data), false);
    }

    public function coupon_validation()
    {
        $coupon = ee()->input->get('coupon');
        $plan_type = ee()->input->get('plan_type');
        $plan_amount = ee()->input->get('plan_amount');

        $hidden = array();
        $data['errors'] = array();
        $data['have_errors'] = false;
        $tag_data = ee()->TMPL->tagdata;

        // Do we have any special values on the form?
        $protected = array();
        foreach ($this->protected_params as $param) {
            if (strpos($param, ':*') > 0) {
                $matcher = explode(':', $param);

                $matcher = $matcher[0] . ':';

                if (!empty(ee()->TMPL->tagparams)) {
                    foreach (ee()->TMPL->tagparams as $key => $val) {
                        if (strpos($key, $matcher) === 0) {
                            $protected[$key] = $val;
                        }
                    }
                }
            }

            if (ee()->TMPL->fetch_param($param) != '') {
                $protected[$param] = ee()->TMPL->fetch_param($param);
            }
        }

        if (!empty($protected)) {
            // We have to encrypt these
            ee()->load->library('encrypt');
            $hidden[$this->protected_input_name] = ee()->encrypt->encode(base64_encode(serialize($protected)), ee()->config->item('encryption_key'));
        }

        $t = $this->_wrap_form('act_validate_coupon', $tag_data, $data, $hidden);
        return $t;
    }

    public function has_coupon()
    {
        $type = ee()->TMPL->fetch_param('type');
        if ($type != '') {
            ee()->db->where('type', $type);
        }

        $payment_type = ee()->TMPL->fetch_param('payment_type');
        if ($payment_type != '') {
            ee()->db->where('payment_type', $payment_type);
        }

        $coupons = ee()->db->get('charge_coupon');

        if ($coupons->num_rows > 0) {
            return ee()->TMPL->tagdata;
        } else {
            return '';
        }
    }

    public function act()
    {
        $type = ee()->TMPL->fetch_param('type');
        $charge_id = ee()->TMPL->fetch_param('charge_id');
        $member_id = ee()->session->userdata('member_id');
        if (ee()->TMPL->fetch_param('member_id') != '') {
            $member_id = ee()->TMPL->fetch_param('member_id');
        }

        // Validate
        $action = ee()->charge_stripe->validate_action($type, $charge_id, $member_id);
        if ($action === false) {
            return ee()->TMPL->no_results;
        }

        $extra = ee()->TMPL->tagparams;
        // remove some parts
        unset($extra['type']);
        unset($extra['charge_id']);
        unset($extra['member_id']);

        // Ok. valid
        // Wrap up into a protected url
        $data['action_url'] = $this->_make_act_url($action, $extra);

        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, array($data), false);
    }

    public function info()
    {
        // We require a hash
        $hash = ee()->TMPL->fetch_param('hash');
        if ($hash == '') {
            return ee()->TMPL->no_results;
        }

        $info = ee()->charge_stripe->fetch($hash);
        if (empty($info)) {
            return ee()->TMPL->no_results;
        }

        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, array($info), false);
    }

    public function public_key()
    {
        $stripe_mode = $this->settings->charge_stripe_account_mode;

        if ($stripe_mode == 'live') {
            return $this->settings->charge_stripe_live_credentials_pk;
        } else {
            return $this->settings->charge_stripe_test_credentials_pk;
        }
    }

    public function error_info()
    {
        $data['has_error'] = false;
        $data['error_message'] = lang('charge_unknown_error');
        $data['error_type'] = 'general';

        // Do we have flash data?
        if (ee()->session->flashdata('charge_stripe_error') == true) {
            $data['has_error'] = true;

            $data['error_message'] = ee()->session->flashdata('charge_stripe_error_message');
            //$data['error_type'] = ee()->session->flashdata('charge_stripe_error_type');
        }

        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, array($data), false);
    }

    public function api()
    {
        $ret = array();

        // Collect our parts
        $call_name = ee()->TMPL->fetch_param('call');
        $call_val = ee()->TMPL->fetch_param('key');
        $call_param_name = ee()->TMPL->fetch_param('param_name');

        $params = array();
        foreach (ee()->TMPL->tagparams as $key => $value) {
            if (strpos($key, 'stripe_') === 0) {
                $key = substr($key, 7);
                $params[$key] = $value;
            }
        }

        // performs a read-only retrieve to the stripe api
        $data = ee()->charge_stripe->api_call($call_name, $call_val, $call_param_name, $params);

        if ($data === false) {
            $ret['has_error'] = true;
            $ret['error_message'] = ee()->charge_stripe->api_error_message;
        } else {
            $ret['data'] = $data;
            $ret['stripe_data'][] = $data;
            $ret['has_error'] = false;
            $ret['error_message'] = '';
        }

        $ret['raw'] = '<pre>' . print_r($data, 1) . '</pre>';

        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, array($ret), false);
    }


    public function js()
    {
        return $this->_js();
    }

    public function stripe_js()
    {
        return $this->_js('stripe');
    }

    public function field()
    {
        $entry_id = ee()->TMPL->fetch_param('entry_id');
        $field_name = ee()->TMPL->fetch_param('field');

        $decimals = ee()->TMPL->fetch_param('decimals');
        $thousands = ee()->TMPL->fetch_param('thousands');

        $field_data = '';

        // If we're getting the plan_amount, find out if this is a single price item or a per member group pricing.
        if ($field_name == 'plan_amount') {
            $field_row = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>'plan_amount_type'))->row();
            if (!empty($field_row) && $field_row->field_data == 'member_group') {
                $field_name = 'plan_amount_group_'.ee()->session->userdata('group_id');
            }
        }

        $field_row = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name))->row();

        if (!empty($field_row)) {
            $field_data = $field_row->field_data;
        }

        if (!empty($decimals) || !empty($thousands)) {
            $decimals = (is_numeric($decimals)) ? $decimals : 2;
            $thousands = ($thousands=="") ? "," : $thousands;
            $thousands = ($thousands=="none") ? "" : $thousands;

            if (empty($field_data)) {
                return number_format(0, $decimals, '.', $thousands);
            } else {
                return number_format($field_data, $decimals, '.', $thousands);
            }
        } else {
            if (empty($field_data)) {
                return '';
            } else {
                return $field_data;
            }
        }
    }

    /**
     * Test tag to assert true/false for the existance of Charge items.
     * @return boolean True/False
     */
    public function test()
    {
        // What are we testing (coupon, action, subscription)?
        $what = ee()->TMPL->fetch_param('what');

        // By what method (id, name, coupon code, payment_type, etc)?
        $by = ee()->TMPL->fetch_param('by');

        // (Optional) Having what value?
        $value = ee()->TMPL->fetch_param('value');

        $exists = ee()->db->get_where($what, array($by=>$value), 1)->num_rows;
        if ($exists) {
            return true;
        } else {
            return false;
        }
    }


    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    //  ACT Method Endpoints
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------


    /**
     * Act Charge
     *
     * The end point for our main action. When posted to this act point
     * we expect to have the stripe token already at this point.
     * This will handle the logic of creating a customer, charge, plan
     * and such
     *    1. Validate the inbound post
     *
     *    2. Choose our path, based on the requested plan
     *            if a one-off, we can fire the charge directly
     *            otherwise we need to check/create a plan
     *            then check/create a customer
     *            then fire the card charge
     *
     *        3. Log some details of whats happened to our audit log
     *
     *        4. Wrap up our response, ajax wrapped as nessecary
     *
     *
     * @access      public
     * @return      array()
     */
    public function act_charge()
    {
        $debug = false;

        // 0. If we have protected params in the data, decrypt and add them to the data
        $this->_handle_protected();

        $item_types = 'charge';

        // Find out if we're using chargecart. If so, we need to know if we're doing a single payment (all one-off
        // charges we can group together) or if we need multi-payments (for when there are more than 1 recurring
        // charges or mixed between recurring or one-off charges).
        if (isset($this->protected['use_chargecart']) && $this->protected['use_chargecart'] == 'yes') {
            if (ee()->addons_model->module_installed('chargecart')) {
                ee()->load->add_package_path(PATH_THIRD . 'chargecart');
                if (!class_exists('Chargecart_model')) {
                    ee()->load->library('Chargecart_model');
                }
                if (!isset(ee()->chargecart_core_model)) {
                    Chargecart_model::load_models();
                }

                $item_types = ee()->chargecart_list_model->get_item_types();
            } else {
                ee()->charge_log->log_error_failed_general('"use_chargecart" flag set but Charge Cart not installed');
            }
        }

        // Generate a unique id to group all the payments into one transaction.
        $chargecart_unique_id = uniqid();

        // Are we processing a multiple item payment (most likely from Charge Cart)?
        // If so, let's spoof the system and act like we're just doing a bunch of individual transactions.
        if ($item_types == 'mixed' || $item_types == 'recurring') {
            // Get rid of the posted protected vars so our handle_protected doesn't try to unserialize the data from post.
            unset($_POST['P']);

            // Loop through the differnet payment types, overriding the "protected" variables from the form and re-running
            // _handle_protected() so we don't have to manually override each data point.
            $grouped_charges = ee()->chargecart_list_model->get_grouped_items();

            $order_total = 0;
            $loopCount = 0;
            foreach ($grouped_charges as $charge_type => $charges) {
                $loopCount++;

                if ($debug) {
                    echo $charge_type, ': ', count($charges), '<br />';
                }

                foreach ($charges as $spoof_data) {
                    if ($debug) {
                        echo '<pre>', var_export($this->protected, true), '</pre>';
                        echo '<hr>';
                        echo '<pre>', var_export($spoof_data, true), '</pre>';
                    }

                    // Overwrite the protected data with our individual payment data.
                    $this->protected = $spoof_data;

                    ee()->charge_log->log_request_start();

                    $this->_handle_protected();

                    // We saved the card to the Stripe Customer so erase the card token to prevent trying to reuse it (and erroring out).
                    if ($loopCount > 1 && isset($this->data['card']['token'])) {
                        if ($debug) {
                            echo '<b>Unsetting Card Token</b><br />';
                        }

                        unset($this->data['card']['token']);
                    }

                    if ($debug) {
                        echo '<h3>Loop ', $loopCount, '</h3>', "\n";
                        echo '<hr><b>Protected:</b><br />';
                        echo '<pre>', var_export($this->protected, true), '</pre>';
                        echo '<hr><b>Data:</b><br />';
                        echo '<pre>', var_export($this->data, true), '</pre>';
                        echo '<hr>';
                    }

                    // 1. Validate - If this isn't our first product, force use of saved card.
                    if ($loopCount > 1) {
                        if ($debug) {
                            echo '<b>Validate Use Saved</b><br />';
                        }

                        $this->_validate_request(false, true); // (Not card only, Use saved card id)
                    } else {
                        if ($debug) {
                            echo '<b>Validate Normal</b><br />';
                        }

                        $this->_validate_request();
                    }

                    $this->_pull_meta();

                    if (ee()->extensions->active_hook('charge_pre_payment') === true && $loopCount == 1) {
                        ee()->extensions->call('charge_pre_payment', $this);

                        if (ee()->extensions->end_script === true) {
                            return;
                        }
                    }

                    // Do we need to register a member?
                    $this->_handle_member();

                    // 1a. If we have error data, do something with it
                    $this->_handle_errors();

                    // 2. Pick a Path
                    $route = $this->_pick_path();

                    $this->data['chargecart_unique_id'] = $chargecart_unique_id;

                    // Create a subscription to the plan or a one-off charge.
                    if ($route == 'recurring') {
                        $return = ee()->charge_stripe->recurring($this->data);
                    } else {
                        $return = ee()->charge_stripe->charge($this->data);
                    }

                    if ($return !== false) {
                        // Make sure the card_saved_id is set.
                        if (!isset($this->data['card']['saved_id']) && isset($return['stripe_card_id'])) {
                            $this->data['card']['saved_id'] = $return['stripe_card_id'];
                        }

                        // Trigger the success actions.
                        $this->_trigger_success($return);

                        $order_total += round($return['plan_amount'] / 100, 2);
                        $return['use_chargecart'] = 1;
                        $return['chargecart_unique_id'] = $chargecart_unique_id;
                    } else {
                        ee()->charge_log->log_error_failed_general($this->data);
                    }
                }
            }

            // If the main form had an on_success action, trigger it here.
            if ($on_success) {
                $this->protected['on_success'] = $on_success;

                if ($success_action) {
                    $this->protected['success_action'] = $success_action;
                }

                $return['order_total'] = $order_total;
                $return['order_total_formatted'] = number_format($order_total);

                // Trigger the success actions.
                $this->_trigger_success($return);
            }

            return $this->_return_simple(true, 'charge_multi_payment', $return);
        } else {
            if (isset($this->protected['use_chargecart']) && $this->protected['use_chargecart'] == 'yes') {
                if (ee()->addons_model->module_installed('chargecart')) {
                    ee()->load->add_package_path(PATH_THIRD . 'chargecart');

                    if (!class_exists('Chargecart_model')) {
                        ee()->load->library('Chargecart_model');
                    }

                    if (!isset(ee()->chargecart_core_model)) {
                        Chargecart_model::load_models();
                    }

                    $this->protected['plan_amount'] = ee()->chargecart_list_model->cart_total('chargecart');
                } else {
                    ee()->charge_log->log_error_failed_general('"use_chargecart" flag set but Charge Cart not installed');
                }
            }

            if (isset($this->protected['use_entry_fields']) && $this->protected['use_entry_fields'] == 'yes') {
                $entry_id = $this->protected['entry_id'];

                // If we're using the entry field data, the charge form fields contain the name of the db field to use.
                $entry_result = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id));
                foreach ($entry_result->result() as $entry_data) {
                    // Skip any plan amount group values, these get used later to override the plan_amount.
                    if (substr($entry_data->field_name, 0, 18) == 'plan_amount_group_') {
                        continue;
                    }

                    // If this field is the plan amount type, save it to use later but don't store it in the protected var.
                    if ($entry_data->field_name == 'plan_amount_type') {
                        $plan_amount_type = $entry_data->field_data;
                        continue;
                    }

                    $this->protected[$entry_data->field_name] = $entry_data->field_data;
                }


                if (isset($plan_amount_type) && $plan_amount_type == 'member_group') {
                    $field_name = 'plan_amount_group_'.ee()->session->userdata('group_id');
                    $field_row = ee()->db->get_where('charge_field_data', array('entry_id'=>$entry_id, 'field_name'=>$field_name))->row();
                    if (!empty($field_row)) {
                        $this->protected['plan_amount'] = $field_row->field_data;
                    }
                }
            }

            ee()->charge_log->log_request_start();

            // 1. Validate
            $this->_validate_request();

            $this->_pull_meta();

            if (ee()->extensions->active_hook('charge_pre_payment') === true) {
                ee()->extensions->call('charge_pre_payment', $this);

                if (ee()->extensions->end_script === true) {
                    return;
                }
            }

            // Do we need to register a member?
            $this->_handle_member();

            // 1a. If we have error data, do something with it
            $this->_handle_errors();

            // 2. Pick a Path
            $route = $this->_pick_path();

            $this->data['chargecart_unique_id'] = $chargecart_unique_id;

            if ($route == 'recurring') {
                // Now create a subscription to the plan and pass if we've already done the stripe verification.
                $return = ee()->charge_stripe->recurring($this->data, ee()->input->post('verified'));

                if ($return !== false) {
                    if (isset($this->protected['use_chargecart']) && $this->protected['use_chargecart'] == 'yes') {
                        $return['use_chargecart'] = 1;
                        $return['chargecart_unique_id'] = $chargecart_unique_id;
                    }

                    $return['order_total'] = round($return['plan_amount'] / 100, 2);
                    $return['order_total_formatted'] = number_format($return['plan_amount'] / 100);

                    $returnState = true;

                    if (!empty($return['requires_action'])) {
                        $returnState = false;
                    }

                    return $this->_return($returnState, 'subscription_success', $return);
                }
            } else {
                // Create a one-off charge
                $return = ee()->charge_stripe->charge($this->data);

                if ($return !== false) {
                    if (isset($this->protected['use_chargecart']) && $this->protected['use_chargecart'] == 'yes') {
                        $return['use_chargecart'] = 1;
                        $return['chargecart_unique_id'] = $chargecart_unique_id;
                    }

                    $return['order_total'] = round($return['plan_amount'] / 100, 2);
                    $return['order_total_formatted'] = number_format($return['plan_amount'] / 100);

                    $returnState = true;

                    if (!empty($return['requires_action'])) {
                        $returnState = false;
                    }

                    return $this->_return($returnState, 'charge_success', $return);
                }
            }

            ee()->charge_log->log_error_failed_general($this->data);
        }

        return $this->_return(false, 'general_error', $this->data);
    }


    public function act_update_plan()
    {
        // Only users who are currently on a plan can update their plan
        // Which means that only valid customers can update
        $customer_id = ee()->charge_stripe->find_customer_id();
        if ($customer_id == false) {
            return $this->_return(false, 'general_error');
        }

        // 0. If we have protected params in the data, decrypt and add them to the data
        $this->_handle_protected();

        // 1. Validate
        // Set our alternative rule set for validation
        $this->rules = array('plan' => $this->rules['plan']);
        $this->_validate_request();

        $this->_pull_meta();

        // 1a. If we have error data, do something with it
        $this->_handle_errors();

        // 2. Pick a Path
        $route = $this->_pick_path();

        if ($route == 'recurring') {
            // Now create a subscription to the plan
            $return = ee()->charge_stripe->recurring($this->data);

            if ($return !== false) {
                return $this->_return(true, 'subscription_update_success', $return);
            }
        } else {
            // Create a (new) one-off charge
            $return = ee()->charge_stripe->charge($this->data);

            if ($return !== false) {
                return $this->_return(true, 'charge_update_success', $return);
            }
        }

        return $this->_return(false, 'general_error', $this->data);
    }

    public function act_update_card()
    {
        // Only valid members with a customer record can update a card
        $customer_id = ee()->charge_stripe->find_customer_id();
        if ($customer_id == false) {
            return $this->_return(false, 'general_error');
        }

        // 1. Validate
        $this->_validate_request(true);

        // 1a. If we have error data, do something with it
        $this->_handle_errors();

        // // Do we want to set this as the default card (default: yes).
        // $this->data['set_as_default'] = get_bool_from_string(ee()->TMPL->fetch_param('set_as_default', true));

        // // Do we want to remove all the other cards on file and replace it with this one?
        // $this->data['remove_other_cards'] = get_bool_from_string(ee()->TMPL->fetch_param('remove_other_cards', true));

        $return = ee()->charge_stripe->update_card($customer_id, $this->data['card']);

        if ($return !== false) {
            return $this->_return(true, 'update_card_success', $return);
        }

        return $this->_return(false, 'general_error', $this->data);
    }

    public function act_webhook()
    {
        // Validate first
        $key = ee()->input->get('key');

        if ($key != $this->settings->charge_webhook_key) {
            ee()->charge_log->log_webhook_bad_trigger(array('key' => $key));
            exit();
        }

        $body = @file_get_contents('php://input');
        $body = Charge_obj_to_array(json_decode($body));

        if (empty($body) or !is_array($body)) {
            ee()->charge_log->log_webhook_empty();
            exit();
        }

        ee()->charge_log->log_webhook_started($body);

        // Ok, pass over to the webhook model to all the work
        $response = ee()->charge_webhook->act($body);

        if (!$response) {
            if (is_array(ee()->charge_webhook->warnings) && count(ee()->charge_webhook->warnings) > 0) {
                ee()->charge_log->log_event('webhook_warning', ee()->charge_webhook->warnings);
            } elseif (is_array(ee()->charge_webhook->errors) && count(ee()->charge_webhook->errors) > 0) {
                ee()->charge_log->log_webhook_handle_failure(ee()->charge_webhook->errors);
            }

            ee()->output->set_status_header(204); // HTTP Status Code: 204 No Content
        } else {
            ee()->output->set_status_header(200);
        } // HTTP Status Code: 200 OK

        exit();
    }

    public function act_user()
    {
        $data = ee()->input->get('A');
        if ($data == '') {
            return $this->_return(false, 'general_error');
        }

        ee()->load->library('encrypt');
        $data = unserialize(base64_decode(ee()->encrypt->decode($data)));
        if (!is_array($data) or empty($data)) {
            return $this->_return(false, 'general_error');
        }

        $extra = ee()->input->get('extra');
        if ($extra != '') {
            $extra = unserialize(base64_decode($extra));
        }
        if (!is_array($extra)) {
            $extra = array();
        }

        // Pass over to the stripe model to actually do something with
        $result = ee()->charge_stripe->user_action($data, $extra);

        return $this->_return($result, 'user_action');
    }

    public function act_validate_coupon()
    {
        $this->_handle_protected();

        $coupon = ee()->input->post('plan_coupon');
        $plan_type = ee()->input->post('plan_type');
        $plan_amount = ee()->input->post('plan_amount');

        $this->data['plan']['coupon'] = $coupon;

        // If the protected plan_amount is set, it will take precedence over the plan_amount form field.
        if (!empty($this->protected['plan_amount'])) {
            $plan_amount = $this->protected['plan_amount'];
        }

        if ($plan_amount == 'cart') {
            if (ee()->addons_model->module_installed('chargecart')) {
                ee()->load->add_package_path(PATH_THIRD . 'chargecart');
                if (!class_exists('Chargecart_model')) {
                    ee()->load->library('Chargecart_model');
                }
                if (!isset(ee()->chargecart_core_model)) {
                    Chargecart_model::load_models();
                }

                $this->data['plan']['amount'] = ee()->chargecart_list_model->cart_total('chargecart');
            } else {
                $this->data['plan']['amount'] = 0;
            }
        } else {
            $this->data['plan']['amount'] = $plan_amount;
        }

        // Make sure our amount is in cents/pennies.
        $this->data['plan']['amount'] = round($this->data['plan']['amount'] * 100);

        if (!$plan_type) {
            $plan_type = 'one-off';
        }

        $this->_validate_coupon($plan_type);

        $return['success'] = true;
        $return['status'] = 'valid';
        $return['errors'] = '';

        if (!empty($this->errors) && is_array($this->errors) && count($this->errors) > 0 && isset($this->errors['plan_coupon'])) {
            $return['status'] = 'invalid';
            $return['errors'] = lang($this->errors['plan_coupon']);
        } else {
            $return['coupon_name'] = $this->data['coupon']['coupon']['name'];
            $return['coupon_type'] = $this->data['coupon']['coupon']['type'];
            $return['coupon_payment_type'] = $this->data['coupon']['coupon']['payment_type'];

            if ($return['coupon_type'] == 'fixed') {
                $return['coupon_value'] = $this->data['coupon']['coupon']['amount_off'];
            } else {
                $return['coupon_value'] = $this->data['coupon']['coupon']['percentage_off'];
            }

            $return['plan_amount'] = $this->data['coupon']['plan']['amount'];
            $return['plan_discount'] = $this->data['coupon']['plan']['discount'];
            $return['plan_full_amount'] = $this->data['coupon']['plan']['full_amount'];
        }

        die(json_encode($return));
    }

    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    //  Private Methods
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------


    /*
    * Handle Member
    *
    * In 1.4 we added the ability to register a member as part of a
    * successful payment. We'll create the member here, before we
    * actually take the payment. This may fail, so we could return errors.
    * Also if they payment fails, we'll need to clean up the member later
    */
    private function _handle_member()
    {
        // Only relevant when we're not already logged in
        if (ee()->session->userdata('member_id') != '0') {
            return;
        }
        if (!(isset($this->protected['create_member']) and $this->protected['create_member'] == 'yes')) {
            return;
        }


        // We also allow an option to let the user decide if they want to have an account created for them
        if (isset($this->data['member']['create']) and $this->data['member']['create'] == 'no') {
            return;
        }

        // if passed, we also need to validate password fields
        if (isset($this->data['member']['password']) and $this->data['member']['password'] != '') {
            if (isset($this->data['member']['password_confirm'])) {
                if ($this->data['member']['password'] != $this->data['member']['password_confirm']) {
                    $this->errors['member_password'] = lang('charge_member_password_must_match');

                    return false;
                }
            } else {
                $this->errors['member_password'] = lang('charge_member_password_confirm');

                return false;
            }
        }

        // Only create the member if there are no errors at this point
        if (!empty($this->errors)) {
            return false;
        }

        // Ok. Create the member.
        // We might have some varying levels of data
        // and we need to validate things too
        $member = ee()->charge_member->create($this->data);
        if ($member === false) {
            // Error state
            $this->errors = array_merge($this->errors, ee()->charge_member->errors);

            return false;
        }

        // Complete, make sure we log them in here too
        return;
    }

    private function _handle_errors()
    {
        if (empty($this->errors) || empty($this->data)) {
            return;
        }

        // On nginx servers, the default buffer size is quite low and repeated calls
        // to set_flashdata will increase and overflow the buffer causing it to throw
        // a 502 error. Bundle flashdata calls into a single call instead.
        $newflash_data = array();

        // There's an error somewhere, we'll need to persist our data for the refresh
        foreach ($this->data as $set => $data) {
            if ($set == 'meta') {
                foreach ($data as $key => $val) {
                    if ($val != '') {
                        $newflash_data[$set . ':' . $key] = $val;
                    }
                }
            } else {
                foreach ($data as $key => $val) {
                    if ($val != '') {
                        $newflash_data[$set . '_' . $key] = $val;
                    }
                }
            }
        }

        foreach ($this->errors as $error_key => $error_val) {
            if ($error_val != '') {
                $newflash_data['error_' . $error_key] = lang($error_val);
            }
        }

        // if (!empty($newflash_data)) {
            // ee()->session->set_flashdata($newflash_data);
        // }

        $this->_return(false, 'validation_fail');
    }

    private function _is_ajax_request()
    {
        if (ee()->input->server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return true;
        }

        return false;
    }
    // END is_ajax_request()


    /*
    * Generates the return url for an ACT link
    * based on params and variables
    */
    private function _make_act_url($data, $extra = array())
    {
        // Wrap up the params into an encoded wrapper
        ee()->load->library('encrypt');
        $str = ee()->encrypt->encode(base64_encode(serialize($data)));

        $url = $this->_get_action_url('act_user');

        $url .= '&A=' . urlencode($str);
        $url .= '&ret=' . $this->_get_ret_url();

        if (!empty($extra)) {
            $url .= '&extra=' . urlencode(base64_encode(serialize($extra)));
        }

        return $url;
    }


    /*
    * Generates the return url for an ACT link
    * based on params and variables
    */
    private function _get_ret_url($params = array(), $name = 'return')
    {
        $ret = ee()->functions->fetch_current_uri();

        // Directly use the tagparams array as we're calling this dynamically
        // later on for toggle and this bypasses a logic hold
        if (isset(ee()->TMPL->tagparams[$name])) {
            $ret = urldecode(ee()->TMPL->tagparams[$name]);
        }

        return $ret;
    }


    private function _return($state, $type, $extra = array())
    {
        if (!is_array($extra)) {
            $extra = Charge_obj_to_array($extra);
        }

        if ($state == false) {
            // We may have errors that should be availble to the next request
            if (isset(ee()->charge_stripe->errors) and !empty(ee()->charge_stripe->errors)) {
                $flashdata = array();
                $flashdata['charge_stripe_error'] = true;

                // We can only really handle one error
                foreach (ee()->charge_stripe->errors as $error_key => $error_val) {
                    $flashdata['charge_stripe_error_message'] = $error_val;//.$error_key] = $error_val;
                }
                ee()->session->set_flashdata($flashdata);
            } elseif (!empty($this->errors)) {
                $flashdata['charge_stripe_error_message'] = '';

                foreach ($this->errors as $key => $value) {
                    $flashdata['charge_stripe_error_message'] .= lang($value) . '<br />';
                }
            }
        }

        if ($state == true && $type != 'update_card_success') {
            if (ee()->extensions->active_hook('charge_post_payment') === true) {
                $hook_data = $extra;

                unset($hook_data['card_token']);

                ee()->extensions->call('charge_post_payment', $type, $hook_data);
                if (ee()->extensions->end_script === true) {
                    return;
                }
            }

            $this->_trigger_success($extra, $type);
        }

        ee()->load->helper('string');

        $return = urldecode(ee()->input->get_post('ret'));

        if ($state === true) {
            if (isset($extra['use_chargecart']) and $extra['use_chargecart'] == 1 and isset($extra['chargecart_unique_id'])) {
                $return .= '/' . $extra['chargecart_unique_id'];
            } elseif (isset($extra['hash'])) {
                $return .= '/' . $extra['hash'];
            }
        } else {
            if ($type == 'validation_fail') {
                $return = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
            } else {
                $return = urldecode(ee()->input->get_post('ret_error'));
            }
        }

        if ($return == '' or $return === false) {
            $return = ee()->functions->fetch_site_index();
        }

        if ($this->_is_ajax_request()) {
            if ($state === true) {
                ee()->output->send_ajax_response(array(
                    'verb'    => $type,
                    'data'    => $extra,
                    'success' => $state,
                    'return' => $return
                ));
            } else {
                ee()->output->send_ajax_response(array(
                    'verb'    => $type,
                    'data'    => $extra,
                    'success' => $state,
                    'errors'  => (!empty($flashdata['charge_stripe_error_message']) ? $flashdata['charge_stripe_error_message'] : '')
                ));
            }
        }

        ee()->functions->redirect($return);
        exit();
    }

    // Duplicate of _return except it doesn't trigger actions.
    private function _return_simple($state, $type, $extra = array())
    {
        if (!is_array($extra)) {
            $extra = Charge_obj_to_array($extra);
        }

        if ($state == false) {
            // We may have errors that should be availble to the next request
            if (isset(ee()->charge_stripe->errors) and !empty(ee()->charge_stripe->errors)) {
                $flashdata = array();
                $flashdata['charge_stripe_error'] = true;


                // We can only really handle one error
                foreach (ee()->charge_stripe->errors as $error_key => $error_val) {
                    $flashdata['charge_stripe_error_message'] = $error_val;//.$error_key] = $error_val;
                }
                ee()->session->set_flashdata($flashdata);
            }
        }

        if ($state == true and $type != 'update_card_success') {
            if (ee()->extensions->active_hook('charge_post_payment') === true) {
                $hook_data = $extra;

                unset($hook_data['card_token']);

                ee()->extensions->call('charge_post_payment', $type, $hook_data);
                if (ee()->extensions->end_script === true) {
                    return;
                }
            }
        }

        ee()->load->helper('string');

        if ($this->_is_ajax_request()) {
            if ($state === true) {
                ee()->output->send_ajax_response(array(
                    'verb'    => $type,
                    'data'    => $extra,
                    'success' => $state
                ));
            } else {
                ee()->output->send_ajax_response(array(
                    'verb'    => $type,
                    'data'    => $extra,
                    'success' => $state
                ));
            }
        }

        $return = urldecode(ee()->input->get_post('ret'));

        if ($state === true and isset($extra['chargecart_unique_id'])) {
            $return .= '/' . $extra['chargecart_unique_id'];
        } elseif ($state === true and isset($extra['hash'])) {
            $return .= '/' . $extra['hash'];
        }

        if ($state === false and $type == 'validation_fail') {
            $return = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
        } elseif ($state === false) {
            $return = urldecode(ee()->input->get_post('ret_error'));
        }

        if ($return == '' or $return === false) {
            $return = ee()->functions->fetch_site_index();
        }

        ee()->functions->redirect($return);
        exit();
    }

    private function _pick_path()
    {
        // Is there a plan_length defined? (and also valid)
        if (isset($this->data['plan']['length']) and is_numeric($this->data['plan']['length'])) {
            // Even if this is a one-time charge, we handle it as a simple recurring payment, but
            // immediately cancel it after the first payment

            // In this case, we need to have a valid interval value also
            if (!isset($this->data['plan']['interval']) or $this->data['plan']['interval'] == '') {
                $this->data['plan']['interval'] = 'month';
            }


            // Add a marker to show this is a spoofed recurring plan
            if (!isset($this->data['plan']['interval_count']) or $this->data['plan']['interval_count'] == '0') {
                $this->data['plan']['spoofed_recurring'] = true;

                // Set the recurring details to match the length
                $this->data['plan']['interval_count'] = $this->data['plan']['length'];
                $this->data['plan']['interval'] = $this->data['plan']['length_interval'];
            }

            return 'recurring';
        }

        // if plan_interval_count isn't set or set to 0, do a one off charge,
        // otherwise, recurring
        if (!isset($this->data['plan']['interval_count']) or $this->data['plan']['interval_count'] == '0') {
            // One off
            return 'charge';
        }

        // In this case, we need to have a valid interval value also
        if (!isset($this->data['plan']['interval']) or $this->data['plan']['interval'] == '') {
            $this->data['plan']['interval'] = 'month';
        }

        return 'recurring';
    }


    private function _handle_protected()
    {
        if (ee()->input->get_post($this->protected_input_name) != '') {
            $protected = ee()->input->get_post($this->protected_input_name);

            ee()->load->library('encrypt');
            $protected = unserialize(base64_decode(ee()->encrypt->decode($protected)));

            if (!empty($protected)) {
                $this->protected = $protected;
            }
        }

        $this->_handle_required_extra();
        $this->_handle_trial_days();
        $this->_handle_account_balance();
        $this->_handle_set_plan_name();
        $this->_handle_current_plan();
        $this->_handle_subscription_trial_end();
    }

    private function _handle_required_extra()
    {
        // If we have a 'required' param in our protected array
        // we let devs specify extra required field inputs
        // beyond the standard
        if (!isset($this->protected['required'])) {
            return;
        }

        $req = explode('|', $this->protected['required']);

        // All rules are subgrouped, by '_', deal with that
        foreach ($req as $req_key) {
            if (strpos($req_key, 'meta:') === 0) {
                // We have a validation on a meta: key
                $req_parts = explode(':', $req_key);
            } else {
                $req_parts = explode('_', $req_key);
            }

            if (count($req_parts) < 2) {
                continue;
            }

            $set = array_shift($req_parts);
            $sub = implode('_', $req_parts);

            if ($set == 'meta') {
                $this->rules['meta'][$sub] = '';
            } elseif (!isset($this->rules[$set][$sub])) {
                continue;
            }

            $current_rule = $this->rules[$set][$sub];
            if ($current_rule == '') {
                $current_rule = 'required';
            } else {
                $current_rule = 'required|' . $current_rule;
            }
            $this->rules[$set][$sub] = $current_rule;
        }

        return;
    }

    private function _handle_set_plan_name()
    {
        if (isset($this->protected['plan_set_name'])) {
            $this->data['plan']['set_name'] = $this->protected['plan_set_name'];
        }
    }

    private function _handle_current_plan()
    {
        if (isset($this->protected['current_plan'])) {
            $this->data['current_plan'] = $this->protected['current_plan'];
        }
    }

    private function _handle_subscription_trial_end()
    {
        // Default to no trail
        $this->data['subscription']['trial_end'] = 0;

        if (isset($this->protected['subscription_trial_end'])) {
            // We need to add extra validation here
            if (!empty($this->protected['subscription_trial_end'])) {
                list($year, $month, $day) = explode('-', $this->protected['subscription_trial_end']);

                if ($year === '*') {
                    $year = 'Y';
                }

                if ($month === '*') {
                    $month = 'm';
                }

                if ($day === 'l') {
                    $day = 't';
                }

                $date = new DateTime();
                $date->setDate(date($year), date($month), date($day));

                $trial_end = $date->format('Ymd');

                if ($trial_end === date('Ymd')) {
                    return;
                } elseif ($trial_end < date('Ymd')) {
                    if ($month === 'm') {
                        $date->modify('+1 month');
                    } elseif ($year === 'Y') {
                        $date->modify('+1 year');
                    }

                    $trial_end = $date->format('Ymd');
                }

                $trial_end = $date->getTimestamp();

                $this->protected['subscription_trial_end'] = $trial_end;
            }
        }
    }

    private function _handle_trial_days()
    {
        // Default to no trail
        $this->data['plan']['trial_days'] = 0;

        if (isset($this->protected['plan_trial_days'])) {
            // We need to add extra validation here
            if (is_numeric($this->protected['plan_trial_days']) and $this->protected['plan_trial_days'] > 0) {
                $this->data['plan']['trial_days'] = ceil($this->protected['plan_trial_days']);
            }
        }
    }


    private function _handle_account_balance()
    {
        if (isset($this->protected['plan_balance'])) {
            // We need to add extra validation here
            if (is_numeric($this->protected['plan_balance']) and $this->protected['plan_balance'] > 0 or $this->protected['plan_balance'] < 0) {
                $this->data['plan']['balance'] = floor($this->protected['plan_balance'] * 100);
            }
        }
    }

    /**
     * Validate Request
     * We need :
     *            a) Stripe Token (via stripe.js)
     *            b) Customer Details (name/email)
     *            c) Charge Details (amount)
     *          We may also have :
     *            d) Plan Details (one off/monthly etc.
     *                if not passed, default to one-off)
     */
    private function _validate_request($card_only = false, $card_use_saved = false)
    {
        if ($card_only === true) {
            foreach ($this->rules as $set => $set_rules) {
                if ($set != 'card') {
                    unset($this->rules[$set]);
                }
            }
        }

        // We might actually have a saved_card_id in place of a stripe card
        // Allow this if the customer is valid
        if ($this->check_bool_true(ee()->input->post('card_use_saved'))) {
            // Ok use a saved card
            // Unset the required on token
            // Set required on saved_id
            $this->rules['card']['token'] = '';
            $this->rules['card']['saved_id'] = 'required';
        }

        foreach ($this->rules as $set_name => $set_rules) {
            foreach ($set_rules as $item_key => $item_rule) {
                $breaker = '_';
                if ($set_name == 'meta') {
                    $breaker = ':';
                }

                $item_val = $this->_validate_item($set_name . $breaker . $item_key, $item_rule);
                //echo $set_name . $breaker . $item_key, ': ', $item_rule, ' || ', $item_val, '<br />';

                // Reset the item value unless it's the card token and we're forcing use of the saved customer card.
                if ($item_val != '' && !($set_name == 'card' && $item_key == 'token' && $card_use_saved)) {
                    $this->data[$set_name][$item_key] = $item_val;
                }
            }
        }

        if ($card_only !== true) {

            // Make sure we have a valid currency value
            if (!isset($this->data['plan']['currency'])) {
                $this->data['plan']['currency'] = $this->settings->charge_stripe_currency;
            } elseif (!in_array($this->data['plan']['currency'], array_keys(ee()->charge_stripe->supported_currencies))) {
                // Throw an error
                $this->errors['plan_currency'] = 'charge_error_invalid_currency';
            }

            if ($this->data['plan']['currency'] == '' or $this->data['plan']['currency'] == false) {
                $this->data['plan']['currency'] = ee()->charge_stripe->default_currency;
            }


            // Make sure our amount is in cents/pennies
            if (!isset($this->data['plan']['amount']) || !is_numeric($this->data['plan']['amount'])) {
                $this->errors['plan_amount'] = lang('charge_error_missing_amount');
            } else {
                $this->data['plan']['amount'] = round($this->data['plan']['amount'] * 100);
            }

            if (isset($this->data['plan']['length']) and is_numeric($this->data['plan']['length'])) {
                $this->_validate_set_length();
            }

            // Validate any supplied coupon is valid
            if (isset($this->data['plan']['coupon']) and $this->data['plan']['coupon'] != '') {
                $this->_validate_coupon();
            }
        }
    }


    /*
    * Quick helper function that wraps tagdata
    * in an opening and closing form tags
    * and also adds some hidden fields while it's at it
    */
    private function _wrap_form($act, $tagdata, $data = array(), $hidden_extra = array())
    {
        $form_name = ee()->TMPL->fetch_param('form_name');
        $form_class = ee()->TMPL->fetch_param('form_class');
        $form_id = ee()->TMPL->fetch_param('form_id');

        $form_name = $form_name != '' ? ' name="' . $form_name . '"' : '';
        $form_class = $form_class != '' ? ' class="' . $form_class . '"' : '';
        $form_id = $form_id != '' ? ' id="' . $form_id . '"' : '';

        $form_method = 'POST';

        $ret = $this->_get_ret_url();
        $ret_error = $this->_get_ret_url(array(), 'return_error');

        // Get the action_id
        $action_url = $this->_get_action_url($act);

        if ($action_url == false) {
            // Looks like the action hasn't been properly configured
            return 'Sorry, this form is currently mis-configured. Please contact the site owner';
        }

        if (APP_VER < '2.7') {
            $hidden[] = '<input type="hidden" name="csrf_token" value="{csrf_token}"/>';
        } else {
            $hidden[] = '<input type="hidden" name="XID" value="{XID_HASH}"/>';
        }

        $hidden[] = '<input type="hidden" name="ret" value="' . urlencode($ret) . '"/>';
        $hidden[] = '<input type="hidden" name="ret_error" value="' . urlencode($ret_error) . '"/>';

        foreach ($hidden_extra as $hidden_key => $hidden_val) {
            $hidden[] = '<input type="hidden" name="' . $hidden_key . '" value="' . $hidden_val . '"/>';
        }

        // data attributes?
        $dataparams = $this->_prep_dataparams();

        $bare = "<form" . $form_name . $form_class . $form_id . " method='" . $form_method . "' action='" . $action_url . "' accept-charset='UTF-8' " . $dataparams . ">";

        $bare .= implode(' ', $hidden);

        $bare .= $tagdata;
        $bare .= "</form>";

        $str = ee()->functions->add_form_security_hash($bare);

        // Parse tagdata
        $t = ee()->TMPL->parse_variables(
            $bare,
            array($data),
            true
        );

        return $t;
    }


    private function _prep_dataparams()
    {
        $params = array();

        if (is_array(ee()->TMPL->tagparams) && count(ee()->TMPL->tagparams) > 0) {
            foreach (ee()->TMPL->tagparams as $key => $val) {
                if (strpos($key, 'data-') === 0) {
                    $params[] = $key . '="' . $val . '"';
                }
            }
        }

        if (empty($params)) {
            return '';
        }

        return implode(' ', $params);
    }


    private function _get_action_url($method_name)
    {
        // Cache the action urls for repeated use
        if (!isset(ee()->session->cache['charge']['action_urls'][$method_name])) {
            $action_id = ee()->db->where(
                array(
                    'class'  => 'Charge',
                    'method' => $method_name
                )
            )->get('actions')->row('action_id');

            if (is_array($action_id)) {
                return false;
            }

            $path = str_replace('??', '?', ee()->functions->fetch_site_index(0, 0) . '?ACT=' . $action_id);

            // On some server setups the https path doesn't naturally flow from the built in url
            if ($this->settings->charge_force_ssl == 'yes') {
                $path = str_replace('http://', 'https://', $path);
            }

            ee()->session->cache['charge']['action_urls'][$method_name] = $this->flux->reduce_double_slashes($path);
        }

        return ee()->session->cache['charge']['action_urls'][$method_name];
    }

    //END get_action_url

    private function _pull_meta()
    {
        $meta = array();

        foreach ($_REQUEST as $key => $val) {
            if (strpos($key, 'meta:') !== false) {
                $key = str_replace('meta:', '', $key);
                $val = $this->flux->xss_clean($val);

                $meta[$key] = $val;
            }
        }

        if (empty($meta)) {
            return;
        }

        $this->data['meta'] = $meta;

        return;
    }

    private function _validate_item($post_name, $rules = '')
    {
        $valid = true;
        $required = false;

        // Set in protected?
        if (isset($this->protected[$post_name])) {
            $val = $this->protected[$post_name];
        } else {
            $val = ee()->input->get_post($post_name);
        }

        $val = trim($val);

        foreach (explode('|', $rules) as $rule) {
            switch ($rule) {
                case 'required':
                    if (empty($val) || $val === 'null') {
                        $valid = false;
                        $required = true;
                        $this->errors[$post_name] = lang('charge_error_required');
                    }
                    break;
                case 'integer':
                    $val = (int)$val;
                    if (!is_int($val)) {
                        $valid = false;
                        $this->errors[$post_name] = lang('charge_error_integer');
                    }
                    break;
                case 'numeric':
                    if (!is_numeric($val)) {
                        $valid = false;
                        $this->errors[$post_name] = lang('charge_error_numeric');
                    }
                    break;
                case 'email':
                    ee()->load->helper('email');
                    if (!valid_email($val)) {
                        $valid = false;
                        $this->errors[$post_name] = lang('charge_error_email');
                    }
                    break;
                case 'alphanumeric':
                    preg_match("/[^a-z\.,\d\s]/iu", $val, $matches);

                    if (!empty($matches)) {
                        $valid = false;
                        $this->errors[$post_name] = lang('charge_error_not_alphanumeric');
                    }
                    break;
                case 'alphanumericpunctuation':
                    preg_match("/[^a-z\.,\d\s\'-]/iu", $val, $matches);

                    if (!empty($matches)) {
                        $valid = false;
                        $this->errors[$post_name] = lang('charge_error_not_alphanumericpunctuation');
                    }
                    break;
                default:
                    $rule = explode(':', $rule);
                    $rule_type = current($rule);
                    next($rule);
                    $next_type = current($rule);

                    switch ($rule_type) {
                        case 'in':
                            if (!in_array($val, explode(',', $next_type))) {
                                $valid = false;
                                $this->errors[$post_name] = lang('charge_error_not_valid_value');
                            }
                            break;
                        case 'min':
                            if (strlen($val) < $next_type) {
                                $valid = false;
                                $this->errors[$post_name] = lang('charge_error_too_short');
                            }
                            break;
                        case 'max':
                            if (strlen($val) > $next_type) {
                                $valid = false;
                                $this->errors[$post_name] = lang('charge_error_too_long');
                            }
                            break;
                    }
                    break;
            }
        }

        // Token gets special handling
        if ($post_name == 'card_token' && !$valid) {
            $this->errors[$post_name] = lang('charge_error_card_token_missing');
        }

        if (!$required and trim($val) == '') {
            unset($this->errors[$post_name]);
        }

        return $val;
    }


    private function _trigger_success($data)
    {
        if (isset($this->protected['on_success']) and $this->protected['on_success'] != '') {
            ee()->charge_log->log_action_trigger_start(array('data' => $data, 'action' => $this->protected['on_success']));

            $actions = explode('|', $this->protected['on_success']);
            foreach ($actions as $action) {
                ee()->charge_action->run($action, $data, $this->protected);
            }
        }

        if (isset($this->protected['success_action']) and $this->protected['success_action'] != '') {
            // Woot. We have something to do.
            ee()->charge_log->log_action_trigger_start(array('data' => $data, 'action' => $this->protected['success_action']));

            // Throw this over to our action model
            ee()->charge_action->trigger($this->protected['success_action'], $data);
        }
    }

    private function _js($type = '')
    {
        if ($type == 'stripe') {
            return 'https://js.stripe.com/v3/';
        }

        $base = '';

        if (defined('URL_THIRD_THEMES')) {
            $base = URL_THIRD_THEMES;
        } else {
            $base = ee()->config->item('theme_folder_url');

            if (substr($base, -1) != '/') {
                $base .= '/';
            }

            $base .= "third_party/";
        }

        $theme_folder_url = $base . 'charge/';

        $path = $theme_folder_url . "scripts/jquery.charge.js?v=" . md5($this->settings->version);

        if ($this->settings->charge_force_ssl == 'yes') {
            $path = str_replace('http://', 'https://', $path);
        }

        return $path;
    }


    private function _add_card_months()
    {
        $card_months = array();

        $card_months[] = array('month_digit' => '01', 'month_long' => ucwords(ee()->lang->line('january')), 'month_short' => ee()->lang->line('jan'), 'current_month' => (date('m') == '01' ? true : false));
        $card_months[] = array('month_digit' => '02', 'month_long' => ucwords(ee()->lang->line('february')), 'month_short' => ee()->lang->line('feb'), 'current_month' => (date('m') == '02' ? true : false));
        $card_months[] = array('month_digit' => '03', 'month_long' => ucwords(ee()->lang->line('march')), 'month_short' => ee()->lang->line('mar'), 'current_month' => (date('m') == '03' ? true : false));
        $card_months[] = array('month_digit' => '04', 'month_long' => ucwords(ee()->lang->line('april')), 'month_short' => ee()->lang->line('apr'), 'current_month' => (date('m') == '04' ? true : false));
        $card_months[] = array('month_digit' => '05', 'month_long' => ucwords(ee()->lang->line('may')), 'month_short' => ee()->lang->line('may'), 'current_month' => (date('m') == '05' ? true : false));
        $card_months[] = array('month_digit' => '06', 'month_long' => ucwords(ee()->lang->line('june')), 'month_short' => ee()->lang->line('jun'), 'current_month' => (date('m') == '06' ? true : false));
        $card_months[] = array('month_digit' => '07', 'month_long' => ucwords(ee()->lang->line('july')), 'month_short' => ee()->lang->line('jul'), 'current_month' => (date('m') == '07' ? true : false));
        $card_months[] = array('month_digit' => '08', 'month_long' => ucwords(ee()->lang->line('august')), 'month_short' => ee()->lang->line('aug'), 'current_month' => (date('m') == '08' ? true : false));
        $card_months[] = array('month_digit' => '09', 'month_long' => ucwords(ee()->lang->line('september')), 'month_short' => ee()->lang->line('sep'), 'current_month' => (date('m') == '09' ? true : false));
        $card_months[] = array('month_digit' => '10', 'month_long' => ucwords(ee()->lang->line('october')), 'month_short' => ee()->lang->line('oct'), 'current_month' => (date('m') == '10' ? true : false));
        $card_months[] = array('month_digit' => '11', 'month_long' => ucwords(ee()->lang->line('november')), 'month_short' => ee()->lang->line('nov'), 'current_month' => (date('m') == '11' ? true : false));
        $card_months[] = array('month_digit' => '12', 'month_long' => ucwords(ee()->lang->line('december')), 'month_short' => ee()->lang->line('dec'), 'current_month' => (date('m') == '12' ? true : false));

        return $card_months;
    }

    private function _add_card_years()
    {
        $card_years = array();

        $this->card_years_show = (ee()->TMPL->fetch_param('show_card_years_count') != '' ? ee()->TMPL->fetch_param('show_card_years_count') : $this->card_years_show);

        $base_year = date('Y');
        for ($i = 0; $i < $this->card_years_show; $i++) {
            $current_year = $base_year + $i;
            $card_years[] = array('year' => $current_year, 'year_long' => $current_year, 'year_short' => substr($current_year, 2));
        }

        return $card_years;
    }


    private function _validate_set_length()
    {

        // If they've passed a plan_length, we must also validate it's a workable length

        if ($this->data['plan']['length'] < 1) {
            unset($this->data['plan']['length']);
        } // Ignore it
        else {

            // If this is a one-time payment, we've got a hard limit on the max set length we can set.
            // If it's a recurring payment, we can be more flexible.
            $limit = false;
            if ($this->_pick_path() == 'charge') {
                $limit = true;
            }

            $this->data['plan']['length'] = floor($this->data['plan']['length']);
            // We let the dev specify the plan_length_interval multiple ways
            // 1. Directly by plan_length_interval
            // 2. Implied by plan_interval
            // 3. Default to 'month' otherwise
            $length_interval = 'month';

            // These are validated to be 'week', 'month' or 'year' by our global rules
            if (isset($this->data['plan']['length_interval']) and $this->data['plan']['length_interval'] != '') {
                $length_interval = $this->data['plan']['length_interval'];
            } elseif (isset($this->data['plan']['interval']) and $this->data['plan']['interval'] != '') {
                $length_interval = $this->data['plan']['interval'];
            }

            $this->data['plan']['length_interval'] = $length_interval;
            // Now check we're within our limits
            switch ($length_interval) {
                case 'week':
                    if ($this->data['plan']['length'] > 52 && $limit) {
                        $this->errors['plan_length'] = 'charge_error_plan_length_too_long';
                    }
                    break;
                case 'month':
                    if ($this->data['plan']['length'] > 12 && $limit) {
                        $this->errors['plan_length'] = 'charge_error_plan_length_too_long';
                    }
                    break;
                case 'year':
                    if ($this->data['plan']['length'] > 1 && $limit) {
                        $this->errors['plan_length'] = 'charge_error_plan_length_too_long';
                    }
                    break;
                default:
                    $this->errors['plan_length'] = 'charge_error_plan_length_invalid';
                    break;
            }

            // Now figure out our expiry timestamp
            $expiry = strtotime('+' . $this->data['plan']['length'] . ' ' . $this->data['plan']['length_interval']);
            $this->data['plan']['length_expiry'] = $expiry;
        }
    }

    private function _validate_coupon($plan_type = 'one-off')
    {
        if (trim($this->data['plan']['coupon']) == '') {
            unset($this->data['plan']['coupon']);
            return; // Ignore it
        }

        if (isset($this->data['plan']['interval_count']) and $this->data['plan']['interval_count'] > 0) {
            $plan_type = 'recurring';
        }

        // Find the coupon in question (if it exists)
        $coupon = ee()->charge_coupon->get_one($this->data['plan']['coupon'], 'code');

        if (empty($coupon)) {
            // Not a valid code
            $this->errors['plan_coupon'] = 'charge_coupon_error_invalid_code';
            return;
        }

        // Check the code is valid for this payment type
        if ($plan_type == 'recurring' and $coupon['payment_type'] == 'one-off') {
            $this->errors['plan_coupon'] = 'charge_coupon_error_invalid_charge';
            return;
        }

        // Check the code is valid for this payment type
        if ($plan_type == 'one-off' and $coupon['payment_type'] == 'recurring') {
            $this->errors['plan_coupon'] = 'charge_coupon_error_invalid_recurring';
            return;
        }

        // If the coupon has a start date, make sure it's before today.
        if ($coupon['start_date'] && $coupon['start_date'] > 0 && strtotime($coupon['start_date']) > time()) {
            $this->errors['plan_coupon'] = 'charge_coupon_error_invalid';
            return;
        }

        // If the coupon has an end date, make sure it's after today.
        if ($coupon['end_date'] && $coupon['end_date'] > 0 && strtotime($coupon['end_date'].' 23:59:59') < time()) {
            $this->errors['plan_coupon'] = 'charge_coupon_error_expired';
            return;
        }

        // If the coupon is for a specific member, validate against the current user.
        if ($coupon['member_id'] && $coupon['member_id'] != ee()->session->userdata('member_id')) {
            $this->errors['plan_coupon'] = 'charge_coupon_error_invalid_member';
            return;
        }

        // If the coupon is restricted to a specific channel entry, make sure we're purchasing just that item or that item is in our cart.
        if ($coupon['entry_id']) {
            // There are two ways to pass a channel entry, as an individual item or as part of Charge Cart.
            if (isset($this->protected['entry_id']) && !empty($this->protected['entry_id'])) {
                if ($this->protected['entry_id'] != $coupon['entry_id']) {
                    $this->errors['plan_coupon'] = 'charge_coupon_error_invalid_item';
                    return;
                }
            } else {
                // Check if we're using Charge Cart and have the item in our cart.

                // If we don't have Charge Cart installed, then we have a coupon limited to an entry but are not passing any entry_id.
                if (!ee()->addons_model->module_installed('chargecart')) {
                    $this->errors['plan_coupon'] = 'charge_coupon_error_invalid_item';
                    return;
                }

                // If we're here than the coupon is limited to an entry, we haven't sent that entry_id directly and we ARE using Charge Cart.
                ee()->load->add_package_path(PATH_THIRD . 'chargecart');
                if (!class_exists('Chargecart_model')) {
                    ee()->load->library('Chargecart_model');
                }
                if (!isset(ee()->chargecart_core_model)) {
                    Chargecart_model::load_models();
                }

                $is_valid = false;
                $has_other_items = false;

                // Get the list of items in our cart and see if:
                //     A) We have the item in the cart
                //     B) If we have anything else in our cart (not currently allowed)

                $list = ee()->chargecart_item_model->get_list_by_name('chargecart');
                if (is_array($list) && count($list) > 0) {
                    foreach ($list as $list_item) {
                        if (is_array($list_item) && isset($list_item['entry_id'])) {
                            if ($list_item['entry_id'] == $coupon['entry_id']) {
                                $is_valid = true;
                            } else {
                                $has_other_items = true;
                            }
                        }
                    }
                }

                // If there are other items in the cart, we have to throw an error otherwise the coupon would apply to all of them.
                // TODO: Find a way to allow a coupon to apply to 1 item when other items are in the cart.
                if ($has_other_items) {
                    $this->errors['plan_coupon'] = 'charge_coupon_error_limited';
                }

                if (!$is_valid) {
                    $this->errors['plan_coupon'] = 'charge_coupon_error_invalid_item';
                    return;
                }
            }
        }

        if ($plan_type == 'recurring') {
            // Nothing more we do now. Recurring coupons are validated by stripe directly
            $this->data['coupon']['coupon'] = $coupon;
            return;
        }

        // Apply our validation for one-time coupons
        // Only dealing with one-time coupons now.
        $base_amount = $this->data['plan']['amount'];
        //$base_currency = $this->data['plan']['currency'];

        $discount_amount = 0;
        $final_amount = $base_amount;

        if ($coupon['type'] == 'percentage') {
            $percentage_off = $coupon['percentage_off'];

            $discount_amount = (double)$base_amount * ($percentage_off / 100);
            $final_amount = (double)$base_amount - $discount_amount;
        }


        if ($coupon['type'] == 'fixed') {
            $amount_off = $coupon['amount_off'];

            $discount_amount = ceil($amount_off * 100);
            $final_amount = (double)$base_amount - $discount_amount;
        }


        // Sanity Check
        if ($discount_amount <= 0) {
            // nope, discount is zero.
            $this->errors['plan_coupon'] = lang('charge_coupon_error_invalid');

            return;
        }

        if ($final_amount >= $base_amount) {
            // nope, somehow this 'discount' has increased the price
            $this->errors['plan_coupon'] = lang('charge_coupon_error_invalid');

            return;
        }

        // Check we're still above the min transaction price
        if ($final_amount <= 0.5) {
            $this->errors['plan_coupon'] = lang('charge_coupon_error_below_min');

            return;
        }

        $final_amount = floor(strval($final_amount));
        $discount_amount = strval($discount_amount);

        $this->data['coupon']['coupon'] = $coupon;
        $this->data['coupon']['plan']['amount'] = $final_amount;
        $this->data['coupon']['plan']['discount'] = $discount_amount;
        $this->data['coupon']['plan']['full_amount'] = $base_amount;

        return;
    }

    private function check_bool_true($checkval)
    {
        // Because EE devs can be messy

        if ($checkval === true) {
            return true;
        }

        if ($checkval == 'y') {
            return true;
        }

        if ($checkval == 'yes') {
            return true;
        }

        if ($checkval == 'true') {
            return true;
        }

        return false;
    }
}
