<?php

namespace EEHarbor\Charge\Conduit;

use EEHarbor\Charge\FluxCapacitor\Conduit\McpNav as FluxNav;

class McpNav extends FluxNav
{
    protected function defaultItems($items = array())
    {
        $default_items = array(
            '/'             => lang('charge_title'),
            'coupons'       => lang('charge_coupons'),
            'actions'       => lang('charge_actions'),
            'subscriptions' => lang('charge_subscriptions'),
            'logs'          => lang('charge_logs'),
            'settings'      => lang('charge_settings'),
        );

        return array_merge($default_items, $items);
    }

    protected function defaultButtons()
    {
        return array(
            'coupons' => array('view_coupon&new=yes' => 'New'),
            'actions' => array('view_action&new=yes' => 'New'),
            'subscriptions' => array('view_subscription&new=yes' => 'New'),
        );
    }

    protected function defaultActiveMap()
    {
        return array(
            'charge' => '/',
            'view_coupon' => 'coupons',
            'view_action' => 'actions',
            'view_subscription' => 'subscriptions',
        );
    }
}
