<?php

return array(
      'author'      => 'Matthew Johnson',
      'author_url'  => 'https://midwesterninteractive.com',
      'name'        => 'scheduler',
      'description' => '',
      'version'     => '1.0',
      'namespace'   => 'scheduler/'
);