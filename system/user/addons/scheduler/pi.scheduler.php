<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Scheduler Class
 
 * @package         ExpressionEngine
 * @category        Scheduler
 * @author          Midwestern Interactive
 * @copyright       Copyright (c) 2015
 * @link            http://midwesterninteractive.com/
 */

$plugin_info = array(
    'pi_name'         => 'Scheduler',
    'pi_version'      => '1.0',
    'pi_author'       => 'Matthew Johnson',
    'pi_author_url'   => 'http://MidwesternInteractive.com/',
    'pi_description'  => 'Allows you to generate 1 of 13. ',
    'pi_usage'        => Scheduler::usage()
);

class Scheduler
{

    public $return_data = "";

    // --------------------------------------------------------------------

    /**
     * Form Refresher
     *
     * This function allows you to refresh the page on back button pressing.
     *
     * @access  public
     * @return  string
     */
    public function __construct()
    {
		$this->EE = get_instance();   
		
        $grid_id = ee()->TMPL->fetch_param('grid_id');
        $student_id = ee()->TMPL->fetch_param('student_id');
        $app_id = ee()->TMPL->fetch_param('app_id');
        


        $userEmail = $this->EE->db->query("SELECT concat(field_id_24, ', ',field_id_23) as result FROM exp_channel_data where entry_id = '".$app_id."'")->row()->result;
        if ($grid_id != "0"){
            $time = $this->EE->db->query("SELECT concat(ct.title, ' @ ',  g.col_id_13) as result from exp_channel_grid_field_185 as g LEFT JOIN exp_channel_titles as ct ON ct.entry_id = g.entry_id where row_id = '".$grid_id."'")->row()->result;
        }
        else{
            $time = $this->EE->db->query("SELECT field_id_156 as result from exp_channel_data where entry_id = '".$app_id."'")->row()->result;
        }

        $data1 = array(
            'col_id_14' => ''
        );

        $data2 = array(
            'col_id_14' => $student_id
        );

        ee()->db->where('col_id_14', $student_id);
        ee()->db->update('exp_channel_grid_field_185', $data1);
        if ($grid_id != "0"){
            ee()->db->where('row_id', $grid_id);
            ee()->db->update('exp_channel_grid_field_185', $data2);
        }

        $to = $userEmail. ', zc@buildmidwestern.com';
        $headers = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "From: Aspire <no-reply@aspire.com>";
        $headers[] = "Content-type: text/html; charset=iso-8859-1";
        $headers[] = "Reply-To: ".$userEmail;

        if ($grid_id == "0"){
            // Canceled Interview
            $subject = "Canceled Interview - ".$time;
            $emailMessage = 'Your interview scheduled for '.$time.' has been canceled';
        }
        else{
            // Scheduled Interview
            $subject = "Scheduled Interview - ".$time;
            $emailMessage = 'You have a new scheduled interview on '.$time.'.';

        }

        ee()->load->library('email');
        ee()->load->helper('text');

        ee()->email->wordwrap = true;
        ee()->email->mailtype = 'text';
        ee()->email->from('no-reply@aspire.com');
        ee()->email->to($to);
        ee()->email->subject($subject);
        ee()->email->message($emailMessage);
        ee()->email->Send();

        // mail($to, $subject, $emailMessage, implode("\r\n", $headers));
    }

    // --------------------------------------------------------------------

    /**
     * Usage
     *
     * This function describes how the Scheduler is used.
     *
     * @access  public
     * @return  string
     */
    public static function usage()
    {
        ob_start();  ?>
        
        @buildmidwestern
        http://midwesterninteractive.com


    <?php
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
    }
    // END
}
/* End of file pi.Scheduler.php */