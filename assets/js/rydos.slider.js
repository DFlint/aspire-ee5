/* Full screen responsive slider
 * Copyright (c) 2016 Ryan Doss @ryandoss @buildmidwestern
 * Apply initiation on Child's Direct Parent
 *
 *
 *
 *
 * @version 1.5 - 2016
 * Added Touch swipe capabilities if proper dependencies are includes
 * Added resize of parent on window resize to improve responsiveness
 *
 * @version 1.4 - 2016
 * Added simultaneous animation to slides. Both current and next slide transition simultaneously
 *
 * @version 1.3 - 2015
 * Added API functions for pausing and starting auto play
 *
 * @version 1.2 - 2015
 * Added sequential child transition. Specified child elements will fade in sequentially.
 *
 * @version 1.1 - 2014
 * Added arrow key navigation
 *
 * @version 1.0 - 2014
 * Initial Launch
 */
(function($)
{
	$.fn.rydoslider = function(d, m)
	{
		return this.each(function()
		{
			var o = $(this),
				methods = {
					init: function()
					{
						// Hide all slides
						o.slides.each(function(i,s)
						{
							$(s).css({opacity:0,position:'absolute',zIndex:i});
						});
						
						// Display first slide
						o.slides.eq(o.currentIndex).css({opacity:1,zIndex:o.count});

						// Set height of main object to height of first slide
						if(o.settings.resizeOnInit)
							o.nextSlide.parent().css({height:o.slides.eq(o.currentIndex).height() + "px"});

						// Set height of main object to height of first slide
						$(window).resize(function(){
						    o.currentSlide.parent().css({height:o.slides.eq(o.currentIndex).height() + "px"});
						});

						// Stop timer on Hover
						if(o.settings.autoPlay && o.settings.pauseOnHover && ! o.paused && o.count >= 2)
						{
							$(o).hover(function() {
								methods.pauseAuto();
								if(o.settings.showTimer)
									methods.pauseTimer();
								o.paused = true;
							}, function() { 
								methods.startAuto();
								if(o.settings.showTimer)
									methods.startTimer();
								o.paused = false;
							});
						}
						
						// If more than one slide append the controls & start timer
						if(o.count > 1)
						{
							methods.appendControls();
							
							setTimeout(function(){
								if(o.settings.autoPlay) {
									methods.startAuto()
									if(o.settings.showTimer)
										methods.startTimer();
								}
							}, o.delayStart);
						}
						
						// Show Navigation on Hover
						if(o.settings.showNavOnHover && o.count >= 2)
						{
							$(o.parent()).hover(function() {
								// If there is Numeric navi
								if(o.settings.showNumeric)
								{
									o.controls.numeric.stop(true, true).animate({opacity:1}, 500);
								}
								// If there is Directional navi
								if(o.settings.showDirectional)
								{
									$('.arrow_next', this).stop(true, true).animate({right:'8%',opacity:1}, 500);
									$('.arrow_previous', this).stop(true, true).animate({left:'8%',opacity:1}, 500);
								}
							}, function() {
								// If there is Numeric navi
								if(o.settings.showNumeric)
								{
									o.controls.numeric.stop(true, true).animate({opacity:0}, 500);
								}
								// If there is Directional navi
								if(o.settings.showDirectional)
								{
									$('.arrow_next', this).stop(true, true).animate({opacity:0}, 0, function(){ $(this).css({ right:'0%' }); });
									$('.arrow_previous', this).stop(true, true).animate({opacity:0}, 0, function(){ $(this).css({ left:'0%' }); });
								}
							});
						}
					},
					
					appendControls: function()
					{
						// Create timer
						if(o.settings.showTimer)
							o.timer = $('<div />').addClass('timer').css({position:'absolute', width:'0%', height:'2px', backgroundColor:'#fff', opacity:0.5, zIndex:15}).appendTo(o);
						
						// Show numeric controls
						if(o.settings.showNumeric)
						{
							o.controls.numeric = $('<div />').addClass('ctrl_nav').css({zIndex:o.count+1}).appendTo(o);
							o.slides.each(function(i)
							{
								var a = i == o.currentIndex ? ' class="active_item"' : '';
								$('<a href=""' + a + ' data-show-slide="' + i + '">' + (i + 1) +'</a>').appendTo(o.controls.numeric);
							});
							
							// Activate numeric controls
							o.controls.numeric.find('a').bind('click', function(e)
							{
								e.preventDefault();
								if(o.animating)
									return;
								var a = $(this).data('show-slide');
								if(a != o.currentIndex)
									methods.nextItem(a);
							});
							if(o.settings.showNavOnHover)
								o.controls.numeric.css('opacity', 0);
						}
						
						// Show directional controls
						if(o.settings.showDirectional)
						{
							var a = ['previous', 'next'];
							o.controls.arrow = $('<div />').addClass('arrow_nav').css({zIndex:o.count+1}).prependTo(o);
							for(i in a)
							{
								var c = ' class="arrow_' + a[i] + '"';
								$('<a href=""' + c + ' data-show-slide="' + a[i] + '" >' + a[i] + '</a>').prependTo(o.controls.arrow);
							}
							
							// Acivate directional controls
							o.controls.arrow.find('a').bind('click', function(e)
							{
								e.preventDefault();
								if(o.animating)
									return;
								var a = $(this).data('show-slide');
								switch(a)
								{
									case 'next':
										a = o.currentIndex >= o.count-1 ? 0 : o.currentIndex+1;
									break;
									case 'previous':
										a = o.currentIndex <= 0 ? o.count-1 : o.currentIndex-1;
									break;
								}
								if(a != o.currentIndex)
									methods.nextItem(a);
							});
							
							if(o.settings.showNavOnHover)
								o.controls.arrow.find('a').css('opacity', 0);
							
						}

						// If Keypress is acitve create keyboard shortcuts
						if(o.settings.keyPress)
						{
							$(document).bind('keydown',function(e)
							{
								// Don't allow change when in transition
								if(o.animating)
									return;
								var key = e.which;
								switch(key)
								{
									// Left arrow to previous slide
									case 37:
										a = o.currentIndex <= 0 ? o.count-1 : o.currentIndex-1;
									break;

									// Right arrow to next slide
									case 39:
										a = o.currentIndex >= o.count-1 ? 0 : o.currentIndex+1;
									break;
									default:
									break;
								}

								// Change slide
								if(a != o.currentIndex && a != 'undefined' && (key == 37 || key == 39))
									methods.nextItem(a);
							});
						}

						// If if swiping is enabled
						if(o.settings.swipeNav)
						{
							$(o).swipe( {
								//Generic swipe handler for all directions
								swipeLeft:function(event, direction, distance, duration, fingerCount) {
									
									a = o.currentIndex >= o.count-1 ? 0 : o.currentIndex+1;

									// Change slide
									if(a != o.currentIndex && a != 'undefined')
										methods.nextItem(a);
								},
								swipeRight:function(event, direction, distance, duration, fingerCount) {
									
									a = o.currentIndex <= 0 ? o.count-1 : o.currentIndex-1;

									// Change slide
									if(a != o.currentIndex && a != 'undefined')
										methods.nextItem(a);
								},
								//Default is 75px, set to 0 for demo so any distance triggers swipe
								threshold:15
							});
						}
					},
					
					// Display the next slide
					nextItem: function(param)
					{
						// Pause timer
						methods.pauseAuto();
						if(o.settings.showTimer)
							methods.pauseTimer();
						
						// Define next index
						o.nextIndex = o.currentIndex >= o.count-1 ? 0 : o.currentIndex+1;
						
						// If next index is specified overwrite previous definition
						if(param != undefined)
							o.nextIndex = param;
						
						// Hide children of current slide
						o.slideKids.animate({opacity:0});
						
						// Define current & next slides
						o.currentSlide = o.slides.eq(o.currentIndex).css({zIndex:o.currentIndex});
						o.nextSlide = o.slides.eq(o.nextIndex);

						// Define next slides children
						o.slideKids = $(o.settings.kids, o.nextSlide);
						
						// Resize parent if setting is true
						if(o.settings.resizeSlide)
							o.nextSlide.parent().animate({height:o.nextSlide.height() + 'px'});
						
						// Change the active numerica control
						if(o.settings.showNumeric)
						{
							o.controls.numeric.find('a').eq(o.currentIndex).removeClass('active_item');
							o.controls.numeric.find('a').eq(o.nextIndex).addClass('active_item');
						}
						
						// Start the transition
						o.animating = true;

						// If sequential true move children to the left 20px
						if(o.settings.sequential)
							o.slideKids.css({zIndex:o.count,opacity:0});

						// Start transition
						o.nextSlide.css({zIndex:o.count,opacity:0}).animate({opacity:1}, o.transition, function()
						{
							// Show children in sequential order
							if(o.settings.sequential)
							{
								o.slideKids.each(function(i)
								{
									$(this).delay(100*i).animate({opacity:1}, o.transition);
								});
							}

							// Fade out current slide after next slide is finished with transition
							if(!o.settings.simultaneousTransition)
							{
								// Set the current slide and index
								o.currentSlide.css({position:'absolute',zIndex:o.currentIndex}).css({opacity:0});
								o.currentIndex = o.nextIndex;
							}
							
							// If paused is not true start the timers again
							if(!o.paused)
							{
								if(o.settings.autoPlay) {
									methods.startAuto();
									if(o.settings.showTimer)
										methods.startTimer();
								}
							}

							// Transition complete
							o.animating = false;
						});

						// Fade current and next slide at the same time.
						if(o.settings.simultaneousTransition)
						{
							// Set the current slide and index
							o.currentSlide.css({position:'absolute',zIndex:o.currentIndex}).animate({opacity:0}, o.transition);
							o.currentIndex = o.nextIndex;
						}
					},
					
					startTimer: function()
					{
						o.timer.animate({width:'100%'}, o.delay);
					},
					
					pauseTimer: function()
					{
						o.timer.stop(true,true);
						o.timer.css({width:'0%'});
					},
					
					startAuto: function()
					{
						clearTimeout(o.timeout);
						o.timeout = setTimeout(function()
						{
							methods.nextItem();
						}, o.delay);
					},
					
					pauseAuto: function()
					{
						clearTimeout(o.timeout);
					},
				},
				defaults = {
					slides: 'header',
					kids: ':header',
					start: 0,
					timeBetween: 6,
					delayStart: 0,
					transitionTime: 1,
					transitionType: 'slide',
					resizeOnInit: false,
					sequential: true,
					simultaneousTransition: false,
					pauseOnHover: true,
					showDirectional: true,
					showNumeric: true,
					showTimer: true,
					showNavOnHover: true,
					autoPlay: true,
					resizeSlide: true,
					keyPress: true,
					swipeNav: false // Requires dependancy - https://github.com/mattbryson/TouchSwipe-Jquery-Plugin   OR   `bower install jquery-touchswipe --save`
				};

			o.methods = $.extend(methods, $.fn.rydoslider.methods, m);
			o.settings = $.extend({}, defaults, $.fn.rydoslider.defaults, d);
			o.slides = o.find(o.settings.slides);
			o.count = o.slides.length,
			o.currentIndex = o.settings.start;
			o.currentSlide = o.slides.eq(o.currentIndex);
			o.nextIndex = 0;
			o.nextSlide = o.currentSlide;
			o.slideKids = $(o.settings.kids, o.nextSlide);
			o.delay = o.settings.timeBetween * 1000,
			o.transition = o.settings.transitionTime * 1000,
			o.controls = {},
			o.timer = {},
			o.paused = false,
			o.animating = false,
			o.resizeTime = false;
			o.delayStart = (o.data('delay-start') == undefined ? o.settings.delayStart : o.data('delay-start')) * 1000;

			methods.init();
			
			/* Add page API options */
			$.fn.rydoslider.pauseAuto = function(){
				clearTimeout(o.timeout);
			}
			
			$.fn.rydoslider.startAuto = function(){
				clearTimeout(o.timeout);
				o.timeout = setTimeout(function()
				{
					methods.nextItem();
				}, o.delay);
			}
		});
	}
})(jQuery);