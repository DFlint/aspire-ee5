$(function (){
    function confirmation(event){
        // button that was pressed
        var button = event.currentTarget;
        var dataRow = $(button).closest("tr").find("[data-row]");
        // prepare data
        var data = {
            entry_id : dataRow.data('entry_id'),
            field_name : $(button).attr("name"),
            field_value : $(button).val(),
            recipient: dataRow.data('recipient'),
            street: dataRow.data('street'),
            city: dataRow.data('city'),
            state: dataRow.data('state'),
            zip: dataRow.data('zip'),
            recipient_email : dataRow.data('email'),
            decision : $(button).data('modify'),
            modification : $(button).data('modify'),
            csrf_token: '{csrf_token}'
        }  
        // prepare modal with data from data{}
        var modal = $("#confirmationModal");
        modal.modal();
        modal.find('.modal-body').html("Are you sure you want to "  + data.modification + " " + data.recipient + " ?");
        modal.find('.btn-primary').html(data.modification);

        switch(data.modification){
            case 'Award':
                modal.find('.btn-primary').removeClass('btn-color-b');
                modal.find('.btn-primary').removeClass('btn-color-r');
                modal.find('.btn-primary').addClass('btn-color-g');
                break;
            case 'Deny':
                modal.find('.btn-primary').removeClass('btn-color-b');
                modal.find('.btn-primary').removeClass('btn-color-g');
                modal.find('.btn-primary').addClass('btn-color-r');
                break;
            case 'Close Application':
                modal.find('.btn-primary').removeClass('btn-color-r');
                modal.find('.btn-primary').removeClass('btn-color-g');
                modal.find('.btn-primary').addClass('btn-color-b');
                break;
        }

        
        // bind on click modification() to confirmation modal and pass in data 
        modal.find('.btn-primary').off('click');
        modal.find('.btn-primary').on('click', {"data" : data, "object" : $(button)}, modification);
    }   

    //changes data in db, and calls sendEmail()
    function modification(event, $object){
        // update database with data from confirmation
        $.post("/entrymodification", event.data.data);
        // send email with data from confirmation
        sendEmail(event.data.data, function(){
            // hide the confirmation modal
            var confirmationModal = $("#confirmationModal").modal('hide');
            var successModal = $("#successModal").modal();
        });
        
        // psudo remove row from table
        event.data.object.closest('tr[role="row"]').hide();
    }

    // sends an email
    function sendEmail(data, callback){ 
        console.log(data)
        $.post("/sendmail/" + data.decision, {
            entry_id: data.entry_id,
            recipient : data.recipient,
            street: data.street,
            city: data.city,
            state: data.state,
            zip: data.zip,
            recipient_email: data.recipient_email,
            csrf_token: '{csrf_token}'
        });
        callback();
    }

    // bind confirmation() to table elements needing confirmation
    $('[data-modify]').on("click", confirmation)
});