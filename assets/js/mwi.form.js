/*          _    __               __             
 *   __ _  (_)__/ /    _____ ___ / /____ _______ 
 *  /  ' \/ / _  / |/|/ / -_|_-</ __/ -_) __/ _ \
 * /_/_/_/_/\_,_/|__,__/\__/___/\__/\__/_/ /_//_/
 *
 * MWI Custom form styles and validation
 * copyright Midwestern Interactive 2015
 *
 *
 * @version 1.1 - May 2016
 * Added Fancy Labels to inputs and textareas if .fancy-labels class is applied to form element
 */

(function($)
{
	$.fn.mwiform = function(d)
	{
		return this.each(function()
		{

			// Default settings
			var defaults = {
				selectClass: 'mwi-select',
				radioClass: 'mwi-radio',
				checkboxClass: 'mwi-checkbox',
				errorLoc: 'after', // With error show before or after the input
				errorPlaceholder: [false,' is required'], // If true will use forms placeholder for beginning of error. Second value is what is to follow the placeholder name.
				errorMsg: 'This is a required field', // If errorPlaceholder is set to false it will use this default message.
			};

			// Define default options and primary object
			var options = $.extend({}, defaults, $.fn.mwiform.defaults, d),
				o = $(this);

			// Style Selects
			if(o.is("select")) {
				var wrapper = $('<div class="'+ options.selectClass +'"></div>');
				o.wrap(wrapper).css({opacity:0,cursor:'pointer'});
				$('<div class="select-options"><span>'+ $('option:selected', o).text() +'</span><i></i></div>').insertBefore(o);

				o.change(function(){
					$('.select-options span', o.parent()).text($('option[value="'+$(this).val()+'"]', this).text());
				}).hover(function(){
					$('.select-options i', o.parent()).toggleClass('active');
				});
			}

			// Style Checkboxes
			if(o.is(":checkbox")) {
				var wrapper = $('<div class="'+ options.checkboxClass +'"></div>');
				o.wrap(wrapper).css({opacity:0,cursor:'pointer'});

				if($(this).is(':checked'))
					$(this).parent().addClass('active');

				o.change(function() {
					$(this).parent().toggleClass('active');
				});
			}

			// Style Radios
			if(o.is(":radio")) {
				var wrapper = $('<div class="'+ options.radioClass +'"></div>');
				o.wrap(wrapper).css({opacity:0,cursor:'pointer'});

				if($(this).is(':checked'))
					$(this).parent().addClass('active');

				o.change(function() {

					var name = $(this).attr('name');
					$('input[name="'+name+'"]').each(function(){
						$(this).parent().removeClass('active');
					});

					$(this).parent().addClass('active');
				});
			}

			// Form validation
			if(o.is('form')) {
				// Fancy Labels
				$('.fancy-labels input, .fancy-labels textarea').focus(function(){
					if(!$(this).hasClass('active'))
						$(this).prev().addClass('active');
				}).blur(function(){
					if($(this).val() == '')
						$(this).prev().removeClass('active');
				});
				$($(this)).submit(function(e){
					var form = this,
						error =	false;
					e.preventDefault();

					$('.error',form).remove();

					$('.require', form).each(function(i){
						var el = $(this),
							errorCon = $('<span />').addClass('error'),
							placeholderTxt = (el.attr('placeholder') != undefined) ? el.attr('placeholder') : "This";

						if(el.attr('name') !== undefined && el.attr('name').indexOf("signature") != -1) {
							if(el.val() == "") {
								error = true;
							}
						}
						if(el.is(':visible')) {
							if(!el.is('input') && !el.is('select') && !el.is('textarea')) {
								var rcFields = $('input:checked', el);
								if(rcFields.length <= 0) {
									(options.errorPlaceholder[0] === true) ? errorCon.text(placeholderTxt + options.errorPlaceholder[1]) : errorCon.text(options.errorMsg);
									(options.errorLoc == 'after') ? errorCon.appendTo(el) : errorCon.prependTo(el);
									error = true;
								}
							} else {
								if(el.val() == "" || el.val() == "--- Select ---") {
									(options.errorPlaceholder[0] === true) ? errorCon.text(placeholderTxt + options.errorPlaceholder[1]) : errorCon.text(options.errorMsg);
									(options.errorLoc == 'after') ? errorCon.insertAfter(el) : errorCon.insertBefore(el);
									error = true;
								}
							}
						}
					});

					if(error != true) {
						form.submit();
					} else {
						$('<div id="errors-on-page" />').addClass('error').text('There are errors on the page, please make sure you completed all required fields').prependTo($(form));
						$('html, body').animate({
						    scrollTop: $("#errors-on-page").offset().top
						}, 1000);
					}
				});
			}
		});
	}
})(jQuery);