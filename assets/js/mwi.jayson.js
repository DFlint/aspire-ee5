/*          _    __               __             
 *   __ _  (_)__/ /    _____ ___ / /____ _______ 
 *  /  ' \/ / _  / |/|/ / -_|_-</ __/ -_) __/ _ \
 * /_/_/_/_/\_,_/|__,__/\__/___/\__/\__/_/ /_//_/
 *
 * MWI Jayson: Grid Field JSON Capture
 * copyright Midwestern Interactive 2016
 *
 *
 * @version 1.0 - July 2016
 * Initial Launch
 */

;(function($)
{
	$.fn.mwijayson = function(d)
	{

		// Default settings
		var defaults = {
			selector: '.jayson',
			setElem: 'set', // The data attribute for the html set i.e. data-set
			addBtn: '[data-add-set]',
			deleteBtn: '[data-delete-set]',
			afterAdd: function() {},
			afterDelete: function() {}
		};

		// Define default options and primary object
		var options = $.extend({}, defaults, $.fn.mwijayson.defaults, d);

		return this.each(function(i)
		{
			$(options.selector, this).each(function(){
				var parent = $(this),
					dataSet = parent.data(options.setElem),
					html = {};

				// Push the HTML for each set to an object
		    	html[dataSet] = $($('.'+dataSet)).last().clone();


		    	// Prepend the desired HTML set on click of specified element
		    	parent.on('click',options.addBtn,function(){
		    		// Reclone each time on add
		    		var clone = html[dataSet].clone(),
						count = $('.'+dataSet).length + 1;

		    		// Append numbers to the end of the radios
					clone.find(':radio').each(function() {
						$(this).attr('name',this.name.replace(/_[0-9]+/g, '_'+count));
					});

					clone.insertBefore($(this));

					// Remove values of clone
					$($('.'+dataSet)).last().find('input,textarea,select').each(function(){
						if ($(this).is(':radio') || $(this).is(':checkbox'))
						{
							$(this).prop('checked',false);
						} else {
							$(this).val('');
						}
					});

					// Callback after set is added with the added element
					options.afterAdd.call(options, clone);
		    	});

		    	// Remove the HTML set on click of specified element
		    	parent.on('click',options.deleteBtn,function(){
					var count = $('.'+dataSet).length;

		    		$(this).closest('.'+dataSet).remove();

					// Callback after set is deleted
					options.afterDelete.call();
		    	});
			});

			$(this).on('submit',function(e){

				$('.jayson').each(function(){
		    		var jsonObj = [];

		    		$('.'+$(this).data('set'), this).each(function(){
		    			var item = {},
		    				set = $(this);

			    		$('input, select, textarea', this).each(function(){
			    			var e = this,
			    				o = $(this);

			    			if (o.is(':radio'))
			    			{
			    				var name = e.name.replace(/_[0-9]+/g,'');
			    				item[name] = $("input[name='"+e.name+"']:checked", set).val() ? $('input[name="'+e.name+'"]:checked', set).val() : '';
			    			} else if (o.is(':checkbox')) {
			    				var name = '';
			    			} else {
			    				item[e.name] = o.val();
			    			}

			    		});
			    		jsonObj.push(item);

		    		});

		    		var dump = $('input[name="'+$(this).data('input')+'"]');
		    		if (dump[0])
		    			dump.val(JSON.stringify(jsonObj));
		    		else
		    			$('<input type="hidden" name="'+$(this).data('input')+'">').val(JSON.stringify(jsonObj)).insertBefore($(this));

		    	});
		    });

		});
	}
})(jQuery);