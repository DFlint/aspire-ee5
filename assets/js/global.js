$(document).ready(function(){
	$('.mobile-btn').click(function(){
		$('body').toggleClass('active');
		$('.mobile-nav').toggleClass('active');
		$('.mobile-nav').toggleClass('mobile');
		$('.mobile-btn').toggleClass('active');
	});
	$('.drop').hover(function(){
		$('.sub').toggleClass('sub-show');
	});
	if ( !$( ".ui-only-color" ).length ) {
		$(window).scroll (function () {
			var sT = $(this).scrollTop();

			if (sT >= 1) {
				$('nav').addClass('navcolor')
				$('.no-bar, .bar').addClass('active');
			}else {
				$('nav').removeClass('navcolor');
				$('.no-bar, .bar').removeClass('active');
			}
		});
	}
	if ($('.ui-datatable').length){
		$('.ui-datatable').DataTable({
			autoWidth: true,
			responsive: true,
			colReorder: true,
			paging: false,
			info: false,
			searching: false
		});
	}

    //window sizer
   // var sizeContainer = $('<div />').addClass('window-size').css({backgroundColor:'#000',opacity:'0.5',position:'fixed',padding:'5px 10px',color: '#fff', zIndex:'1000',right:'10px',bottom:'10px'}).text($(window).width() + ', ' + $(window).height()).prependTo('body');
   //  $(window).resize(function(){
   //      sizeContainer.text($(window).width() + ', ' + $(window).height());
   //  });

    // Word counter for success story
    $('#text').click(function(){
    	$('.words-left').show();
    });
    counter = function() {
    	var value = $('#text').val();

    	if (value.length == 0) {
    		$('#wordCount').html(0);
    		return;
    	}

    	var regex = /\s+/gi;
    	var wordCount = value.trim().replace(regex, ' ').split(' ').length;
    	var totalChars = value.length;
    	var charCount = value.trim().length;
    	var charCountNoSpace = value.replace(regex, '').length;

    	$('#wordCount').html(wordCount);
    };
    $('#count').click(counter);
    $('#text').change(counter);
    $('#text').keydown(counter);
    $('#text').keypress(counter);
    $('#text').keyup(counter);
    $('#text').blur(counter);
    $('#text').focus(counter);

$('.same_info').on('change', function(){
  var inputs = ['mailing_street','mailing_apartment_number','mailing_city','mailing_state','mailing_zip'];
  for (i = 0; i < inputs.length; i++)
  {
    $('input[name="res_'+inputs[i]+'"]').val($(this).is(':checked') ? $('input[name="mailing_'+inputs[i]+'"]').val() : "");
  }
});

});